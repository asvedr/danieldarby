use crate::entities::constants::MAX_SHARED_CARDS;
use crate::entities::desk::Card;
use crate::entities::errors::GameError;
use crate::entities::game::{
    ExpectedAction, GameState, NextRoundResponse, Player, PlayerInfo, PlayerRound, RoundState,
    RoundStateKind, SharedGameInfo, Step, SwitchStatus, UserGameInfo,
};
use crate::proto::game::{IAutomaticStepProcessor, IGame, IUserStepProcessor};
use crate::proto::rate_check::IWinChecker;

pub struct Game<WinChecker> {
    win_checker: WinChecker,
    user_steps: Vec<Box<dyn IUserStepProcessor>>,
    auto_steps: Vec<Box<dyn IAutomaticStepProcessor>>,
}

impl<WinChecker: IWinChecker> Game<WinChecker> {
    pub(crate) fn new(
        win_checker: WinChecker,
        user_steps: Vec<Box<dyn IUserStepProcessor>>,
        auto_steps: Vec<Box<dyn IAutomaticStepProcessor>>,
    ) -> Self {
        Self {
            win_checker,
            user_steps,
            auto_steps,
        }
    }

    fn init_new_round(state: &mut GameState) {
        let mut players = Vec::new();
        for p in state.players.iter() {
            let player = PlayerRound {
                bet: 0,
                cards: vec![],
                active: p.balance > 0,
            };
            players.push(player)
        }
        let dealer = (state.round_number - 1) % state.players.len();
        state.round = RoundState {
            players,
            shared_cards: Vec::new(),
            state: RoundStateKind::BigBlind,
            unused_cards: Card::all(),
            dealer,
            current_player: 0,
        };
        let dealer = state.round.closest_active_player(dealer);
        state.round.dealer = dealer;
    }

    fn flush_bank(winner: usize, state: &GameState) -> GameState {
        let bank = state.round.bank();
        let mut players = state
            .players
            .iter()
            .enumerate()
            .map(|(id, player)| Player {
                name: player.name.clone(),
                balance: player.balance - state.round.players[id].bet,
            })
            .collect::<Vec<_>>();
        players[winner].balance += bank;
        GameState {
            round: Default::default(),
            round_number: state.round_number,
            players,
        }
    }

    fn apply_step_processor(&self, state: &mut RoundState, player: usize, step: Step) {
        for processor in self.user_steps.iter() {
            if processor.check_matched(&step) {
                processor.execute(state, player, step);
                return;
            }
        }
        panic!("Step processor not configured")
    }

    fn switch_state_to_auto(state: &mut RoundState) {
        assert!(matches!(state.state, RoundStateKind::Check { .. }));
        let bet = state.state.get_bet();
        state.current_player = state.dealer;
        let new_state = if state.shared_cards.len() == MAX_SHARED_CARDS {
            RoundStateKind::OpenAll
        } else {
            RoundStateKind::NewCard { bet }
        };
        state.state = new_state
    }

    fn get_expected_auto_step(&self, state: &RoundState) -> Option<&dyn IAutomaticStepProcessor> {
        for processor in self.auto_steps.iter() {
            if processor.check_matched(state) {
                return Some(&**processor);
            }
        }
        None
    }

    fn activate_check_loop(state: &mut RoundState) {
        let player = state.state.get_player_id();
        state.state = RoundStateKind::Check {
            bet: state.state.get_bet(),
            player,
        };
        state.current_player = player;
    }

    fn make_player_info(state: &GameState, id: usize) -> PlayerInfo {
        PlayerInfo {
            name: state.players[id].name.clone(),
            has_cards: state.round.players[id].active,
            balance: state.players[id].balance,
            bet: state.round.players[id].bet,
        }
    }
}

impl<WinChecker: IWinChecker> IGame for Game<WinChecker> {
    fn new_game(&self, players: Vec<String>, balance: usize) -> Result<GameState, GameError> {
        if players.len() < 2 {
            return Err(GameError::TooSmallPlayers);
        }
        let players = players
            .into_iter()
            .map(|name| Player { name, balance })
            .collect::<Vec<_>>();
        let mut state = GameState {
            round: RoundState::default(),
            round_number: 1,
            players,
        };
        Self::init_new_round(&mut state);
        Ok(state)
    }

    fn get_round_winner(&self, state: &GameState) -> Option<usize> {
        let players = state
            .round
            .players
            .iter()
            .enumerate()
            .filter(|(_, p)| p.active)
            .map(|(id, p)| (id, p.cards.clone()))
            .collect::<Vec<_>>();
        if players.len() == 1 {
            let (id, _) = &players[0];
            return Some(*id);
        }
        if !matches!(state.round.state, RoundStateKind::CheckAll) {
            return None;
        }
        let winner = self
            .win_checker
            .get_status(&state.round.shared_cards, &players);
        Some(winner)
    }

    fn next_round(&self, state: &GameState) -> NextRoundResponse {
        let winner = match self.get_round_winner(state) {
            None => return NextRoundResponse::NotReady,
            Some(val) => val,
        };
        let mut state = Self::flush_bank(winner, state);
        state.round_number += 1;
        Self::init_new_round(&mut state);
        let active = state.round.get_active_players();
        if active.len() == 1 {
            let (id, _) = &active[0];
            NextRoundResponse::Winner(*id)
        } else {
            NextRoundResponse::Round(state)
        }
    }

    fn get_steps(&self, state: &GameState, player: usize) -> Vec<Step> {
        let mut result = Vec::new();
        if !(state.round.state.is_user_step() && state.round.current_player == player) {
            return result;
        }
        let min_balance = state
            .players
            .iter()
            .filter(|p| p.balance > 0)
            .map(|p| p.balance)
            .min()
            .unwrap_or(0);
        for processor in self.user_steps.iter() {
            result.append(&mut processor.get_available(&state.round, min_balance));
        }
        result
    }

    fn get_expected_action(&self, state: &GameState) -> ExpectedAction {
        let kind = &state.round.state;
        match () {
            _ if kind.is_automatic_step() => match self.get_expected_auto_step(&state.round) {
                Some(val) => ExpectedAction::AutoStep(val.get_kind()),
                _ => panic!("auto step had no processor: {:?}", kind),
            },
            _ if kind.is_user_step() => ExpectedAction::UserStep(state.round.current_player),
            _ => ExpectedAction::Nothing,
        }
    }

    fn get_shared_info(&self, state: &GameState) -> SharedGameInfo {
        SharedGameInfo {
            players: (0..state.players.len())
                .map(|id| Self::make_player_info(state, id))
                .collect::<Vec<_>>(),
            cards: state.round.shared_cards.clone(),
            bank: state.round.bank(),
            dealer: state.round.dealer,
        }
    }

    fn get_user_info(&self, state: &GameState, player: usize) -> UserGameInfo {
        UserGameInfo {
            shared_info: self.get_shared_info(state),
            id: player,
            cards: state.round.players[player].cards.clone(),
        }
    }

    fn apply_auto_step(&self, state: &GameState) -> Result<GameState, GameError> {
        let processor = match self.get_expected_auto_step(&state.round) {
            Some(val) => val,
            None => return Err(GameError::AutomaticStepNotExpected),
        };
        let balances = state.players.iter().map(|p| p.balance).collect::<Vec<_>>();
        let mut copy = state.clone();
        processor.execute(&mut copy.round, &balances);
        Ok(copy)
    }

    fn apply_user_step(
        &self,
        state: &GameState,
        player: usize,
        step: Step,
    ) -> Result<GameState, GameError> {
        let available = self.get_steps(state, player);
        if available.is_empty() {
            return Err(GameError::NotYourStep);
        }
        if !available.contains(&step) {
            return Err(GameError::InvalidStep);
        }
        let mut new_state = state.clone();
        self.apply_step_processor(&mut new_state.round, player, step);
        let status = new_state.round.switch_player_to_next();
        match (status, &state.round.state) {
            (SwitchStatus::Switched, _) => (),
            (SwitchStatus::OnlyOneLeft, _) => new_state.round.state = RoundStateKind::AllFolded,
            (SwitchStatus::LoopCompleted, RoundStateKind::NewBet { .. }) => {
                Self::activate_check_loop(&mut new_state.round)
            }
            (SwitchStatus::LoopCompleted, RoundStateKind::Check { .. }) => {
                Self::switch_state_to_auto(&mut new_state.round)
            }
            (SwitchStatus::LoopCompleted, other) => unreachable!("expected state: {:?}", other),
        }
        Ok(new_state)
    }
}
