use crate::entities::desk::Card;
use crate::entities::rule::RuleKind;
use crate::impls::rate_checker::RateChecker;
use crate::impls::rules;
use crate::impls::win_checker::WinChecker;
use crate::proto::rate_check::{IRateChecker, IWinChecker};

const VARIANTS: &[(&str, RuleKind)] = &[
    ("sa,d2,c5,cq,hk", RuleKind::HighCard),
    ("s5,d2,c5,cq,hk", RuleKind::Pair),
    ("s4,d2,c2,cq,h4", RuleKind::TwoPairs),
    ("sa,d2,c2,cq,h2", RuleKind::ThreeOfKind),
    ("sa,d2,c3,c4,h5", RuleKind::Straight),
    ("sa,dj,c10,cq,hk", RuleKind::Straight),
    ("ca,c2,c5,cq,ck", RuleKind::Flush),
    ("s2,d2,cq,dq,hq", RuleKind::FullHouse),
    ("sq,d2,cq,dq,hq", RuleKind::FourOfKind),
    ("sa,sj,s10,sq,sk", RuleKind::StraightFlush),
];

#[test]
fn test_rule_match() {
    let rate_checker = RateChecker::new(rules::all());
    for (variant, kind) in VARIANTS {
        let mut cards = Card::deserialize_vec(variant).unwrap();
        cards.sort();
        let used = rate_checker.get_rate_rule(&cards);
        assert_eq!(
            used, *kind,
            "rule expected: {:?}, rule found: {:?}",
            kind, used
        );
    }
}

fn assert_set_rate(higher: &str, lower: &str) {
    let rate_checker = RateChecker::new(rules::all());
    let mut higher_c = Card::deserialize_vec(higher).unwrap();
    higher_c.sort();
    let mut lower_c = Card::deserialize_vec(lower).unwrap();
    lower_c.sort();
    let rate1 = rate_checker.get_rate(&higher_c);
    let rate2 = rate_checker.get_rate(&lower_c);
    assert!(
        rate1 > rate2,
        "{}({:?} -> {}) must be higher then {}({:?} -> {})",
        higher,
        rate_checker.get_rate_rule(&higher_c),
        rate1,
        lower,
        rate_checker.get_rate_rule(&lower_c),
        rate2,
    )
}

#[test]
fn test_compare_rate_different_rules() {
    for i in 1..VARIANTS.len() {
        let (set1, _) = VARIANTS[i];
        let (set2, _) = VARIANTS[i - 1];
        assert_set_rate(set1, set2)
    }
}

// 1st > 2nd
const SAME_RULE_HIERARCHY: &[(&str, &str)] = &[
    ("s3,d2,c5,cq,sk", "s3,d2,c5,cq,hk"),   // High card; s > h
    ("s3,d2,c5,cq,hk", "s3,d2,c5,cq,dk"),   // High card; h > d
    ("s3,d2,c5,cq,dk", "s3,d2,c5,cq,ck"),   // High card; d > c
    ("s3,d2,c5,cq,ha", "s3,d2,c5,cq,sk"),   // High card; A > K
    ("s2,d2,sa,sk,sq", "s2,d2,h10,hj,hq"),  // Pair; A > Q
    ("s3,d3,h10,hj,hq", "s2,d2,sa,sk,sq"),  // Pair; 3 > 2
    ("s3,d3,h2,d2,ca", "s2,d2,s3,c3,hq"),   // Two Pairs; A > Q
    ("s4,d4,s3,c3,hq", "s3,d3,h2,d2,ca"),   // Two Pairs; 4 > 3
    ("s3,d3,h5,d5,ca", "s4,d4,s3,c3,hq"),   // Two Pairs; 5 > 4
    ("s3,d3,h3,d5,ca", "s4,d2,s2,c2,hq"),   // 3 of kind; 3 > 2
    ("s3,d3,h3,c3,ca", "s2,d2,h2,c2,hq"),   // 4 of kind; 3 > 2
    ("s2,h3,c4,s5,d6", "sa,h2,c3,s4,d5"),   // Straight; 6 > 5
    ("s10,hj,cq,sk,da", "s7,h8,c9,s10,dj"), // Straight; A > J
    ("s10,hj,cq,sk,da", "sa,h2,c3,s4,d5"),  // Straight; A(14) > A(1)
    ("d2,d10,dq,dk,da", "s2,s10,sq,sk,s7"), // Flush; A > K
    ("s2,s10,sq,sk,s7", "d2,d10,dq,dk,d7"), // Flush; s > d
    ("sa,da,ha,d5,c5", "s7,dk,sk,ck,h7"),   // Full House; Triple higher(A > K)
    ("s2,s3,s4,s5,s6", "sa,s2,s3,s4,s5"),   // Straight Flush; 6 > 5
];

fn assert_same_rule(str_a: &str, str_b: &str) {
    let mut set_a = Card::deserialize_vec(str_a).unwrap();
    let mut set_b = Card::deserialize_vec(str_b).unwrap();
    set_a.sort();
    set_b.sort();
    let rate_checker = RateChecker::new(rules::all());
    assert_eq!(
        rate_checker.get_rate_rule(&set_a),
        rate_checker.get_rate_rule(&set_b),
        "EXPECTED SAME RULES FOR {} AND {}",
        str_a,
        str_b,
    )
}

#[test]
fn test_compare_rate_same_rules() {
    for (a, b) in SAME_RULE_HIERARCHY {
        assert_same_rule(a, b);
        assert_set_rate(a, b)
    }
}

const WIN_CHECK_VARIANTS: &[(&str, &[&str], usize)] = &[
    ("c7,c8,c10", &["ha,hj", "s6,dq"], 0),          // high card
    ("c7,c8,c10", &["ha,hj", "s6,d7"], 1),          // pair
    ("c7,c8,c10", &["h6,h9", "d8,d7"], 0),          // straight
    ("c7,c8,c10", &["h6,h9", "d8,d7", "c6,c9"], 2), // straight flush
];

#[test]
fn test_win_checker() {
    let win_checker = WinChecker {
        rate_checker: RateChecker::new(rules::all()),
    };
    for (shared_s, players_s, expected) in WIN_CHECK_VARIANTS {
        let shared = Card::deserialize_vec(shared_s).unwrap();
        let players = players_s
            .iter()
            .enumerate()
            .map(|(i, src)| (i, Card::deserialize_vec(src).unwrap()))
            .collect::<Vec<_>>();
        let winner = win_checker.get_status(&shared, &players);
        assert_eq!(
            winner,
            *expected,
            "Invalid winner on set: {:?} expected:{}, got:{}",
            (shared_s, players_s),
            expected,
            winner,
        )
    }
}

#[test]
fn test_bug_mick_daniel() {
    let shared_cards = Card::deserialize_vec("♣K,♦5,♣8").unwrap();
    let mick_cards = Card::deserialize_vec("♠9,♥J").unwrap();
    let daniel_cards = Card::deserialize_vec("♠Q,♠6").unwrap();
    let win_checker = WinChecker {
        rate_checker: RateChecker::new(rules::all()),
    };
    let winner = win_checker.get_status(&shared_cards, &[(1, mick_cards), (2, daniel_cards)]);
    assert_eq!(winner, 2)
}
