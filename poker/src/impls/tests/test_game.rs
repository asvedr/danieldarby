use crate::entities::desk::Card;
use crate::entities::errors::GameError;
use crate::entities::game::{ExpectedAction, NextRoundResponse, Player, RoundStateKind, Step};
use crate::impls::game::Game;
use crate::impls::{auto_step_processors, user_step_processors};
use crate::proto::game::IGame;
use crate::proto::rate_check::IWinChecker;

struct MockWinChecker {}

impl IWinChecker for MockWinChecker {
    fn get_status(&self, _: &[Card], _: &[(usize, Vec<Card>)]) -> usize {
        0
    }
}

pub fn controller() -> Box<dyn IGame> {
    Box::new(Game::new(
        MockWinChecker {},
        user_step_processors::all(),
        auto_step_processors::all(),
    ))
}

#[test]
fn test_new_game_one_player() {
    let ctrl = controller();
    assert_eq!(
        ctrl.new_game(vec!["a".to_string()], 100),
        Err(GameError::TooSmallPlayers),
    )
}

#[test]
fn test_new_game() {
    let ctrl = controller();
    let game = ctrl
        .new_game(vec!["a".to_string(), "b".to_string()], 100)
        .unwrap();
    assert_eq!(game.round_number, 1);
    assert_eq!(
        game.players,
        &[
            Player {
                name: "a".to_string(),
                balance: 100
            },
            Player {
                name: "b".to_string(),
                balance: 100
            }
        ]
    );
    assert_eq!(game.round.players.len(), 2);
    assert_eq!(
        game.round
            .players
            .iter()
            .map(|p| p.cards.len())
            .sum::<usize>(),
        0,
    );
    assert!(game.round.shared_cards.is_empty());
    assert_eq!(game.round.state, RoundStateKind::BigBlind);
    assert_eq!(game.round.unused_cards.len(), 52);
    assert_eq!(game.round.dealer, 0);
    assert_eq!(game.round.current_player, 0);
}

#[test]
fn test_get_round_winner_folded() {
    let ctrl = controller();
    let mut game = ctrl
        .new_game(vec!["a".to_string(), "b".to_string()], 100)
        .unwrap();
    game.round.players[0].active = false;
    assert_eq!(ctrl.get_round_winner(&game), Some(1));
}

#[test]
fn test_get_round_winner_none() {
    let ctrl = controller();
    let mut game = ctrl
        .new_game(vec!["a".to_string(), "b".to_string()], 100)
        .unwrap();
    let states = [
        RoundStateKind::BigBlind,
        RoundStateKind::SmallBlinds,
        RoundStateKind::NewBet { bet: 1, player: 1 },
        RoundStateKind::Check { bet: 1, player: 1 },
        RoundStateKind::NewCard { bet: 0 },
        RoundStateKind::OpenAll,
    ];
    for state in states {
        game.round.state = state.clone();
        assert_eq!(
            ctrl.get_round_winner(&game),
            None,
            "Expected None on {:?}",
            state,
        );
    }
}

#[test]
fn test_get_round_winner_check() {
    let ctrl = controller();
    let mut game = ctrl
        .new_game(vec!["a".to_string(), "b".to_string()], 100)
        .unwrap();
    game.round.state = RoundStateKind::CheckAll;
    assert_eq!(ctrl.get_round_winner(&game), Some(0));
}

#[test]
fn test_next_round_not_ready() {
    let ctrl = controller();
    let game = ctrl
        .new_game(vec!["a".to_string(), "b".to_string()], 100)
        .unwrap();
    assert_eq!(ctrl.next_round(&game), NextRoundResponse::NotReady,)
}

#[test]
fn test_next_round_round() {
    let ctrl = controller();
    let mut game = ctrl
        .new_game(vec!["a".to_string(), "b".to_string()], 100)
        .unwrap();
    game.round.state = RoundStateKind::CheckAll;
    game.round.players[0].bet = 10;
    game.round.players[1].bet = 10;
    // game.round.bank = 20;
    let game = match ctrl.next_round(&game) {
        NextRoundResponse::Round(val) => val,
        _ => panic!(),
    };
    assert_eq!(game.round_number, 2);
    assert_eq!(
        game.players,
        &[
            Player {
                name: "a".to_string(),
                balance: 110
            },
            Player {
                name: "b".to_string(),
                balance: 90
            }
        ]
    );
    assert_eq!(game.round.players.len(), 2);
    assert_eq!(
        game.round
            .players
            .iter()
            .map(|p| p.cards.len())
            .sum::<usize>(),
        0,
    );
    assert!(game.round.shared_cards.is_empty());
    assert_eq!(game.round.state, RoundStateKind::BigBlind);
    assert_eq!(game.round.unused_cards.len(), 52);
    assert_eq!(game.round.dealer, 1);
    assert_eq!(game.round.current_player, 0);
}

#[test]
fn test_next_round_end() {
    let ctrl = controller();
    let mut game = ctrl
        .new_game(vec!["a".to_string(), "b".to_string()], 10)
        .unwrap();
    game.round.state = RoundStateKind::CheckAll;
    game.round.players[0].bet = 10;
    game.round.players[1].bet = 10;
    // game.round.bank = 20;
    assert_eq!(ctrl.next_round(&game), NextRoundResponse::Winner(0),)
}

#[test]
fn test_get_steps_bad_state() {
    let ctrl = controller();
    let mut game = ctrl
        .new_game(vec!["a".to_string(), "b".to_string()], 10)
        .unwrap();
    let states = [
        RoundStateKind::BigBlind,
        RoundStateKind::SmallBlinds,
        RoundStateKind::NewCard { bet: 0 },
        RoundStateKind::OpenAll,
        RoundStateKind::CheckAll,
        RoundStateKind::AllFolded,
    ];
    for state in states {
        game.round.state = state.clone();
        assert!(
            ctrl.get_steps(&game, 0).is_empty(),
            "Got steps on state: {:?}",
            state,
        )
    }
}

#[test]
fn test_get_steps_not_your() {
    let ctrl = controller();
    let mut game = ctrl
        .new_game(vec!["a".to_string(), "b".to_string()], 10)
        .unwrap();
    let states = [
        RoundStateKind::NewBet { bet: 0, player: 0 },
        RoundStateKind::Check { bet: 0, player: 0 },
    ];
    for state in states {
        game.round.state = state.clone();
        let s0 = ctrl.get_steps(&game, 0).is_empty();
        let s1 = ctrl.get_steps(&game, 1).is_empty();
        assert_eq!((s0, s1), (false, true), "Bad steps on state {:?}", state,);
    }
}

#[test]
fn test_expected_action() {
    use RoundStateKind::*;

    let ctrl = controller();
    let mut game = ctrl
        .new_game(vec!["a".to_string(), "b".to_string()], 10)
        .unwrap();
    let map = [
        (BigBlind, ExpectedAction::AutoStep(BigBlind)),
        (SmallBlinds, ExpectedAction::AutoStep(SmallBlinds)),
        (NewBet { bet: 0, player: 0 }, ExpectedAction::UserStep(0)),
        (Check { bet: 0, player: 0 }, ExpectedAction::UserStep(0)),
        (
            NewCard { bet: 0 },
            ExpectedAction::AutoStep(NewCard { bet: 0 }),
        ),
        (OpenAll, ExpectedAction::AutoStep(OpenAll)),
        (CheckAll, ExpectedAction::AutoStep(CheckAll)),
    ];
    for (state, expected) in map {
        game.round.state = state.clone();
        let action = ctrl.get_expected_action(&game);
        assert_eq!(
            action, expected,
            "State: {:?}, expected {:?}, found {:?}",
            state, expected, action,
        )
    }
}

// #[test]
// #[should_panic]
// fn test_expected_action_err() {
//     let ctrl = controller();
//     let mut game = ctrl
//         .new_game(vec!["a".to_string(), "b".to_string()], 10)
//         .unwrap();
//     game.round.state = RoundStateKind::AllFolded;
//     ctrl.get_expected_action(&game);
// }

#[test]
fn test_flush_bank() {
    let ctrl = controller();
    let mut game = ctrl
        .new_game(vec!["a".to_string(), "b".to_string()], 100)
        .unwrap();
    game.round.state = RoundStateKind::AllFolded;
    game.round.players[0].bet = 20;
    game.round.players[1].bet = 10;
    game.round.players[1].active = false;
    let game = match ctrl.next_round(&game) {
        NextRoundResponse::Round(val) => val,
        _ => unreachable!(),
    };
    assert_eq!(game.players[0].balance, 110);
    assert_eq!(game.players[1].balance, 90);
}

#[test]
fn test_limit_of_bet() {
    let ctrl = controller();
    let mut game = ctrl
        .new_game(vec!["a".to_string(), "b".to_string()], 20)
        .unwrap();
    game.round.players[0].bet = 20;
    game.round.players[1].bet = 20;
    game.round.state = RoundStateKind::Check { bet: 20, player: 1 };
    game.round.current_player = 0;
    let steps = ctrl.get_steps(&game, 0);
    let expected = [Step::Fold, Step::Check];
    assert_eq!(steps, expected);
}
