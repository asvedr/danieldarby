use crate::entities::ai::{AiSchema, StrategiesSchema};
use crate::impls::serialization::ai_deserializer::AiDeserializer;
use crate::proto::rate_check::IEstimator;
use crate::proto::serializer::IAiDeserializer;
use crate::{AiStrategyParams, Card};

struct MockEstimator {}

impl IEstimator for MockEstimator {
    fn estimate(&mut self, _: &[Card], _: &[Card], _: usize) -> f64 {
        0.5
    }
}

fn create_deserializer() -> Box<dyn IAiDeserializer> {
    let estimator_factory = |_: usize| MockEstimator {};
    Box::new(AiDeserializer { estimator_factory })
}

fn default_schema() -> (AiSchema, &'static str) {
    let schema = AiSchema {
        config: Default::default(),
        strategies: StrategiesSchema {
            bluff_va_banque: vec![],
            common_bluff: vec![(AiStrategyParams::default(), "mrse".to_string())],
            risky_gain: vec![],
            safe: vec![
                (
                    AiStrategyParams {
                        min_win_prob: Some(0.7),
                        ..Default::default()
                    },
                    "rse 0.9".to_string(),
                ),
                (
                    AiStrategyParams {
                        min_win_prob: Some(0.6),
                        ..Default::default()
                    },
                    "rse 20".to_string(),
                ),
                (AiStrategyParams::default(), "chca".to_string()),
            ],
        },
    };
    let expected = concat!(
        "AiConfig { prob_bluff_va_banque_mode: -1.0, prob_common_bluff_mode: -1.0, prob_risky_gain_mode: -1.0, estimator_tries: 1000 }\n",
        "bvb\n",
        "\n",
        "b\n",
        "  \"max_raise\":AiStrategyParams { max_win_prob: None, min_win_prob: None, min_cards: None, max_cards: None, min_bet: None, max_bet: None }\n",
        "rg\n",
        "\n",
        "s\n",
        "  \"raise(max_bet=Err(0.9))\":AiStrategyParams { max_win_prob: None, min_win_prob: Some(0.7), min_cards: None, max_cards: None, min_bet: None, max_bet: None }\n",
        "  \"raise(max_bet=Ok(20))\":AiStrategyParams { max_win_prob: None, min_win_prob: Some(0.6), min_cards: None, max_cards: None, min_bet: None, max_bet: None }\n",
        "  \"check/call\":AiStrategyParams { max_win_prob: None, min_win_prob: None, min_cards: None, max_cards: None, min_bet: None, max_bet: None }",
    );
    (schema, expected)
}

#[test]
fn test_create_ai() {
    let deserializer = create_deserializer();
    let (schema, expected) = default_schema();
    let ai = deserializer.schema_to_ai(&schema).unwrap();
    let info = ai.debug_info();
    assert_eq!(info, expected);
}

#[test]
fn test_json_to_ai() {
    let deserializer = create_deserializer();
    let (schema, expected) = default_schema();
    let js = deserializer.schema_to_json(&schema);
    let ai = deserializer.json_to_ai(&js).unwrap();
    let info = ai.debug_info();
    assert_eq!(info, expected);
}
