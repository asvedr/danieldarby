use crate::impls::estimator_probe::Estimator;
use crate::impls::rate_checker::RateChecker;
use crate::impls::rules;
use crate::impls::win_checker::WinChecker;
use crate::{Card, IEstimator};

const VARIANTS: &[(&str, &str, usize, f64)] = &[
    ("s6,d2,d9", "s9,s10", 3, 0.78),
    ("", "s9,s10", 3, 0.22),
    ("s6,s2,s3", "s4,s5", 2, 1.0),
];
const AVAILABLE_ERROR: f64 = 0.03;

#[test]
fn test_stable() {
    let mut estimator = Estimator::new(
        10_000,
        WinChecker {
            rate_checker: RateChecker::new(rules::all()),
        },
    );
    for (shared_s, user_s, enemy_count, _) in VARIANTS {
        let shared = Card::deserialize_vec(shared_s).unwrap();
        let user = Card::deserialize_vec(user_s).unwrap();
        let a = estimator.estimate(&shared, &user, *enemy_count);
        let b = estimator.estimate(&shared, &user, *enemy_count);
        let c = estimator.estimate(&shared, &user, *enemy_count);
        assert!(
            (a - b).abs() < AVAILABLE_ERROR && (b - c).abs() < AVAILABLE_ERROR,
            "Estimation is unstable on {:?},{:?},{}: {:?}",
            shared_s,
            user_s,
            enemy_count,
            (a, b, c)
        )
    }
}

#[test]
fn test_estimate() {
    let mut estimator = Estimator::new(
        10_000,
        WinChecker {
            rate_checker: RateChecker::new(rules::all()),
        },
    );
    for (shared_s, user_s, enemy_count, expected) in VARIANTS {
        let shared = Card::deserialize_vec(shared_s).unwrap();
        let user = Card::deserialize_vec(user_s).unwrap();
        let result = estimator.estimate(&shared, &user, *enemy_count);
        assert!(
            (result - expected).abs() < AVAILABLE_ERROR,
            "Invalid estimation on {:?},{:?},{}: {} != {}",
            shared_s,
            user_s,
            enemy_count,
            result,
            expected,
        )
    }
}
