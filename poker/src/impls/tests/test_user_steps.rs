use crate::entities::constants::{BIG_BLIND, SMALL_BLIND};
use crate::entities::desk::Card;
use crate::entities::game::{GameState, RoundStateKind, Step};
use crate::impls::tests::test_game::controller;
use crate::proto::game::IGame;

fn setup() -> (Box<dyn IGame>, GameState) {
    let ctrl = controller();
    let players = vec!["a".to_string(), "b".to_string()];
    let mut game = ctrl.new_game(players, 100).unwrap();
    game.round.state = RoundStateKind::NewBet {
        bet: BIG_BLIND,
        player: 0,
    };
    game.round.current_player = 1;
    (ctrl, game)
}

#[test]
fn test_first_step() {
    let (ctrl, game) = setup();
    assert!(ctrl.get_steps(&game, 0).is_empty());
    let steps = ctrl.get_steps(&game, 1);
    let expected = &[
        Step::Fold,
        Step::Call,
        Step::Raise(SMALL_BLIND),     // 5
        Step::Raise(SMALL_BLIND * 2), // 10
        Step::Raise(SMALL_BLIND * 3), // 15
        Step::Raise(SMALL_BLIND * 4), // 20
        Step::Raise(100 - BIG_BLIND), // 90
    ];
    assert_eq!(steps, expected);
}

#[test]
fn test_second_step_call() {
    let (ctrl, game) = setup();
    let game = ctrl.apply_user_step(&game, 1, Step::Call).unwrap();
    let steps = ctrl.get_steps(&game, 0);
    let expected = &[
        Step::Fold,
        Step::Check,
        Step::Raise(SMALL_BLIND),     // 5
        Step::Raise(SMALL_BLIND * 2), // 10
        Step::Raise(SMALL_BLIND * 3), // 15
        Step::Raise(SMALL_BLIND * 4), // 20
        Step::Raise(100 - BIG_BLIND), // 90
    ];
    assert_eq!(steps, expected);
}

#[test]
fn test_third_step_check() {
    let (ctrl, game) = setup();
    let game = ctrl.apply_user_step(&game, 1, Step::Call).unwrap();
    let game = ctrl.apply_user_step(&game, 0, Step::Check).unwrap();
    let steps = ctrl.get_steps(&game, 1);
    let expected = &[
        Step::Fold,
        Step::Check,
        Step::Raise(SMALL_BLIND),     // 5
        Step::Raise(SMALL_BLIND * 2), // 10
        Step::Raise(SMALL_BLIND * 3), // 15
        Step::Raise(SMALL_BLIND * 4), // 20
        Step::Raise(100 - BIG_BLIND), // 90
    ];
    assert_eq!(steps, expected);
    assert_eq!(game.round.players[0].bet, BIG_BLIND);
    assert_eq!(game.round.players[1].bet, BIG_BLIND);
}

#[test]
fn test_ready_to_new_card() {
    let (ctrl, game) = setup();
    let game = ctrl.apply_user_step(&game, 1, Step::Call).unwrap();
    let game = ctrl.apply_user_step(&game, 0, Step::Check).unwrap();
    let game = ctrl.apply_user_step(&game, 1, Step::Check).unwrap();
    assert_eq!(game.round.state, RoundStateKind::NewCard { bet: BIG_BLIND });
}

#[test]
fn test_ready_to_new_check_all() {
    let (ctrl, mut game) = setup();
    game.round.shared_cards = Card::deserialize_vec("s6,d6,h6").unwrap();
    let game = ctrl.apply_user_step(&game, 1, Step::Call).unwrap();
    let game = ctrl.apply_user_step(&game, 0, Step::Check).unwrap();
    let game = ctrl.apply_user_step(&game, 1, Step::Check).unwrap();
    assert_eq!(game.round.state, RoundStateKind::OpenAll);
}

#[test]
fn test_raise() {
    let (ctrl, game) = setup();
    let game = ctrl.apply_user_step(&game, 1, Step::Raise(10)).unwrap();
    let steps = ctrl.get_steps(&game, 0);
    let expected = &[
        Step::Fold,
        Step::Call,
        Step::Raise(SMALL_BLIND),
        Step::Raise(SMALL_BLIND * 2),
        Step::Raise(SMALL_BLIND * 3),
        Step::Raise(SMALL_BLIND * 4),
        Step::Raise(100 - (BIG_BLIND + 10)),
    ];
    assert_eq!(steps, expected);
    assert_eq!(game.round.players[1].bet, BIG_BLIND + 10);
}

#[test]
fn test_raise_call() {
    let (ctrl, game) = setup();
    let game = ctrl.apply_user_step(&game, 1, Step::Raise(10)).unwrap();
    let game = ctrl.apply_user_step(&game, 0, Step::Call).unwrap();
    let steps = ctrl.get_steps(&game, 1);
    let expected = &[
        Step::Fold,
        Step::Check,
        Step::Raise(SMALL_BLIND),
        Step::Raise(SMALL_BLIND * 2),
        Step::Raise(SMALL_BLIND * 3),
        Step::Raise(SMALL_BLIND * 4),
        Step::Raise(100 - (BIG_BLIND + 10)),
    ];
    assert_eq!(steps, expected);
    assert_eq!(game.round.players[0].bet, BIG_BLIND + 10);
    assert_eq!(game.round.players[1].bet, BIG_BLIND + 10);
}

#[test]
fn test_raise_call_raise_call_check_check() {
    let (ctrl, game) = setup();
    let game = ctrl.apply_user_step(&game, 1, Step::Raise(10)).unwrap();
    let game = ctrl.apply_user_step(&game, 0, Step::Call).unwrap();
    let game = ctrl.apply_user_step(&game, 1, Step::Raise(15)).unwrap();
    let game = ctrl.apply_user_step(&game, 0, Step::Call).unwrap();
    let game = ctrl.apply_user_step(&game, 1, Step::Check).unwrap();
    let game = ctrl.apply_user_step(&game, 0, Step::Check).unwrap();
    assert_eq!(
        game.round.state,
        RoundStateKind::NewCard {
            bet: BIG_BLIND + 10 + 15
        },
    );
    assert_eq!(game.round.dealer, 0);
    assert_eq!(game.round.players[0].bet, BIG_BLIND + 10 + 15);
    assert_eq!(game.round.players[1].bet, BIG_BLIND + 10 + 15);
}

#[test]
fn test_raise_call_raise_fold() {
    let (ctrl, game) = setup();
    let game = ctrl.apply_user_step(&game, 1, Step::Raise(10)).unwrap();
    let game = ctrl.apply_user_step(&game, 0, Step::Call).unwrap();
    let game = ctrl.apply_user_step(&game, 1, Step::Raise(10)).unwrap();
    let game = ctrl.apply_user_step(&game, 0, Step::Fold).unwrap();
    assert_eq!(game.round.state, RoundStateKind::AllFolded);
    assert!(!game.round.players[0].active);
    assert_eq!(game.round.players[0].bet, BIG_BLIND + 10);
    assert!(game.round.players[1].active);
    assert_eq!(game.round.players[1].bet, BIG_BLIND + 10 + 10);
}
