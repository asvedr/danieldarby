use crate::entities::constants::{BIG_BLIND, SMALL_BLIND};
use crate::entities::game::{ExpectedAction, GameState, RoundStateKind, Step};
use crate::impls::tests::test_game::controller;
use crate::proto::game::IGame;

fn setup() -> (Box<dyn IGame>, GameState) {
    let ctrl = controller();
    let players = vec!["a".to_string(), "b".to_string()];
    let game = ctrl.new_game(players, 100).unwrap();
    (ctrl, game)
}

#[test]
fn test_big_blind() {
    let (ctrl, game) = setup();
    assert_eq!(
        ctrl.get_expected_action(&game),
        ExpectedAction::AutoStep(RoundStateKind::BigBlind),
    );
    let game = ctrl.apply_auto_step(&game).unwrap();
    assert_eq!(game.round.state, RoundStateKind::SmallBlinds);
    assert_eq!(game.round.players[0].bet, BIG_BLIND);
    assert_eq!(game.round.bank(), BIG_BLIND);
}

#[test]
fn test_small_blind() {
    let (ctrl, game) = setup();
    let game = ctrl.apply_auto_step(&game).unwrap();
    let game = ctrl.apply_auto_step(&game).unwrap();
    assert_eq!(
        game.round.state,
        RoundStateKind::NewBet {
            bet: BIG_BLIND,
            player: 0
        },
    );
    assert_eq!(game.round.players[1].bet, SMALL_BLIND);
    assert_eq!(game.round.bank(), BIG_BLIND + SMALL_BLIND);
}

#[test]
fn test_new_card() {
    let (ctrl, game) = setup();
    // big blind
    let game = ctrl.apply_auto_step(&game).unwrap();
    // small blinds
    let game = ctrl.apply_auto_step(&game).unwrap();
    let game = ctrl.apply_user_step(&game, 1, Step::Call).unwrap();
    let game = ctrl.apply_user_step(&game, 0, Step::Check).unwrap();
    let game = ctrl.apply_user_step(&game, 1, Step::Check).unwrap();
    // expected new card
    assert_eq!(game.round.state, RoundStateKind::NewCard { bet: BIG_BLIND });
    let game = ctrl.apply_auto_step(&game).unwrap();
    assert_eq!(
        game.round.state,
        RoundStateKind::Check {
            bet: BIG_BLIND,
            player: 0
        },
    );
    assert_eq!(game.round.bank(), BIG_BLIND * 2);
    assert_eq!(game.round.shared_cards.len(), 1);
}
