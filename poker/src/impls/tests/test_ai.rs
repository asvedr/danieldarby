use crate::entities::ai::{
    AiCheckStrategyParams, AiConfig, AiRoundMode, AiState, AiStrategyParams,
};
use crate::entities::game::{PlayerInfo, SharedGameInfo, Step, UserGameInfo};
use crate::impls::ai::{Ai, Strategies};
use crate::impls::ai_strategies::AiStr;
use crate::proto::game::{IAi, IAiStrategy};
use crate::{Card, IEstimator};

const CALL_STEPS: &[Step] = &[
    Step::Fold,
    Step::Call,
    Step::Raise(5),
    Step::Raise(10),
    Step::Raise(15),
    Step::Raise(100),
];

struct MockStrategy {
    name: &'static str,
    result: Step,
}

struct MockEstimator {
    results: Vec<f64>,
}

impl IAiStrategy for MockStrategy {
    fn name(&self) -> String {
        self.name.to_string()
    }

    fn choose_step(&self, _: &AiCheckStrategyParams, _: &[Step]) -> Step {
        self.result.clone()
    }
}

impl IEstimator for MockEstimator {
    fn estimate(&mut self, _: &[Card], _: &[Card], _: usize) -> f64 {
        let val = self.results[0];
        self.results.remove(0);
        val
    }
}

fn user_game_info() -> UserGameInfo {
    UserGameInfo {
        shared_info: SharedGameInfo {
            players: vec![
                PlayerInfo {
                    name: "A".to_string(),
                    has_cards: false,
                    balance: 100,
                    bet: 20,
                },
                PlayerInfo {
                    name: "B".to_string(),
                    has_cards: false,
                    balance: 100,
                    bet: 20,
                },
            ],
            cards: Card::deserialize_vec("").unwrap(),
            bank: 40,
            dealer: 0,
        },
        id: 0,
        cards: Card::deserialize_vec("").unwrap(),
    }
}

#[test]
fn test_choose_strategy_init_state() {
    let bluff_va_banque = vec![AiStr::random(AiStrategyParams::default())];
    let ai = Ai::new(
        AiConfig {
            prob_bluff_va_banque_mode: 1.0,
            ..Default::default()
        },
        MockEstimator { results: vec![0.5] },
        Strategies {
            bluff_va_banque,
            ..Default::default()
        },
    );
    let mut state = AiState::default();
    ai.choose_step(&user_game_info(), &mut state, &CALL_STEPS);
    assert!(!state.init_required);
    assert_eq!(state.mode, AiRoundMode::BluffVaBanque);
}

#[test]
fn test_choose_mode() {
    let count = 5000;
    let ai = Ai::new(
        AiConfig {
            prob_bluff_va_banque_mode: 0.1,
            prob_common_bluff_mode: 0.2,
            prob_risky_gain_mode: 0.25,
            estimator_tries: 10_000,
        },
        MockEstimator {
            results: vec![0.5; count],
        },
        Strategies {
            bluff_va_banque: vec![AiStr::random(AiStrategyParams::default())],
            common_bluff: vec![AiStr::raise(AiStrategyParams::default(), Ok(20))],
            risky_gain: vec![AiStr::max_raise(AiStrategyParams::default())],
            safe: vec![AiStr::check_call(AiStrategyParams::default())],
        },
    );
    let mut va_banque = 0.0;
    let mut common_bluff = 0.0;
    let mut std = 0.0;
    let mut gain = 0.0;
    for _ in 0..count {
        let mut state = AiState::default();
        let chosen = ai.choose_step(&user_game_info(), &mut state, &CALL_STEPS);
        match state.mode {
            AiRoundMode::BluffVaBanque => {
                assert_eq!(
                    chosen.info,
                    "estimation: 0.5, mode: BluffVaBanque, strategy: \"random\""
                );
                va_banque += 1.0
            }
            AiRoundMode::CommonBluff => {
                assert_eq!(
                    chosen.info,
                    "estimation: 0.5, mode: CommonBluff, strategy: \"raise(max_bet=Ok(20))\""
                );
                common_bluff += 1.0;
            }
            AiRoundMode::Std => {
                assert_eq!(
                    chosen.info,
                    "estimation: 0.5, mode: Std, strategy: \"check/call\""
                );
                std += 1.0
            }
            AiRoundMode::RiskyGain => {
                assert_eq!(
                    chosen.info,
                    "estimation: 0.5, mode: RiskyGain, strategy: \"max_raise\""
                );
                gain += 1.0
            }
        }
    }
    let fcount = count as f64;
    println!(
        "{} {} {} {}",
        va_banque / fcount,
        common_bluff / fcount,
        gain / fcount,
        std / fcount
    );
    assert!(((va_banque / fcount) - 0.1).abs() < 0.02);
    assert!(((common_bluff / fcount) - 0.2).abs() < 0.02);
    assert!(((gain / fcount) - 0.25).abs() < 0.02);
    assert!(((std / fcount) - 0.45).abs() < 0.02);
}

#[test]
fn test_choose_by_prob() {
    let ai = Ai::new(
        AiConfig::default(),
        MockEstimator {
            results: vec![0.1, 0.5, 0.7],
        },
        Strategies {
            safe: vec![
                AiStr::check_call(AiStrategyParams {
                    max_win_prob: Some(0.6),
                    min_win_prob: Some(0.4),
                    ..Default::default()
                }),
                AiStr::max_raise(AiStrategyParams {
                    min_win_prob: Some(0.6),
                    ..Default::default()
                }),
                AiStr::fold(AiStrategyParams::default()),
            ],
            ..Default::default()
        },
    );
    let chosen = ai.choose_step(&user_game_info(), &mut AiState::default(), &CALL_STEPS);
    assert_eq!(chosen.step, Step::Fold);
    let chosen = ai.choose_step(&user_game_info(), &mut AiState::default(), &CALL_STEPS);
    assert_eq!(chosen.step, Step::Call);
    let chosen = ai.choose_step(&user_game_info(), &mut AiState::default(), &CALL_STEPS);
    assert!(matches!(chosen.step, Step::Raise(_)))
}

#[test]
fn test_choose_by_cards() {
    let ai = Ai::new(
        AiConfig::default(),
        MockEstimator {
            results: vec![0.5, 0.5, 0.5],
        },
        Strategies {
            safe: vec![
                AiStr::check_call(AiStrategyParams {
                    max_cards: Some(2),
                    min_cards: Some(1),
                    ..Default::default()
                }),
                AiStr::max_raise(AiStrategyParams {
                    min_cards: Some(2),
                    ..Default::default()
                }),
                AiStr::fold(AiStrategyParams::default()),
            ],
            ..Default::default()
        },
    );
    let mut info = user_game_info();
    info.shared_info.cards = vec![];
    let chosen = ai.choose_step(&info, &mut AiState::default(), &CALL_STEPS);
    assert_eq!(chosen.step, Step::Fold);
    info.shared_info.cards = Card::deserialize_vec("c6,c7").unwrap();
    let chosen = ai.choose_step(&info, &mut AiState::default(), &CALL_STEPS);
    assert_eq!(chosen.step, Step::Call);
    info.shared_info.cards = Card::deserialize_vec("c6,c7,c8").unwrap();
    let chosen = ai.choose_step(&info, &mut AiState::default(), &CALL_STEPS);
    assert!(matches!(chosen.step, Step::Raise(_)))
}

#[test]
fn test_choose_by_bet() {
    let ai = Ai::new(
        AiConfig::default(),
        MockEstimator {
            results: vec![0.1, 0.1, 0.1],
        },
        Strategies {
            safe: vec![
                AiStr::check_call(AiStrategyParams {
                    max_bet: Some(Ok(160)),
                    min_bet: Some(Ok(140)),
                    ..Default::default()
                }),
                AiStr::max_raise(AiStrategyParams {
                    min_bet: Some(Ok(161)),
                    ..Default::default()
                }),
                AiStr::fold(AiStrategyParams::default()),
            ],
            ..Default::default()
        },
    );
    let mut info = user_game_info();
    info.shared_info.players[info.id].bet = 100;
    for player in info.shared_info.players.iter_mut() {
        player.balance = 1000;
    }
    let opponent = (info.id + 1) % info.shared_info.players.len();
    info.shared_info.players[opponent].bet = 145;
    let chosen = ai.choose_step(&info, &mut AiState::default(), &CALL_STEPS);
    assert_eq!(chosen.step, Step::Call);
    info.shared_info.players[opponent].bet = 170;
    let chosen = ai.choose_step(&info, &mut AiState::default(), &CALL_STEPS);
    assert_eq!(chosen.step, Step::Raise(100));
    info.shared_info.players[opponent].bet = 120;
    let chosen = ai.choose_step(&user_game_info(), &mut AiState::default(), &CALL_STEPS);
    assert_eq!(chosen.step, Step::Fold);
}
