use crate::entities::ai::{AiCheckStrategyParams, AiRoundMode, AiStrategyParams};
use crate::entities::game::Step;
use crate::impls::ai_strategies::AiStr;

fn random_params() -> AiCheckStrategyParams {
    let modes = [
        AiRoundMode::BluffVaBanque,
        AiRoundMode::CommonBluff,
        AiRoundMode::Std,
        AiRoundMode::RiskyGain,
    ];
    AiCheckStrategyParams {
        win_estimation: fastrand::f64(),
        current_bet: fastrand::usize(5..100),
        expected_bet: fastrand::usize(5..100),
        balance: fastrand::usize(..100),
        mode: modes[fastrand::usize(..modes.len())],
        check_loop: fastrand::bool(),
        cards_on_desk: fastrand::usize(..4),
    }
}

const ALL_STEPS: &[Step] = &[
    Step::Fold,
    Step::Check,
    Step::Call,
    Step::Raise(5),
    Step::Raise(10),
    Step::Raise(15),
    Step::Raise(100),
];

#[test]
fn test_fold() {
    let (_, strategy) = AiStr::fold(AiStrategyParams::default());
    assert_eq!(strategy.name(), "fold");
    for _ in 0..50 {
        let mut steps = ALL_STEPS.iter().cloned().collect::<Vec<_>>();
        fastrand::shuffle(&mut steps);
        let chosen = strategy.choose_step(&random_params(), &steps);
        assert_eq!(chosen, Step::Fold);
    }
}

#[test]
fn test_check_call() {
    let (_, strategy) = AiStr::check_call(AiStrategyParams::default());
    assert_eq!(strategy.name(), "check/call");
    for _ in 0..50 {
        let mut ch_steps = ALL_STEPS
            .iter()
            .filter(|step| **step != Step::Call)
            .cloned()
            .collect::<Vec<_>>();
        let mut cl_steps = ALL_STEPS
            .iter()
            .filter(|step| **step != Step::Check)
            .cloned()
            .collect::<Vec<_>>();
        fastrand::shuffle(&mut ch_steps);
        fastrand::shuffle(&mut cl_steps);
        let params = random_params();
        let step = strategy.choose_step(&params, &ch_steps);
        assert_eq!(step, Step::Check);
        let step = strategy.choose_step(&params, &cl_steps);
        assert_eq!(step, Step::Call);
    }
}

#[test]
fn test_raise_abs_limit_5_15() {
    let (_, strategy) = AiStr::raise(AiStrategyParams::default(), Ok(20));
    assert_eq!(strategy.name(), "raise(max_bet=Ok(20))");
    for _ in 0..50 {
        let mut steps = ALL_STEPS.iter().cloned().collect::<Vec<_>>();
        fastrand::shuffle(&mut steps);
        let mut params = random_params();
        params.current_bet = 0;
        let raise = match strategy.choose_step(&params, &steps) {
            Step::Raise(val) => val,
            _ => panic!(),
        };
        assert!(raise >= 5 && raise <= 15);
    }
}

#[test]
fn test_raise_abs_limit_only_5() {
    let (_, strategy) = AiStr::raise(AiStrategyParams::default(), Ok(20));
    assert_eq!(strategy.name(), "raise(max_bet=Ok(20))");
    for _ in 0..50 {
        let mut steps = ALL_STEPS.iter().cloned().collect::<Vec<_>>();
        fastrand::shuffle(&mut steps);
        let mut params = random_params();
        params.current_bet = 15;
        match strategy.choose_step(&params, &steps) {
            Step::Raise(5) => (),
            _ => panic!(),
        };
    }
}

#[test]
fn test_raise_abs_limit_check() {
    let (_, strategy) = AiStr::raise(AiStrategyParams::default(), Ok(20));
    assert_eq!(strategy.name(), "raise(max_bet=Ok(20))");
    for _ in 0..50 {
        let mut steps = ALL_STEPS.iter().cloned().collect::<Vec<_>>();
        fastrand::shuffle(&mut steps);
        let mut params = random_params();
        params.current_bet = 25;
        let step = strategy.choose_step(&params, &steps);
        let status = matches!(step, Step::Check | Step::Call);
        assert!(status, "{:?}", step);
    }
}

#[test]
fn test_raise_rel_level_5() {
    let (_, strategy) = AiStr::raise(AiStrategyParams::default(), Err(0.5));
    assert_eq!(strategy.name(), "raise(max_bet=Err(0.5))");
    for _ in 0..50 {
        let mut steps = ALL_STEPS.iter().cloned().collect::<Vec<_>>();
        fastrand::shuffle(&mut steps);
        let mut params = random_params();
        params.balance = 30;
        params.current_bet = 10;
        match strategy.choose_step(&params, &steps) {
            Step::Raise(5) => (),
            _ => panic!(),
        };
    }
}

#[test]
fn test_raise_rel_check() {
    let (_, strategy) = AiStr::raise(AiStrategyParams::default(), Err(0.5));
    assert_eq!(strategy.name(), "raise(max_bet=Err(0.5))");
    for _ in 0..50 {
        let mut steps = ALL_STEPS.iter().cloned().collect::<Vec<_>>();
        fastrand::shuffle(&mut steps);
        let mut params = random_params();
        params.balance = 30;
        params.current_bet = 20;
        let step = strategy.choose_step(&params, &steps);
        assert!(matches!(step, Step::Check | Step::Call), "{:?}", step);
    }
}

#[test]
fn test_max_raise() {
    let (_, strategy) = AiStr::max_raise(AiStrategyParams::default());
    assert_eq!(strategy.name(), "max_raise");
    for _ in 0..50 {
        let mut steps = ALL_STEPS.iter().cloned().collect::<Vec<_>>();
        fastrand::shuffle(&mut steps);
        let params = random_params();
        match strategy.choose_step(&params, &steps) {
            Step::Raise(_) => (),
            _ => panic!(),
        }
    }
}
