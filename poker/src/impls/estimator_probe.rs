use crate::entities::constants::MAX_SHARED_CARDS;

use crate::entities::desk::Card;
use crate::proto::rate_check::{IEstimator, IWinChecker};

pub struct Estimator<WC: IWinChecker> {
    experiment_count: usize,
    win_checker: WC,
    all_cards: Vec<Card>,
}

impl<WC: IWinChecker> Estimator<WC> {
    pub fn new(experiment_count: usize, win_checker: WC) -> Self {
        Self {
            experiment_count,
            win_checker,
            all_cards: Card::all(),
        }
    }

    fn take_card(&self, head: usize, blacklist: &[Card]) -> (Card, usize) {
        for i in head..self.all_cards.len() {
            if !blacklist.contains(&self.all_cards[i]) {
                return (self.all_cards[i], i + 1);
            }
        }
        panic!("out of cards")
    }

    fn complete_shared(
        &self,
        shared: &[Card],
        player_cards: &[Card],
        used: &mut Vec<Card>,
    ) -> (Vec<Card>, usize) {
        let mut result = shared.to_vec();
        let mut head = 0;
        used.extend_from_slice(player_cards);
        for _ in 0..MAX_SHARED_CARDS - shared.len() {
            let (card, head_) = self.take_card(head, used);
            head = head_;
            used.push(card);
            result.push(card);
        }
        (result, head)
    }

    fn fill_enemies(
        &self,
        hands: &mut Vec<(usize, Vec<Card>)>,
        count: usize,
        used: &mut [Card],
        mut head: usize,
    ) {
        for i in 0..count {
            let (card_a, head_) = self.take_card(head, used);
            let (card_b, head_) = self.take_card(head_, used);
            head = head_;
            hands.push((i + 1, vec![card_a, card_b]))
        }
    }

    fn dice(&self, shared_cards: &[Card], player_cards: &[Card], enemies_count: usize) -> bool {
        let mut used = shared_cards.to_vec();
        used.extend_from_slice(player_cards);
        let mut hands = vec![(0, player_cards.to_vec())];
        let winner = if shared_cards.len() == MAX_SHARED_CARDS {
            self.fill_enemies(&mut hands, enemies_count, &mut used, 0);
            self.win_checker.get_status(shared_cards, &hands)
        } else {
            let (shared_cards, head) = self.complete_shared(shared_cards, player_cards, &mut used);
            self.fill_enemies(&mut hands, enemies_count, &mut used, head);
            self.win_checker.get_status(&shared_cards, &hands)
        };
        winner == 0
    }
}

impl<WC: IWinChecker> IEstimator for Estimator<WC> {
    fn estimate(&mut self, shared_cards: &[Card], player_cards: &[Card], enemies: usize) -> f64 {
        let mut succ = 0;
        for _ in 0..self.experiment_count {
            fastrand::shuffle(&mut self.all_cards);
            let is_win = self.dice(shared_cards, player_cards, enemies);
            if is_win {
                succ += 1
            }
        }
        succ as f64 / self.experiment_count as f64
    }
}
