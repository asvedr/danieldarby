use std::cell::RefCell;

use crate::entities::ai::{
    AiCheckStrategyParams, AiConfig, AiRoundMode, AiState, AiStrategyParams,
};
use crate::entities::constants::MAX_SHARED_CARDS;
use crate::entities::game::{ChosenStep, Step, UserGameInfo};
use crate::proto::game::{IAi, IAiStrategy};
use crate::proto::rate_check::IEstimator;

pub type ParametrizedStrategy = (AiStrategyParams, Box<dyn IAiStrategy>);

#[derive(Default)]
pub struct Strategies {
    pub bluff_va_banque: Vec<ParametrizedStrategy>,
    pub common_bluff: Vec<ParametrizedStrategy>,
    pub risky_gain: Vec<ParametrizedStrategy>,
    pub safe: Vec<ParametrizedStrategy>,
}

pub struct Ai<Es> {
    config: AiConfig,
    estimator: RefCell<Es>,
    strategies: Strategies,
}

impl<Es> Ai<Es> {
    pub(crate) fn new(config: AiConfig, estimator: Es, strategies: Strategies) -> Self {
        Self {
            config,
            estimator: RefCell::new(estimator),
            strategies,
        }
    }

    fn init_ai_state(&self, ai_state: &mut AiState) {
        ai_state.init_required = false;
        ai_state.mode = self.choose_round_mode();
    }

    fn choose_round_mode(&self) -> AiRoundMode {
        let mut rnd = fastrand::f64();
        let seq = [
            (
                self.config.prob_bluff_va_banque_mode,
                AiRoundMode::BluffVaBanque,
            ),
            (self.config.prob_common_bluff_mode, AiRoundMode::CommonBluff),
            (self.config.prob_risky_gain_mode, AiRoundMode::RiskyGain),
        ];
        for (prob, mode) in seq {
            if rnd < prob {
                return mode;
            }
            rnd -= prob
        }
        AiRoundMode::Std
    }

    fn choose_strategy(&self, params: &AiCheckStrategyParams) -> &dyn IAiStrategy {
        let sequence = match params.mode {
            AiRoundMode::BluffVaBanque => &self.strategies.bluff_va_banque,
            AiRoundMode::CommonBluff => &self.strategies.common_bluff,
            AiRoundMode::Std => &self.strategies.safe,
            AiRoundMode::RiskyGain => &self.strategies.risky_gain,
        };
        for (required, strategy) in sequence {
            if Self::matches(required, params) && strategy.matches(params) {
                return &**strategy;
            }
        }
        panic!("No strategy matched: {:?}", params)
    }

    #[inline]
    fn matches(required: &AiStrategyParams, got: &AiCheckStrategyParams) -> bool {
        let min_cards = required.min_cards.unwrap_or(0);
        let max_cards = required.max_cards.unwrap_or(MAX_SHARED_CARDS);
        let min_prob = required.min_win_prob.unwrap_or(-1.0);
        let max_prob = required.max_win_prob.unwrap_or(2.0);
        let min_bet = Self::get_int_bet(required.min_bet.unwrap_or(Ok(0)), got);
        let max_bet = Self::get_int_bet(required.max_bet.unwrap_or(Ok(got.balance)), got);
        got.cards_on_desk >= min_cards
            && got.cards_on_desk <= max_cards
            && got.win_estimation >= min_prob
            && got.win_estimation <= max_prob
            && got.expected_bet >= min_bet
            && got.expected_bet <= max_bet
    }

    fn get_int_bet(bet: Result<usize, f64>, params: &AiCheckStrategyParams) -> usize {
        match bet {
            Ok(val) => val,
            Err(coef) => (coef * (params.balance as f64)) as usize,
        }
    }
}

impl<Es: IEstimator> IAi for Ai<Es> {
    fn choose_step(
        &self,
        game_state: &UserGameInfo,
        ai_state: &mut AiState,
        steps: &[Step],
    ) -> ChosenStep {
        if steps.len() == 1 {
            let info = "estimation: N/A, mode: one_step, strategy: N/A".to_string();
            return ChosenStep {
                step: steps[0].clone(),
                info,
            };
        }
        if ai_state.init_required {
            self.init_ai_state(ai_state)
        }
        let enemies = game_state
            .shared_info
            .players
            .iter()
            .filter(|p| p.has_cards)
            .count()
            .max(1)
            - 1;
        let win_estimation = self.estimator.borrow_mut().estimate(
            &game_state.shared_info.cards,
            &game_state.cards,
            enemies,
        );
        let id = game_state.id;
        let match_params = AiCheckStrategyParams {
            win_estimation,
            current_bet: game_state.shared_info.players[id].bet,
            expected_bet: game_state.shared_info.get_max_bet(),
            balance: game_state.shared_info.players[id].balance,
            mode: ai_state.mode,
            check_loop: steps.contains(&Step::Check),
            cards_on_desk: game_state.shared_info.cards.len(),
        };
        let strategy = self.choose_strategy(&match_params);
        let step = strategy.choose_step(&match_params, steps);
        let info = format!(
            "estimation: {}, mode: {:?}, strategy: {:?}",
            win_estimation,
            ai_state.mode,
            strategy.name(),
        );
        ChosenStep { step, info }
    }

    #[cfg(test)]
    fn debug_info(&self) -> String {
        fn show(strs: &[ParametrizedStrategy]) -> String {
            strs.iter()
                .map(|(params, s)| format!("  {:?}:{:?}", s.name(), params))
                .collect::<Vec<_>>()
                .join("\n")
        }

        format!(
            "{:?}\nbvb\n{}\nb\n{}\nrg\n{}\ns\n{}",
            self.config,
            show(&self.strategies.bluff_va_banque),
            show(&self.strategies.common_bluff),
            show(&self.strategies.risky_gain),
            show(&self.strategies.safe),
        )
    }
}
