use crate::entities::ai::AiStrategyParams;
use crate::proto::game::IAiStrategy;

pub mod check_call;
pub mod fold;
pub mod max_raise;
pub mod raise;
pub mod random;

pub struct AiStr {}

impl AiStr {
    pub fn random(p: AiStrategyParams) -> (AiStrategyParams, Box<dyn IAiStrategy>) {
        (p, Box::new(random::AiStrRandom::default()))
    }

    pub fn fold(p: AiStrategyParams) -> (AiStrategyParams, Box<dyn IAiStrategy>) {
        (p, Box::new(fold::AiStrFold::default()))
    }

    pub fn check_call(p: AiStrategyParams) -> (AiStrategyParams, Box<dyn IAiStrategy>) {
        (p, Box::new(check_call::AiStrCheckCall::default()))
    }

    pub fn raise(
        p: AiStrategyParams,
        max_bet: Result<usize, f64>,
    ) -> (AiStrategyParams, Box<dyn IAiStrategy>) {
        (p, Box::new(raise::AiStrRaise { max_bet }))
    }

    pub fn max_raise(p: AiStrategyParams) -> (AiStrategyParams, Box<dyn IAiStrategy>) {
        (p, Box::new(max_raise::AiStrMaxRaise::default()))
    }
}
