use crate::entities::ai::AiCheckStrategyParams;
use crate::entities::game::Step;
use crate::proto::game::IAiStrategy;

pub(crate) struct AiStrRaise {
    pub(crate) max_bet: Result<usize, f64>,
}

impl AiStrRaise {
    fn get_max_bet(&self, params: &AiCheckStrategyParams) -> usize {
        match self.max_bet {
            Ok(val) => val,
            Err(percent) => (params.balance as f64 * percent) as usize,
        }
    }

    fn get_check_or_call(steps: &[Step]) -> Step {
        for step in steps {
            if matches!(step, Step::Call | Step::Check) {
                return step.clone();
            }
        }
        unreachable!()
    }

    fn filter_raises(steps: &[Step], current_bet: usize, max_bet: usize) -> Vec<Step> {
        steps
            .iter()
            .filter(|step| {
                matches!(
                    step,
                    Step::Raise(bet) if current_bet + bet <= max_bet,
                )
            })
            .cloned()
            .collect::<Vec<_>>()
    }
}

impl IAiStrategy for AiStrRaise {
    fn matches(&self, params: &AiCheckStrategyParams) -> bool {
        let max_bet = self.get_max_bet(params);
        params.expected_bet <= max_bet
    }

    fn name(&self) -> String {
        format!("raise(max_bet={:?})", self.max_bet)
    }

    fn choose_step(&self, params: &AiCheckStrategyParams, steps: &[Step]) -> Step {
        let max_bet = self.get_max_bet(params);
        let current_bet = params.current_bet;
        let raises = Self::filter_raises(steps, current_bet, max_bet);
        if raises.is_empty() {
            Self::get_check_or_call(steps)
        } else {
            raises[fastrand::usize(..raises.len())].clone()
        }
    }
}
