use crate::entities::ai::AiCheckStrategyParams;
use crate::entities::game::Step;
use crate::proto::game::IAiStrategy;

#[derive(Default)]
pub struct AiStrCheckCall {}

impl IAiStrategy for AiStrCheckCall {
    fn name(&self) -> String {
        "check/call".to_string()
    }

    fn choose_step(&self, _: &AiCheckStrategyParams, steps: &[Step]) -> Step {
        for step in steps {
            if matches!(step, Step::Call | Step::Check) {
                return step.clone();
            }
        }
        unreachable!()
    }
}
