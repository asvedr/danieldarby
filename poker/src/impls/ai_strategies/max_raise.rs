use crate::entities::ai::AiCheckStrategyParams;
use crate::entities::game::Step;
use crate::proto::game::IAiStrategy;

#[derive(Default)]
pub(crate) struct AiStrMaxRaise {}

impl IAiStrategy for AiStrMaxRaise {
    fn name(&self) -> String {
        "max_raise".to_string()
    }

    fn choose_step(&self, _: &AiCheckStrategyParams, steps: &[Step]) -> Step {
        steps.iter().max_by_key(|s| s.as_isize()).unwrap().clone()
    }
}
