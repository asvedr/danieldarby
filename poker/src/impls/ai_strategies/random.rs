use crate::entities::ai::AiCheckStrategyParams;
use crate::entities::game::Step;
use crate::proto::game::IAiStrategy;

#[derive(Default)]
pub(crate) struct AiStrRandom {}

impl IAiStrategy for AiStrRandom {
    fn name(&self) -> String {
        "random".to_string()
    }

    fn choose_step(&self, _: &AiCheckStrategyParams, steps: &[Step]) -> Step {
        let index = fastrand::usize(..steps.len());
        steps[index].clone()
    }
}
