use crate::entities::ai::AiCheckStrategyParams;
use crate::entities::game::Step;
use crate::proto::game::IAiStrategy;

#[derive(Default)]
pub(crate) struct AiStrFold {}

impl IAiStrategy for AiStrFold {
    fn name(&self) -> String {
        "fold".to_string()
    }

    fn choose_step(&self, _: &AiCheckStrategyParams, steps: &[Step]) -> Step {
        assert!(steps.contains(&Step::Fold));
        Step::Fold
    }
}
