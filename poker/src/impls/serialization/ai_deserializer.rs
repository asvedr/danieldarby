use std::str::FromStr;

use json::object;
use json::object::Object;
use json::JsonValue;

use crate::entities::ai::{AiConfig, AiRoundMode, AiSchema, StrategiesSchema};
use crate::entities::errors::SerializerError;
use crate::impls::ai::{Ai, ParametrizedStrategy, Strategies};
use crate::impls::ai_strategies;
use crate::proto::game::{IAi, IAiStrategy};
use crate::proto::rate_check::IEstimator;
use crate::proto::serializer::IAiDeserializer;
use crate::utils::json::{
    convert_arr_or_empty, get_bool, get_obj, get_real, get_str, get_uint, get_uint_or_real,
    item_to_obj, nf_to_default, nf_to_none, top_level_to_obj,
};
use crate::{AiState, AiStrategyParams};

/*
text format:
    va_banque:
 */

pub struct AiDeserializer<Es: IEstimator + 'static, Factory: Fn(usize) -> Es> {
    pub(crate) estimator_factory: Factory,
}

impl<Es: IEstimator + 'static, Factory: Fn(usize) -> Es> AiDeserializer<Es, Factory> {
    fn make_strategy(src: &str) -> Result<Box<dyn IAiStrategy>, SerializerError> {
        let err = || SerializerError::InvalidStrategy(src.to_string());
        Ok(match &src[..4] {
            "rand" => Box::new(ai_strategies::random::AiStrRandom::default()),
            "fold" => Box::new(ai_strategies::fold::AiStrFold::default()),
            "chca" => Box::new(ai_strategies::check_call::AiStrCheckCall::default()),
            "rse " => {
                let key = &src[4..];
                let max_bet = match (usize::from_str(key), f64::from_str(key)) {
                    (Ok(abs), _) => Ok(abs),
                    (_, Ok(rel)) => Err(rel),
                    _ => return Err(err()),
                };
                Box::new(ai_strategies::raise::AiStrRaise { max_bet })
            }
            "mrse" => Box::new(ai_strategies::max_raise::AiStrMaxRaise::default()),
            _ => return Err(err()),
        })
    }

    fn make_strategies(
        src: &[(AiStrategyParams, String)],
    ) -> Result<Vec<ParametrizedStrategy>, SerializerError> {
        src.iter()
            .map(|(p, s)| Ok((p.clone(), Self::make_strategy(s)?)))
            .collect::<Result<Vec<_>, _>>()
    }

    fn parse_config(src: &Object) -> Result<AiConfig, SerializerError> {
        let tries = get_uint(src, "tries")?;
        let bluff_va_banque = nf_to_default(get_real(src, "bluff_va_banque"), 0.0)?;
        let bluff = nf_to_default(get_real(src, "bluff"), 0.0)?;
        let gain = nf_to_default(get_real(src, "gain"), 0.0)?;
        Ok(AiConfig {
            prob_bluff_va_banque_mode: bluff_va_banque,
            prob_common_bluff_mode: bluff,
            prob_risky_gain_mode: gain,
            estimator_tries: tries,
        })
    }

    fn parse_strategy(src: &JsonValue) -> Result<(AiStrategyParams, String), SerializerError> {
        let obj = item_to_obj(src, "one of strategies")?;
        let key = get_str(obj, "str")?.to_string();
        let params = AiStrategyParams {
            max_win_prob: nf_to_none(get_real(obj, "Mwp"))?,
            min_win_prob: nf_to_none(get_real(obj, "mwp"))?,
            min_cards: nf_to_none(get_uint(obj, "mc"))?,
            max_cards: nf_to_none(get_uint(obj, "Mc"))?,
            min_bet: nf_to_none(get_uint_or_real(obj, "mb"))?,
            max_bet: nf_to_none(get_uint_or_real(obj, "Mb"))?,
        };
        Ok((params, key))
    }

    fn parse_strategies(src: &Object) -> Result<StrategiesSchema, SerializerError> {
        let result = StrategiesSchema {
            bluff_va_banque: convert_arr_or_empty(src, "bluff_va_banque", Self::parse_strategy)?,
            common_bluff: convert_arr_or_empty(src, "bluff", Self::parse_strategy)?,
            risky_gain: convert_arr_or_empty(src, "gain", Self::parse_strategy)?,
            safe: convert_arr_or_empty(src, "safe", Self::parse_strategy)?,
        };
        Ok(result)
    }

    fn json_to_schema(src: &str) -> Result<AiSchema, SerializerError> {
        let js = json::parse(src).map_err(|err| SerializerError::InvalidJson(err.to_string()))?;
        let obj = top_level_to_obj(&js)?;
        let config = Self::parse_config(get_obj(obj, "config")?)?;
        let strategies = Self::parse_strategies(get_obj(obj, "strategies")?)?;
        Ok(AiSchema { config, strategies })
    }

    fn config_to_json(config: &AiConfig) -> JsonValue {
        object! {
            "tries": config.estimator_tries,
            "bluff_va_banque": config.prob_bluff_va_banque_mode,
            "bluff": config.prob_common_bluff_mode,
            "gain": config.prob_risky_gain_mode,
        }
    }

    fn strategy_to_json(pair: &(AiStrategyParams, String)) -> JsonValue {
        let (params, strategy) = pair;
        let mut result = object! {"str": strategy.clone()};
        if let Some(prob) = params.max_win_prob {
            result.insert("Mwp", prob).unwrap();
        }
        if let Some(prob) = params.min_win_prob {
            result.insert("mwp", prob).unwrap();
        }
        if let Some(val) = params.max_cards {
            result.insert("Mc", val).unwrap();
        }
        if let Some(val) = params.min_cards {
            result.insert("mc", val).unwrap();
        }
        result
    }

    fn strategies_to_json(strats: &[(AiStrategyParams, String)]) -> JsonValue {
        let vec = strats
            .iter()
            .map(Self::strategy_to_json)
            .collect::<Vec<_>>();
        JsonValue::Array(vec)
    }
}

impl<Es: IEstimator + 'static, Factory: Fn(usize) -> Es> IAiDeserializer
    for AiDeserializer<Es, Factory>
{
    fn schema_to_ai(&self, src: &AiSchema) -> Result<Box<dyn IAi>, SerializerError> {
        let strategies = Strategies {
            bluff_va_banque: Self::make_strategies(&src.strategies.bluff_va_banque)?,
            common_bluff: Self::make_strategies(&src.strategies.common_bluff)?,
            risky_gain: Self::make_strategies(&src.strategies.risky_gain)?,
            safe: Self::make_strategies(&src.strategies.safe)?,
        };
        let ai = Ai::new(
            src.config.clone(),
            (self.estimator_factory)(src.config.estimator_tries),
            strategies,
        );
        Ok(Box::new(ai))
    }

    fn json_to_ai(&self, src: &str) -> Result<Box<dyn IAi>, SerializerError> {
        let schema = Self::json_to_schema(src)?;
        self.schema_to_ai(&schema)
    }

    fn schema_to_json(&self, src: &AiSchema) -> String {
        let strategies = object! {
            "bluff_va_banque": Self::strategies_to_json(&src.strategies.bluff_va_banque),
            "bluff": Self::strategies_to_json(&src.strategies.common_bluff),
            "gain": Self::strategies_to_json(&src.strategies.risky_gain),
            "safe": Self::strategies_to_json(&src.strategies.safe),
        };
        let js = object! {
            "config": Self::config_to_json(&src.config),
            "strategies": strategies,
        };
        js.dump()
    }

    fn str_to_ai_state(&self, src: &str) -> Result<AiState, SerializerError> {
        let js = json::parse(src).map_err(|err| SerializerError::InvalidJson(err.to_string()))?;
        let obj = top_level_to_obj(&js)?;
        let mode = match AiRoundMode::from_usize(get_uint(obj, "mode")?) {
            None => return Err(SerializerError::InvalidField("mode".to_string())),
            Some(val) => val,
        };
        Ok(AiState {
            mode,
            init_required: get_bool(obj, "init_required")?,
        })
    }

    fn ai_state_to_str(&self, state: &AiState) -> String {
        let obj = object! {
            "mode": state.mode.to_usize(),
            "init_required": state.init_required,
        };
        obj.dump()
    }
}
