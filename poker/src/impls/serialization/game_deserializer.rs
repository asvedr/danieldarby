use std::str::FromStr;

use json::object::Object;
use json::{parse, JsonValue};

use crate::entities::errors::SerializerError;
use crate::entities::game::{
    GameState, Player, PlayerInfo, PlayerRound, RoundState, RoundStateKind, SharedGameInfo, Step,
    UserGameInfo,
};
use crate::proto::serializer::IGameDeserializer;
use crate::utils::json::{
    convert_arr, get_bool, get_cards, get_obj, get_str, get_uint, item_to_obj, top_level_to_obj,
};

#[derive(Default)]
pub struct GameDeserializer {}

impl GameDeserializer {
    fn deserialize_player_info(src: &JsonValue) -> Result<PlayerInfo, SerializerError> {
        let js = item_to_obj(src, "one of players")?;
        let name = get_str(js, "name")?.to_string();
        let has_cards = get_bool(js, "has_cards")?;
        let balance = get_uint(js, "balance")?;
        let bet = get_uint(js, "bet")?;
        Ok(PlayerInfo {
            name,
            has_cards,
            balance,
            bet,
        })
    }

    fn deserialize_shared_info(src: &Object) -> Result<SharedGameInfo, SerializerError> {
        let players = convert_arr(src, "players", Self::deserialize_player_info)?;
        let cards = get_cards(src, "cards")?;
        Ok(SharedGameInfo {
            players,
            cards,
            bank: get_uint(src, "bank")?,
            dealer: get_uint(src, "dealer")?,
        })
    }

    fn deserialize_player(js: &JsonValue) -> Result<Player, SerializerError> {
        let obj = item_to_obj(js, "player")?;
        Ok(Player {
            name: get_str(obj, "name")?.to_string(),
            balance: get_uint(obj, "balance")?,
        })
    }

    fn deserialize_round_state_kind(state: &str) -> Result<RoundStateKind, SerializerError> {
        macro_rules! err {
            () => {
                SerializerError::InvalidField("state".to_string())
            };
        }
        let split = state.split('_').collect::<Vec<_>>();
        let result = match split[..] {
            ["bb"] => RoundStateKind::BigBlind,
            ["sb"] => RoundStateKind::SmallBlinds,
            ["oo"] => RoundStateKind::OpenAll,
            ["co"] => RoundStateKind::CheckAll,
            ["af"] => RoundStateKind::AllFolded,
            ["bet", bet, player] => {
                let bet = usize::from_str(bet).map_err(|_| err!())?;
                let player = usize::from_str(player).map_err(|_| err!())?;
                RoundStateKind::NewBet { bet, player }
            }
            ["ch", bet, player] => {
                let bet = usize::from_str(bet).map_err(|_| err!())?;
                let player = usize::from_str(player).map_err(|_| err!())?;
                RoundStateKind::Check { bet, player }
            }
            ["nc", bet] => {
                let bet = usize::from_str(bet).map_err(|_| err!())?;
                RoundStateKind::NewCard { bet }
            }
            _ => return Err(err!()),
        };
        Ok(result)
    }

    fn deserialize_player_round(item: &JsonValue) -> Result<PlayerRound, SerializerError> {
        let obj = item_to_obj(item, "round.player")?;
        Ok(PlayerRound {
            bet: get_uint(obj, "bet")?,
            cards: get_cards(obj, "cards")?,
            active: get_bool(obj, "active")?,
        })
    }

    fn deserialize_round(obj: &Object) -> Result<RoundState, SerializerError> {
        let result = RoundState {
            players: convert_arr(obj, "players", Self::deserialize_player_round)?,
            shared_cards: get_cards(obj, "shared_cards")?,
            state: Self::deserialize_round_state_kind(get_str(obj, "state")?)?,
            unused_cards: get_cards(obj, "unused_cards")?,
            dealer: get_uint(obj, "dealer")?,
            current_player: get_uint(obj, "current_player")?,
        };
        Ok(result)
    }
}

impl IGameDeserializer for GameDeserializer {
    fn str_to_state(&self, src: &str) -> Result<GameState, SerializerError> {
        let js = parse(src).map_err(|err| SerializerError::InvalidJson(err.to_string()))?;
        let obj = top_level_to_obj(&js)?;
        let players = convert_arr(obj, "players", Self::deserialize_player)?;
        let round = get_obj(obj, "round")?;
        let gs = GameState {
            round: Self::deserialize_round(round)?,
            round_number: get_uint(obj, "round_number")?,
            players,
        };
        Ok(gs)
    }

    fn str_to_user_info(&self, src: &str) -> Result<UserGameInfo, SerializerError> {
        let js = parse(src).map_err(|err| SerializerError::InvalidJson(err.to_string()))?;
        let obj = top_level_to_obj(&js)?;
        let shared_info = Self::deserialize_shared_info(get_obj(obj, "shared_info")?)?;
        let id = get_uint(obj, "id")?;
        let cards = get_cards(obj, "cards")?;
        Ok(UserGameInfo {
            shared_info,
            id,
            cards,
        })
    }

    fn str_to_steps(&self, src: &str) -> Result<Vec<Step>, SerializerError> {
        let arr = match parse(src) {
            Ok(JsonValue::Array(val)) => val,
            _ => return Err(SerializerError::InvalidJson(src.to_string())),
        };
        arr.iter()
            .map(|js| match js.as_str() {
                None => Err(SerializerError::InvalidJson(src.to_string())),
                Some(val) => self.str_to_step(val),
            })
            .collect::<Result<Vec<_>, _>>()
    }

    fn str_to_step(&self, src: &str) -> Result<Step, SerializerError> {
        let rest: &str = match src.trim() {
            "fold" => return Ok(Step::Fold),
            "check" => return Ok(Step::Check),
            "call" => return Ok(Step::Call),
            other if other.starts_with("raise ") => &other["raise ".len()..],
            other => return Err(SerializerError::InvalidStep(other.to_string())),
        };
        let value =
            usize::from_str(rest).map_err(|_| SerializerError::InvalidStep(src.to_string()))?;
        Ok(Step::Raise(value))
    }
}
