use json::object::Object;
use json::{object, JsonValue};

use crate::entities::desk::Card;
use crate::entities::game::{
    GameState, Player, PlayerRound, RoundState, RoundStateKind, SharedGameInfo, Step, UserGameInfo,
};
use crate::proto::serializer::IGameSerializer;

#[derive(Default)]
pub(crate) struct GameSerializer {
    pub(crate) use_ascii: bool,
}

impl GameSerializer {
    fn shared_info_to_js(&self, info: &SharedGameInfo) -> JsonValue {
        let players = info
            .players
            .iter()
            .map(|p| {
                object! {
                    "name": p.name.clone(),
                    "has_cards": p.has_cards,
                    "balance": p.balance,
                    "bet": p.bet,
                }
            })
            .collect::<Vec<_>>();
        let cards = Card::serialize_vec(&info.cards, self.use_ascii);
        object! {
            "players": players,
            "cards": cards,
            "bank": info.bank,
            "dealer": info.dealer,
        }
    }

    fn step_to_str_static(step: &Step) -> String {
        match step {
            Step::Fold => "fold".to_string(),
            Step::Check => "check".to_string(),
            Step::Call => "call".to_string(),
            Step::Raise(val) => format!("raise {}", val),
        }
    }

    fn serialize_round_state_kind(state: &RoundStateKind) -> String {
        match state {
            RoundStateKind::BigBlind => "bb".to_string(),
            RoundStateKind::SmallBlinds => "sb".to_string(),
            RoundStateKind::NewBet { bet, player } => format!("bet_{}_{}", bet, player),
            RoundStateKind::Check { bet, player } => format!("ch_{}_{}", bet, player),
            RoundStateKind::NewCard { bet } => format!("nc_{}", bet),
            RoundStateKind::OpenAll => "oo".to_string(),
            RoundStateKind::CheckAll => "co".to_string(),
            RoundStateKind::AllFolded => "af".to_string(),
        }
    }

    fn serialize_round(state: &RoundState) -> JsonValue {
        let players = state
            .players
            .iter()
            .map(Self::serialize_round_player)
            .collect::<Vec<_>>();
        object! {
            "shared_cards": Card::serialize_vec(&state.shared_cards, true),
            "unused_cards": Card::serialize_vec(&state.unused_cards, true),
            "dealer": state.dealer,
            "current_player": state.current_player,
            "state": Self::serialize_round_state_kind(&state.state),
            "players": players,
        }
    }

    fn serialize_round_player(player: &PlayerRound) -> JsonValue {
        object! {
            "bet": player.bet,
            "cards": Card::serialize_vec(&player.cards, true),
            "active": player.active,
        }
    }

    fn serialize_player(player: &Player) -> JsonValue {
        object! {
            "name": player.name.clone(),
            "balance": player.balance,
        }
    }
}

impl IGameSerializer for GameSerializer {
    fn state_to_str(&self, state: &GameState) -> String {
        let players = state
            .players
            .iter()
            .map(Self::serialize_player)
            .collect::<Vec<_>>();
        let obj = object! {
            "round": Self::serialize_round(&state.round),
            "round_number": state.round_number,
            "players": players,
        };
        obj.dump()
    }

    fn shared_info_to_str(&self, info: &SharedGameInfo) -> String {
        self.shared_info_to_js(info).dump()
    }

    fn user_info_to_str(&self, info: UserGameInfo) -> String {
        let cards = Card::serialize_vec(&info.cards, self.use_ascii);
        let obj = object! {
            "shared_info": self.shared_info_to_js(&info.shared_info),
            "id": info.id,
            "cards": cards,
        };
        obj.dump()
    }

    fn steps_to_str(&self, steps: &[Step]) -> String {
        let vec = steps
            .iter()
            .map(Self::step_to_str_static)
            .collect::<Vec<_>>();
        json::stringify(vec)
    }

    fn step_to_str(&self, step: &Step) -> String {
        Self::step_to_str_static(step)
    }

    fn active_user_cards_to_str(&self, state: &GameState) -> String {
        let map = state
            .round
            .players
            .iter()
            .enumerate()
            .filter(|(_, p)| p.active)
            .map(|(id, p)| {
                (
                    id.to_string(),
                    Card::serialize_vec(&p.cards, self.use_ascii),
                )
            });
        Object::from_iter(map).dump()
    }

    fn cards_to_str(&self, cards: &[Card]) -> String {
        Card::serialize_vec(cards, self.use_ascii)
    }
}
