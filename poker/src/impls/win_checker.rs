use crate::entities::desk::Card;
use crate::proto::rate_check::{IRateChecker, IWinChecker};

pub(crate) struct WinChecker<RateChecker> {
    pub rate_checker: RateChecker,
}

impl<RateChecker: IRateChecker> WinChecker<RateChecker> {
    fn get_rate(&self, shared_cards: &[Card], player_cards: &[Card]) -> usize {
        let mut total_cards = player_cards.to_vec();
        total_cards.extend_from_slice(shared_cards);
        total_cards.sort();
        self.rate_checker.get_rate(&total_cards)
    }

    fn find_winner_by_max_card(players: &[&(usize, Vec<Card>)]) -> usize {
        let (id, _) = players
            .iter()
            .map(|(id, p)| (id, p.iter().max().cloned().unwrap()))
            .max_by_key(|(_, card)| *card)
            .unwrap();
        *id
    }
}

impl<RateChecker: IRateChecker> IWinChecker for WinChecker<RateChecker> {
    #[allow(clippy::comparison_chain)]
    fn get_status(&self, shared_cards: &[Card], players: &[(usize, Vec<Card>)]) -> usize {
        let (max_id, cards) = players[0].clone();
        let mut max_rate = self.get_rate(shared_cards, &cards);
        let mut max_id_list = vec![max_id];
        for (id, cards) in players {
            let rate = self.get_rate(shared_cards, cards);
            if rate > max_rate {
                max_id_list = vec![*id];
                max_rate = rate;
            } else if rate == max_rate {
                max_id_list.push(*id);
            }
        }
        if max_id_list.len() == 1 {
            return max_id_list[0];
        }
        let candidates = players
            .iter()
            .filter(|(id, _)| max_id_list.contains(id))
            .collect::<Vec<_>>();
        Self::find_winner_by_max_card(&candidates)
    }
}
