use crate::entities::game::{RoundState, RoundStateKind, SwitchStatus};
use crate::proto::game::IAutomaticStepProcessor;
use crate::utils::cards::take_card;

#[derive(Default)]
pub struct StepProcessor {}

impl IAutomaticStepProcessor for StepProcessor {
    fn check_matched(&self, state: &RoundState) -> bool {
        matches!(state.state, RoundStateKind::NewCard { .. })
    }

    fn get_kind(&self) -> RoundStateKind {
        RoundStateKind::NewCard { bet: 0 }
    }

    fn execute(&self, state: &mut RoundState, _: &[usize]) {
        let bet = state.state.get_bet();
        let card = take_card(&mut state.unused_cards);
        state.shared_cards.push(card);
        state.state = RoundStateKind::Check {
            bet,
            player: state.dealer,
        };
        if state.players[state.dealer].active {
            return;
        }
        let status = state.switch_player_to_next();
        assert_eq!(status, SwitchStatus::Switched);
        let player = state.current_player;
        state.state = RoundStateKind::Check { bet, player };
    }
}
