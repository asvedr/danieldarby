use crate::entities::game::{RoundState, RoundStateKind};
use crate::proto::game::IAutomaticStepProcessor;

pub struct StepProcessor {
    kind: RoundStateKind,
}

impl StepProcessor {
    pub fn new(kind: RoundStateKind) -> Self {
        Self { kind }
    }
}

impl IAutomaticStepProcessor for StepProcessor {
    fn check_matched(&self, state: &RoundState) -> bool {
        state.state == self.kind
    }

    fn get_kind(&self) -> RoundStateKind {
        self.kind.clone()
    }

    fn execute(&self, _: &mut RoundState, _: &[usize]) {}
}
