use crate::entities::game::RoundStateKind;
use crate::proto::game::IAutomaticStepProcessor;

mod big_blind;
mod new_card;
mod open_all;
mod pass;
mod small_blinds;

pub(crate) fn all() -> Vec<Box<dyn IAutomaticStepProcessor>> {
    vec![
        Box::new(big_blind::StepProcessor::default()),
        Box::new(small_blinds::StepProcessor::default()),
        Box::new(new_card::StepProcessor::default()),
        Box::new(open_all::StepProcessor::default()),
        Box::new(pass::StepProcessor::new(RoundStateKind::CheckAll)),
        Box::new(pass::StepProcessor::new(RoundStateKind::AllFolded)),
    ]
}
