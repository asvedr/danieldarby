use crate::entities::constants::SMALL_BLIND;
use crate::entities::game::{RoundState, RoundStateKind};
use crate::proto::game::IAutomaticStepProcessor;
use crate::utils::cards::take_player_cards;

#[derive(Default)]
pub struct StepProcessor {}

impl IAutomaticStepProcessor for StepProcessor {
    fn check_matched(&self, state: &RoundState) -> bool {
        matches!(state.state, RoundStateKind::SmallBlinds)
    }

    fn get_kind(&self) -> RoundStateKind {
        RoundStateKind::SmallBlinds
    }

    fn execute(&self, state: &mut RoundState, balances: &[usize]) {
        for (id, player) in state.players.iter_mut().enumerate() {
            if balances[id] == 0 {
                continue;
            }
            let cards = take_player_cards(&mut state.unused_cards);
            if player.bet == 0 {
                player.bet = SMALL_BLIND;
            }
            player.cards = cards;
        }
        let dealer_bet = state.players[state.dealer].bet;
        let new_state = if dealer_bet == SMALL_BLIND {
            RoundStateKind::Check {
                bet: dealer_bet,
                player: state.dealer,
            }
        } else {
            RoundStateKind::NewBet {
                bet: dealer_bet,
                player: state.dealer,
            }
        };
        state.state = new_state;
    }
}
