use crate::entities::constants::{BIG_BLIND, SMALL_BLIND};
use crate::entities::game::{RoundState, RoundStateKind};
use crate::proto::game::IAutomaticStepProcessor;

#[derive(Default)]
pub struct StepProcessor {}

impl IAutomaticStepProcessor for StepProcessor {
    fn check_matched(&self, state: &RoundState) -> bool {
        matches!(state.state, RoundStateKind::BigBlind)
    }

    fn get_kind(&self) -> RoundStateKind {
        RoundStateKind::BigBlind
    }

    fn execute(&self, state: &mut RoundState, balances: &[usize]) {
        let player = state.dealer;
        if balances[player] == 0 {
            panic!("Big blind player has 0 balance");
        }
        let amount = if balances[player] < BIG_BLIND {
            SMALL_BLIND
        } else {
            BIG_BLIND
        };
        state.players[player].bet = amount;
        state.state = RoundStateKind::SmallBlinds;
        let next = state.closest_active_player(player + 1);
        state.current_player = next;
    }
}
