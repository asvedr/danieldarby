use crate::entities::game::RoundState;
use crate::proto::game::IAutomaticStepProcessor;
use crate::RoundStateKind;

#[derive(Default)]
pub struct StepProcessor {}

impl IAutomaticStepProcessor for StepProcessor {
    fn check_matched(&self, state: &RoundState) -> bool {
        matches!(state.state, RoundStateKind::OpenAll)
    }

    fn get_kind(&self) -> RoundStateKind {
        RoundStateKind::OpenAll
    }

    fn execute(&self, state: &mut RoundState, _: &[usize]) {
        state.state = RoundStateKind::CheckAll
    }
}
