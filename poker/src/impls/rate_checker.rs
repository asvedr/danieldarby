use crate::entities::desk::Card;
use crate::entities::rule::{RuleKind, SetStats};
use crate::proto::rate_check::{IRateChecker, IRule};

pub(crate) struct RateChecker {
    rules: Vec<Box<dyn IRule>>,
}

impl RateChecker {
    pub fn new(mut rules: Vec<Box<dyn IRule>>) -> Self {
        rules.sort_by_key(|rule| rule.kind());
        Self { rules }
    }

    fn collect_stats(cards: &[Card]) -> SetStats {
        let mut stats = SetStats::default();
        let mut suits = Vec::new();
        for card in cards {
            if !suits.contains(&card.suit) {
                suits.push(card.suit)
            }
            stats.value_count[card.value as usize] += 1;
        }
        for val in stats.value_count {
            match val {
                2 => stats.count_of_2 += 1,
                3 => stats.has_3 = true,
                4 => stats.has_4 = true,
                _ => (),
            }
        }
        stats.suits = suits.len();
        stats
    }
}

impl IRateChecker for RateChecker {
    #[inline]
    fn get_rate(&self, cards: &[Card]) -> usize {
        // RULE_RATE = rule_id * 100_000
        //     + rule_high_val_1 * 10_000
        //     + rule_high_val_2 * 1000
        //     + high_card
        let stats = Self::collect_stats(cards);
        for rule_id in (0..self.rules.len()).rev() {
            let rule = &self.rules[rule_id];
            if rule.matches(cards, &stats) {
                let max_card: u16 = (*cards.last().unwrap()).into();
                return (rule_id * 100_000) + rule.high_values(cards) + max_card as usize;
            }
        }
        unreachable!()
    }

    #[inline]
    fn get_rate_rule(&self, cards: &[Card]) -> RuleKind {
        let stats = Self::collect_stats(cards);
        for rule in self.rules.iter().rev() {
            if rule.matches(cards, &stats) {
                return rule.kind();
            }
        }
        unreachable!()
    }

    #[inline]
    fn get_rule(&self, kind: RuleKind) -> &dyn IRule {
        for rule in self.rules.iter() {
            if rule.kind() == kind {
                return &**rule;
            }
        }
        unreachable!()
    }

    #[inline]
    fn get_higher_rules(&self, kind: RuleKind) -> &[Box<dyn IRule>] {
        for i in 0..self.rules.len() {
            if self.rules[i].kind() == kind {
                return &self.rules[i + 1..];
            }
        }
        unreachable!()
    }
}
