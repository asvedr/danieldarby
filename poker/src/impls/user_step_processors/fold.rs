use crate::entities::game::{RoundState, Step};
use crate::proto::game::IUserStepProcessor;

#[derive(Default)]
pub struct StepProcessor {}

impl IUserStepProcessor for StepProcessor {
    fn check_matched(&self, step: &Step) -> bool {
        matches!(step, Step::Fold)
    }

    fn get_available(&self, _: &RoundState, _: usize) -> Vec<Step> {
        vec![Step::Fold]
    }

    fn execute(&self, state: &mut RoundState, player: usize, _: Step) {
        state.players[player].active = false
    }
}
