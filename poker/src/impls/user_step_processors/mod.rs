use crate::proto::game::IUserStepProcessor;

mod call;
mod check;
mod fold;
mod raise;

pub(crate) fn all() -> Vec<Box<dyn IUserStepProcessor>> {
    vec![
        Box::new(fold::StepProcessor::default()),
        Box::new(check::StepProcessor::default()),
        Box::new(call::StepProcessor::default()),
        Box::new(raise::StepProcessor::default()),
    ]
}
