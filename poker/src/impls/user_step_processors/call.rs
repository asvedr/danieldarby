use crate::entities::game::{RoundState, RoundStateKind, Step};
use crate::proto::game::IUserStepProcessor;

#[derive(Default)]
pub struct StepProcessor {}

impl IUserStepProcessor for StepProcessor {
    fn check_matched(&self, step: &Step) -> bool {
        matches!(step, Step::Call)
    }

    fn get_available(&self, state: &RoundState, _: usize) -> Vec<Step> {
        if matches!(state.state, RoundStateKind::NewBet { .. }) {
            vec![Step::Call]
        } else {
            vec![]
        }
    }

    fn execute(&self, state: &mut RoundState, player: usize, _: Step) {
        state.players[player].bet = state.state.get_bet();
    }
}
