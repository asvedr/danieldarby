use crate::entities::constants::RAISES;
use crate::entities::game::{RoundState, RoundStateKind, Step};
use crate::proto::game::IUserStepProcessor;

#[derive(Default)]
pub struct StepProcessor {}

impl IUserStepProcessor for StepProcessor {
    fn check_matched(&self, step: &Step) -> bool {
        matches!(step, Step::Raise(_))
    }

    fn get_available(&self, state: &RoundState, min_balance: usize) -> Vec<Step> {
        let current_bet = match state.state {
            RoundStateKind::NewBet { bet, .. } => bet,
            RoundStateKind::Check { bet, .. } => bet,
            _ => return Vec::new(),
        };
        let mut result = Vec::new();
        for raise in RAISES {
            if current_bet + raise < min_balance {
                result.push(Step::Raise(*raise));
            }
        }
        if min_balance <= current_bet {
            return result;
        }
        let va_banque = Step::Raise(min_balance - current_bet);
        if !result.contains(&va_banque) {
            result.push(va_banque);
        }
        result
    }

    fn execute(&self, state: &mut RoundState, player: usize, step: Step) {
        let bet = match step {
            Step::Raise(val) => val + state.state.get_bet(),
            _ => unreachable!(),
        };
        state.players[player].bet = bet;
        state.state = RoundStateKind::NewBet { bet, player }
    }
}
