use crate::proto::rate_check::IRule;

mod common;
// mod royal_flush;
mod flush;
mod four_of_kind;
mod full_house;
mod high_card;
mod pair;
mod straight;
mod straight_flush;
mod three_of_kind;
mod two_pairs;

type StraightFlush = straight_flush::Rule<straight::Rule, flush::Rule>;
type FullHouse = full_house::Rule<three_of_kind::Rule>;

pub(crate) fn all() -> Vec<Box<dyn IRule>> {
    let mut rules: Vec<Box<dyn IRule>> = vec![
        Box::new(high_card::Rule::default()),
        Box::new(pair::Rule::default()),
        Box::new(two_pairs::Rule::default()),
        Box::new(three_of_kind::Rule::default()),
        Box::new(straight::Rule::default()),
        Box::new(flush::Rule::default()),
        Box::new(FullHouse::default()),
        Box::new(four_of_kind::Rule::default()),
        Box::new(StraightFlush::default()),
        // Box::new(royal_flush::Rule::<StraightFlush>::default()),
    ];
    rules.sort_by_key(|rule| rule.kind());
    rules
}
