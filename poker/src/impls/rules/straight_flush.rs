use crate::entities::desk::Card;
use crate::entities::rule::{RuleKind, SetStats};
use crate::proto::rate_check::IRule;

#[derive(Default)]
pub(crate) struct Rule<Straight, Flush> {
    flush: Flush,
    straight: Straight,
}

impl<Straight: IRule, Flush: IRule> IRule for Rule<Straight, Flush> {
    fn kind(&self) -> RuleKind {
        RuleKind::StraightFlush
    }

    fn matches(&self, cards: &[Card], stats: &SetStats) -> bool {
        self.straight.matches(cards, stats) && self.flush.matches(cards, stats)
    }

    fn high_values(&self, cards: &[Card]) -> usize {
        self.straight.high_values(cards)
    }
}
