use crate::entities::desk::Card;

pub fn same_num(card: Card, rest: &[Card], result: &mut Vec<Card>) {
    for other in rest {
        if card.value == other.value {
            result.push(*other)
        }
    }
}

#[inline]
pub fn matched_cards(cards: &[Card], count: usize) -> Vec<Card> {
    let mut buffer = Vec::new();
    for i in 0..cards.len() - (count - 1) {
        same_num(cards[i], &cards[i..], &mut buffer);
        if buffer.len() == count {
            return buffer;
        }
        buffer.clear();
    }
    unreachable!()
}

// RULE_RATE = rule_id * 100_000
//      + rule_high_val_1 * 10_000
//      + rule_high_val_2 * 1000
//      + high_card
// Use this macro to join rule_high_val_1 and rule_high_val_2
#[macro_export]
macro_rules! values_to_rate {
    ($c1:expr) => {
        $c1 as usize * 10_000
    };
    ($c1:expr, $c2:expr) => {
        ($c1 as usize * 10_000) + ($c2 as usize * 1000)
    };
}
