use crate::entities::desk::Card;
use crate::entities::rule::RuleKind;
use crate::proto::rate_check::IRule;

#[derive(Default)]
pub struct Rule<StraightFlush: IRule> {
    straight_flush: StraightFlush,
}

impl<StraightFlush: IRule> IRule for Rule<StraightFlush> {
    fn kind(&self) -> RuleKind {
        RuleKind::RoyalFlush
    }

    fn matches(&self, cards: &[Card]) -> bool {
        self.straight_flush.matches(cards)
            && cards[0].value == 10
    }

    fn high_card(&self, cards: &[Card]) -> Card {
        *cards.last().unwrap()
    }
}
