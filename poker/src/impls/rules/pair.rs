use crate::entities::desk::Card;
use crate::entities::rule::{RuleKind, SetStats};
use crate::impls::rules::common::matched_cards;
use crate::proto::rate_check::IRule;
use crate::values_to_rate;

#[derive(Default)]
pub(crate) struct Rule {}

impl IRule for Rule {
    fn kind(&self) -> RuleKind {
        RuleKind::Pair
    }

    fn matches(&self, _: &[Card], stats: &SetStats) -> bool {
        stats.count_of_2 == 1
    }

    fn high_values(&self, cards: &[Card]) -> usize {
        values_to_rate!(matched_cards(cards, 2)[0].value)
    }
}
