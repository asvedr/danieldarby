use crate::entities::desk::Card;
use crate::entities::rule::{RuleKind, SetStats};
use crate::proto::rate_check::IRule;
use crate::values_to_rate;

#[derive(Default)]
pub(crate) struct Rule {}

impl IRule for Rule {
    fn kind(&self) -> RuleKind {
        RuleKind::Flush
    }

    fn matches(&self, _: &[Card], stats: &SetStats) -> bool {
        stats.suits == 1
    }

    fn high_values(&self, cards: &[Card]) -> usize {
        values_to_rate!(cards.last().unwrap().value)
    }
}
