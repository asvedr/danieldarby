use crate::entities::desk::Card;
use crate::entities::rule::{RuleKind, SetStats};
use crate::proto::rate_check::IRule;

#[derive(Default)]
pub(crate) struct Rule<Three: IRule> {
    three: Three,
}

impl<Three: IRule> IRule for Rule<Three> {
    fn kind(&self) -> RuleKind {
        RuleKind::FullHouse
    }

    fn matches(&self, _: &[Card], stats: &SetStats) -> bool {
        stats.count_of_2 == 1 && stats.has_3
    }

    fn high_values(&self, cards: &[Card]) -> usize {
        self.three.high_values(cards)
    }
}
