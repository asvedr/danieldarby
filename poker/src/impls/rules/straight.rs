use crate::entities::desk::Card;
use crate::entities::rule::{RuleKind, SetStats};
use crate::proto::rate_check::IRule;
use crate::values_to_rate;

#[derive(Default)]
pub(crate) struct Rule {}

impl Rule {
    fn check_common(cards: &[Card]) -> bool {
        for i in 0..cards.len() - 1 {
            if cards[i + 1].value != cards[i].value + 1 {
                return false;
            }
        }
        true
    }

    fn check_ace_as_one(stats: &SetStats) -> bool {
        if stats.has_3 || stats.has_4 || stats.count_of_2 > 0 {
            return false;
        }
        let sum: usize = stats.value_count[2..=5].iter().sum();
        sum + stats.value_count[Card::ACE as usize] == 5
    }
}

impl IRule for Rule {
    fn kind(&self) -> RuleKind {
        RuleKind::Straight
    }

    fn matches(&self, cards: &[Card], stats: &SetStats) -> bool {
        if stats.value_count[Card::ACE as usize] == 1 && stats.value_count[2] == 1 {
            Self::check_ace_as_one(stats)
        } else {
            Self::check_common(cards)
        }
    }

    fn high_values(&self, cards: &[Card]) -> usize {
        let last = cards[4].value;
        let first = cards[0].value;
        if last == Card::ACE && first == 2 {
            values_to_rate!(cards[3].value)
        } else {
            values_to_rate!(last)
        }
    }
}
