use crate::entities::desk::Card;
use crate::entities::rule::{RuleKind, SetStats};
use crate::impls::rules::common::same_num;
use crate::proto::rate_check::IRule;
use crate::values_to_rate;

#[derive(Default)]
pub(crate) struct Rule {}

impl Rule {
    fn get_matched_values(cards: &[Card]) -> (Card, Card) {
        let mut buffer = Vec::new();
        let mut found = None;
        for i in 0..cards.len() - 1 {
            let card = cards[i];
            match found {
                Some(val) if card.value == val => (),
                Some(_) => {
                    same_num(card, &cards[i..], &mut buffer);
                    if buffer.len() == 2 {
                        return (buffer[0], buffer[1]);
                    }
                    buffer.clear();
                }
                _ => {
                    same_num(card, &cards[i..], &mut buffer);
                    if buffer.len() == 2 {
                        found = Some(card.value)
                    }
                    buffer.clear();
                }
            }
        }
        unreachable!()
    }
}

impl IRule for Rule {
    fn kind(&self) -> RuleKind {
        RuleKind::TwoPairs
    }

    fn matches(&self, _: &[Card], stats: &SetStats) -> bool {
        stats.count_of_2 == 2
    }

    fn high_values(&self, cards: &[Card]) -> usize {
        let (c1, c2) = Self::get_matched_values(cards);
        if c2.value > c1.value {
            values_to_rate!(c2.value, c1.value)
        } else {
            values_to_rate!(c1.value, c2.value)
        }
    }
}
