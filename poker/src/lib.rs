#![allow(clippy::result_unit_err)]
mod entities;
mod impls;
mod proto;
mod utils;

pub use crate::entities::ai::AiState;
pub use crate::entities::ai::{AiConfig, AiSchema, AiStrategyParams, StrategiesSchema};
pub use crate::entities::desk::{Card, Suit};
pub use crate::entities::errors;
pub use crate::entities::game::{
    ExpectedAction, GameState, NextRoundResponse, Player, PlayerInfo, RoundStateKind, Step,
    UserGameInfo,
};
pub use crate::impls::ai::Strategies;
pub use crate::impls::ai_strategies::AiStr;
pub use crate::proto::game::{IAi, IGame};
pub use crate::proto::rate_check::IEstimator;
pub use crate::proto::serializer::{IAiDeserializer, IGameDeserializer, IGameSerializer};

use crate::impls::ai::Ai;
use crate::impls::estimator_probe::Estimator;
use crate::impls::game::Game;
use crate::impls::rate_checker::RateChecker;
use crate::impls::serialization::ai_deserializer::AiDeserializer;
use crate::impls::serialization::game_deserializer::GameDeserializer;
use crate::impls::serialization::game_serializer::GameSerializer;
use crate::impls::win_checker::WinChecker;
use crate::impls::{auto_step_processors, user_step_processors};

fn rate_checker() -> RateChecker {
    use crate::impls::rules;
    RateChecker::new(rules::all())
}

fn win_checker() -> WinChecker<RateChecker> {
    WinChecker {
        rate_checker: rate_checker(),
    }
}

fn estimator_probe_raw(exp_count: usize) -> Estimator<WinChecker<RateChecker>> {
    Estimator::new(
        exp_count,
        WinChecker {
            rate_checker: rate_checker(),
        },
    )
}

pub fn estimator_probe(exp_count: usize) -> Box<dyn IEstimator> {
    Box::new(estimator_probe_raw(exp_count))
}

pub fn game_manager() -> Box<dyn IGame> {
    let game = Game::new(
        win_checker(),
        user_step_processors::all(),
        auto_step_processors::all(),
    );
    Box::new(game)
}

pub fn create_ai(config: AiConfig, strategies: Strategies) -> Box<dyn IAi> {
    let estimator = estimator_probe_raw(config.estimator_tries);
    Box::new(Ai::new(config, estimator, strategies))
}

pub fn create_game_serializer(use_ascii: bool) -> Box<dyn IGameSerializer> {
    Box::new(GameSerializer { use_ascii })
}

pub fn create_game_deserializer() -> Box<dyn IGameDeserializer> {
    Box::new(GameDeserializer::default())
}

pub fn create_ai_deserializer() -> Box<dyn IAiDeserializer> {
    let deserializer = AiDeserializer {
        estimator_factory: estimator_probe_raw,
    };
    Box::new(deserializer)
}
