use crate::entities::desk::Card;
use crate::entities::rule::{RuleKind, SetStats};

pub(crate) trait IRule {
    fn kind(&self) -> RuleKind;
    fn matches(&self, cards: &[Card], stats: &SetStats) -> bool;
    fn high_values(&self, cards: &[Card]) -> usize;
}

pub(crate) trait IRateChecker {
    fn get_rate(&self, cards: &[Card]) -> usize;
    fn get_rate_rule(&self, cards: &[Card]) -> RuleKind;
    fn get_rule(&self, kind: RuleKind) -> &dyn IRule;
    fn get_higher_rules(&self, kind: RuleKind) -> &[Box<dyn IRule>];
}

pub trait IWinChecker {
    fn get_status(&self, shared_cards: &[Card], players: &[(usize, Vec<Card>)]) -> usize;
}

pub trait IEstimator {
    // probability of win
    fn estimate(&mut self, shared_cards: &[Card], player_cards: &[Card], enemies: usize) -> f64;
}
