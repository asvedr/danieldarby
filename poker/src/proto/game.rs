use crate::entities::ai::{AiCheckStrategyParams, AiState};
use crate::entities::errors::GameError;
use crate::entities::game::{
    ChosenStep, ExpectedAction, GameState, NextRoundResponse, RoundState, RoundStateKind,
    SharedGameInfo, Step, UserGameInfo,
};

pub(crate) trait IUserStepProcessor {
    fn check_matched(&self, step: &Step) -> bool;
    fn get_available(&self, state: &RoundState, min_balance: usize) -> Vec<Step>;
    fn execute(&self, state: &mut RoundState, player: usize, step: Step);
}

pub(crate) trait IAutomaticStepProcessor {
    fn check_matched(&self, state: &RoundState) -> bool;
    fn get_kind(&self) -> RoundStateKind;
    fn execute(&self, state: &mut RoundState, balances: &[usize]);
}

pub trait IGame {
    fn new_game(&self, players: Vec<String>, balance: usize) -> Result<GameState, GameError>;
    fn get_round_winner(&self, state: &GameState) -> Option<usize>;
    fn next_round(&self, state: &GameState) -> NextRoundResponse;
    fn get_steps(&self, state: &GameState, player: usize) -> Vec<Step>;
    fn get_expected_action(&self, state: &GameState) -> ExpectedAction;
    fn get_shared_info(&self, state: &GameState) -> SharedGameInfo;
    fn get_user_info(&self, state: &GameState, player: usize) -> UserGameInfo;
    fn apply_auto_step(&self, state: &GameState) -> Result<GameState, GameError>;
    fn apply_user_step(
        &self,
        state: &GameState,
        player: usize,
        step: Step,
    ) -> Result<GameState, GameError>;
}

pub trait IAiStrategy {
    fn matches(&self, _params: &AiCheckStrategyParams) -> bool {
        true
    }
    fn name(&self) -> String;
    fn choose_step(&self, params: &AiCheckStrategyParams, steps: &[Step]) -> Step;
}

pub trait IAi {
    fn choose_step(
        &self,
        game_state: &UserGameInfo,
        ai_state: &mut AiState,
        steps: &[Step],
    ) -> ChosenStep;
    #[cfg(test)]
    fn debug_info(&self) -> String;
}
