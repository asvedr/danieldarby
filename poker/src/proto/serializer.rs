use crate::entities::ai::AiSchema;
use crate::entities::errors::SerializerError;
use crate::entities::game::{GameState, SharedGameInfo, Step, UserGameInfo};
use crate::proto::game::IAi;
use crate::{AiState, Card};

pub trait IGameSerializer {
    // for manager
    fn state_to_str(&self, state: &GameState) -> String;
    // for manager
    fn shared_info_to_str(&self, info: &SharedGameInfo) -> String;
    // for manager
    fn user_info_to_str(&self, info: UserGameInfo) -> String;
    // for manager
    fn steps_to_str(&self, steps: &[Step]) -> String;
    // for manager
    fn step_to_str(&self, step: &Step) -> String;
    // for manager
    fn active_user_cards_to_str(&self, state: &GameState) -> String;
    fn cards_to_str(&self, cards: &[Card]) -> String;
}

pub trait IGameDeserializer {
    // for manager
    fn str_to_state(&self, src: &str) -> Result<GameState, SerializerError>;
    // for ai and client
    fn str_to_user_info(&self, src: &str) -> Result<UserGameInfo, SerializerError>;
    // for ai and client
    fn str_to_steps(&self, src: &str) -> Result<Vec<Step>, SerializerError>;
    // for manager
    fn str_to_step(&self, src: &str) -> Result<Step, SerializerError>;
}

pub trait IAiDeserializer {
    fn schema_to_ai(&self, src: &AiSchema) -> Result<Box<dyn IAi>, SerializerError>;
    fn json_to_ai(&self, src: &str) -> Result<Box<dyn IAi>, SerializerError>;
    fn schema_to_json(&self, src: &AiSchema) -> String;
    fn str_to_ai_state(&self, src: &str) -> Result<AiState, SerializerError>;
    fn ai_state_to_str(&self, state: &AiState) -> String;
}
