use crate::entities::constants::PLAYER_CARDS;
use crate::Card;

#[inline]
pub fn take_card(unused: &mut Vec<Card>) -> Card {
    let index = fastrand::usize(..unused.len());
    unused.remove(index)
}

#[inline]
pub fn take_player_cards(unused: &mut Vec<Card>) -> Vec<Card> {
    let mut result = Vec::with_capacity(PLAYER_CARDS);
    for _ in 0..PLAYER_CARDS {
        result.push(take_card(unused));
    }
    result
}
