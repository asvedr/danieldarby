use crate::Card;
use json::object::Object;
use json::JsonValue;

use crate::entities::errors::SerializerError;

pub fn top_level_to_obj(src: &JsonValue) -> Result<&Object, SerializerError> {
    match src {
        JsonValue::Object(val) => Ok(val),
        _ => Err(SerializerError::TopLevelNotAnObject),
    }
}

pub fn item_to_obj<'a>(src: &'a JsonValue, item: &str) -> Result<&'a Object, SerializerError> {
    match src {
        JsonValue::Object(val) => Ok(val),
        _ => Err(SerializerError::InvalidJson(format!(
            "item {} not an object",
            item
        ))),
    }
}

pub fn get_obj<'a>(obj: &'a Object, field: &str) -> Result<&'a Object, SerializerError> {
    match get_field(obj, field)? {
        JsonValue::Object(val) => Ok(val),
        _ => invalid_field(format!("field {} not an object", field)),
    }
}

pub fn get_arr<'a>(obj: &'a Object, field: &str) -> Result<&'a [JsonValue], SerializerError> {
    match get_field(obj, field)? {
        JsonValue::Array(val) => Ok(val),
        _ => invalid_field(format!("field {} not an array", field)),
    }
}

pub fn convert_arr<T: 'static, F: Fn(&JsonValue) -> Result<T, SerializerError>>(
    obj: &Object,
    field: &str,
    func: F,
) -> Result<Vec<T>, SerializerError> {
    get_arr(obj, field)?
        .iter()
        .map(func)
        .collect::<Result<Vec<_>, _>>()
}

pub fn convert_arr_or_empty<T: 'static, F: Fn(&JsonValue) -> Result<T, SerializerError>>(
    obj: &Object,
    field: &str,
    func: F,
) -> Result<Vec<T>, SerializerError> {
    let arr = match get_arr(obj, field) {
        Ok(val) => val,
        Err(SerializerError::FieldNotFound(_)) => return Ok(Vec::new()),
        Err(err) => return Err(err),
    };
    arr.iter().map(func).collect::<Result<Vec<_>, _>>()
}

pub fn get_uint(obj: &Object, field: &str) -> Result<usize, SerializerError> {
    match get_field(obj, field)?.as_usize() {
        Some(val) => Ok(val),
        _ => invalid_field(format!("field {} not an uint", field)),
    }
}

pub fn get_real(obj: &Object, field: &str) -> Result<f64, SerializerError> {
    match get_field(obj, field)?.as_f64() {
        Some(val) => Ok(val),
        _ => invalid_field(format!("field {} not a real", field)),
    }
}

pub fn get_uint_or_real(obj: &Object, field: &str) -> Result<Result<usize, f64>, SerializerError> {
    let value = get_field(obj, field)?;
    match (value.as_usize(), value.as_f64()) {
        (Some(val), _) => Ok(Ok(val)),
        (_, Some(val)) => Ok(Err(val)),
        _ => invalid_field(format!("field {} not a number", field)),
    }
}

pub fn get_str<'a>(obj: &'a Object, field: &str) -> Result<&'a str, SerializerError> {
    match get_field(obj, field)?.as_str() {
        Some(val) => Ok(val),
        _ => invalid_field(format!("field {} not a str", field)),
    }
}

pub fn get_cards(obj: &Object, field: &str) -> Result<Vec<Card>, SerializerError> {
    match Card::deserialize_vec(get_str(obj, field)?) {
        Ok(val) => Ok(val),
        _ => invalid_field("invalid cards".to_string()),
    }
}

pub fn get_bool(obj: &Object, field: &str) -> Result<bool, SerializerError> {
    match get_field(obj, field)?.as_bool() {
        Some(val) => Ok(val),
        _ => invalid_field(format!("field {} not a str", field)),
    }
}

pub fn nf_to_none<T>(res: Result<T, SerializerError>) -> Result<Option<T>, SerializerError> {
    match res {
        Ok(val) => Ok(Some(val)),
        Err(SerializerError::FieldNotFound(_)) => Ok(None),
        Err(other) => Err(other),
    }
}

pub fn nf_to_default<T>(res: Result<T, SerializerError>, def: T) -> Result<T, SerializerError> {
    Ok(match nf_to_none(res)? {
        Some(val) => val,
        None => def,
    })
}

fn invalid_field<T>(msg: String) -> Result<T, SerializerError> {
    Err(SerializerError::InvalidField(msg))
}

fn get_field<'a>(obj: &'a Object, field: &str) -> Result<&'a JsonValue, SerializerError> {
    match obj.get(field) {
        None => Err(SerializerError::FieldNotFound(field.to_string())),
        Some(val) => Ok(val),
    }
}
