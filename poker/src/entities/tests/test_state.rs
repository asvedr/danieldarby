use crate::entities::game::{PlayerRound, RoundState};
use crate::Card;

#[test]
#[should_panic]
fn test_get_closest_player_empty() {
    RoundState::default().closest_active_player(0);
}

#[test]
fn test_get_closest_player_match() {
    let mut state = RoundState::default();
    state.players = vec![
        PlayerRound {
            bet: 0,
            cards: Card::deserialize_vec("s3,d3").unwrap(),
            active: true,
        },
        PlayerRound {
            bet: 0,
            cards: Card::deserialize_vec("s4,d4").unwrap(),
            active: true,
        },
    ];
    assert_eq!(state.closest_active_player(0), 0);
    assert_eq!(
        state.get_active_players(),
        &[
            (0, Card::deserialize_vec("s3,d3").unwrap()),
            (1, Card::deserialize_vec("s4,d4").unwrap())
        ]
    )
}

#[test]
fn test_get_closest_player_miss_match_1() {
    let mut state = RoundState::default();
    state.players = vec![
        PlayerRound {
            bet: 0,
            cards: Card::deserialize_vec("s3,d3").unwrap(),
            active: false,
        },
        PlayerRound {
            bet: 0,
            cards: Card::deserialize_vec("s4,d4").unwrap(),
            active: true,
        },
    ];
    assert_eq!(state.closest_active_player(0), 1);
    assert_eq!(
        state.get_active_players(),
        &[(1, Card::deserialize_vec("s4,d4").unwrap())]
    )
}

#[test]
fn test_get_closest_player_miss_match_2() {
    let mut state = RoundState::default();
    state.players = vec![
        PlayerRound {
            bet: 0,
            cards: Card::deserialize_vec("s3,d3").unwrap(),
            active: true,
        },
        PlayerRound {
            bet: 0,
            cards: Card::deserialize_vec("s4,d4").unwrap(),
            active: false,
        },
    ];
    assert_eq!(state.closest_active_player(1), 0);
    assert_eq!(
        state.get_active_players(),
        &[(0, Card::deserialize_vec("s3,d3").unwrap())],
    )
}
