use crate::Card;
use std::str::FromStr;

#[test]
fn test_card_set() {
    let set = Card::all();
    assert_eq!(set.len(), 52);
    for a in 0..set.len() {
        for b in a + 1..set.len() {
            assert_ne!(a, b, "Card::all() produced not uniq values: {:?}", a);
        }
    }
}

fn test_card_ser_de_body(card: Card, use_ascii: bool) {
    let as_text = card.serialize(use_ascii);
    let de = Card::from_str(&as_text).expect(&format!(
        "{:?}.serialize({}) from_str failed",
        card, use_ascii
    ));
    assert_eq!(card, de, "Card::from_str() for {:?}", card);
    let key: u16 = card.into();
    let restored = Card::from(key);
    assert_eq!(card, restored, "Card::from::<u16>() for {:?}", card);
}

#[test]
fn test_ser_de() {
    let set = Card::all();
    for card in set {
        for use_ascii in [true, false] {
            test_card_ser_de_body(card, use_ascii);
        }
    }
}
