use std::str::FromStr;

#[derive(Debug, Ord, PartialOrd, Eq, PartialEq, Clone, Copy)]
pub enum Suit {
    Spades,
    Hearts,
    Diamonds,
    Clubs,
}

#[derive(Debug, Ord, PartialOrd, Eq, PartialEq, Clone, Copy)]
pub struct Card {
    pub value: u8,
    pub suit: Suit,
}

impl Card {
    // Values
    pub const JACK: u8 = 11;
    pub const QUEEN: u8 = 12;
    pub const KING: u8 = 13;
    pub const ACE: u8 = 14;

    pub const VALUE_STR_MAP: &'static [(u8, &'static str)] = &[
        (2, "2"),
        (3, "3"),
        (4, "4"),
        (5, "5"),
        (6, "6"),
        (7, "7"),
        (8, "8"),
        (9, "9"),
        (10, "10"),
        (Self::JACK, "J"),
        (Self::QUEEN, "Q"),
        (Self::KING, "K"),
        (Self::ACE, "A"),
    ];
    pub const MIN_VAL: u8 = 2;
    pub const MAX_VAL: u8 = Self::ACE;
    pub const LIMIT: usize = Self::MAX_VAL as usize + 1;
}

impl Suit {
    pub const ALL: &'static [Self] = &[Self::Clubs, Self::Diamonds, Self::Hearts, Self::Spades];
    const CHAR_MAP: &'static [(Self, char, char)] = &[
        (Self::Spades, 'S', '♠'),
        (Self::Clubs, 'C', '♣'),
        (Self::Diamonds, 'D', '♦'),
        (Self::Hearts, 'H', '♥'),
    ];

    pub fn serialize(&self, use_ascii: bool) -> char {
        for (item, ascii, utf) in Self::CHAR_MAP {
            if item != self {
                continue;
            }
            return if use_ascii { *ascii } else { *utf };
        }
        unreachable!()
    }

    pub fn deserialize(sym: char) -> Result<Self, ()> {
        for (item, ascii, utf) in Self::CHAR_MAP {
            if *ascii == sym || *utf == sym {
                return Ok(*item);
            }
        }
        Err(())
    }
}

impl FromStr for Suit {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s.to_uppercase().chars().next() {
            None => Err(()),
            Some(val) => Self::deserialize(val),
        }
    }
}

impl Card {
    pub fn all() -> Vec<Card> {
        let mut result = Vec::new();
        for suit in Suit::ALL {
            for value in Self::MIN_VAL..=Self::MAX_VAL {
                result.push(Card { suit: *suit, value })
            }
        }
        result
    }

    pub fn serialize(&self, use_ascii: bool) -> String {
        let mut result = String::new();
        result.push(self.suit.serialize(use_ascii));
        result.push_str(Self::serialize_value(self.value));
        result
    }

    pub fn serialize_vec(vec: &[Self], use_ascii: bool) -> String {
        vec.iter()
            .map(|card| card.serialize(use_ascii))
            .collect::<Vec<_>>()
            .join(",")
    }

    pub fn deserialize_vec(src: &str) -> Result<Vec<Self>, ()> {
        if src.is_empty() {
            return Ok(Vec::new());
        }
        src.split(',')
            .map(Self::from_str)
            .collect::<Result<Vec<_>, _>>()
    }

    fn serialize_value(value: u8) -> &'static str {
        for (val, sym) in Self::VALUE_STR_MAP {
            if *val == value {
                return sym;
            }
        }
        unreachable!()
    }

    fn deserialize_value(key: &str) -> Result<u8, ()> {
        for (val, sym) in Self::VALUE_STR_MAP {
            if key == *sym {
                return Ok(*val);
            }
        }
        Err(())
    }
}

impl From<Suit> for u16 {
    fn from(src: Suit) -> Self {
        for (i, var) in Suit::ALL.iter().enumerate() {
            if *var == src {
                return i as u16;
            }
        }
        unreachable!()
    }
}

impl From<u16> for Suit {
    fn from(num: u16) -> Self {
        let us_num = num as usize;
        assert!(us_num < Self::ALL.len());
        Self::ALL[us_num]
    }
}

impl From<Card> for u16 {
    fn from(src: Card) -> Self {
        let suit: u16 = src.suit.into();
        (src.value as u16 * 10) + suit
    }
}

impl From<u16> for Card {
    fn from(src: u16) -> Self {
        Self {
            suit: Suit::from(src % 10),
            value: (src / 10) as u8,
        }
    }
}

impl FromStr for Card {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let upper = s.to_uppercase();
        let symbols = upper.chars().collect::<Vec<_>>();
        if !(2..=3).contains(&symbols.len()) {
            return Err(());
        }
        let suit = symbols[0];
        let val = symbols[1..].iter().collect::<String>();
        Ok(Self {
            suit: Suit::deserialize(suit)?,
            value: Self::deserialize_value(&val)?,
        })
    }
}
