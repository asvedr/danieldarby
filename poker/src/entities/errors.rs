#[derive(Debug, Eq, PartialEq)]
pub enum SerializerError {
    InvalidStep(String),
    InvalidJson(String),
    InvalidField(String),
    FieldNotFound(String),
    TopLevelNotAnObject,
    InvalidStrategy(String),
}

#[derive(Debug, Eq, PartialEq)]
pub enum GameError {
    TooSmallPlayers,
    InvalidStep,
    NotYourStep,
    AutomaticStepNotExpected,
}
