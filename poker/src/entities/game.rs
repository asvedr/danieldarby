use crate::Card;

#[derive(Debug, Eq, PartialEq, Clone)]
pub enum RoundStateKind {
    BigBlind,
    SmallBlinds,
    NewBet { bet: usize, player: usize },
    Check { bet: usize, player: usize },
    NewCard { bet: usize },
    OpenAll,
    CheckAll,
    AllFolded,
}

impl RoundStateKind {
    pub fn is_automatic_step(&self) -> bool {
        matches!(
            self,
            Self::BigBlind
                | Self::SmallBlinds
                | Self::NewCard { .. }
                | Self::OpenAll
                | Self::CheckAll
                | Self::AllFolded
        )
    }

    pub fn is_user_step(&self) -> bool {
        matches!(
            self,
            RoundStateKind::NewBet { .. } | RoundStateKind::Check { .. }
        )
    }

    pub fn get_player_id(&self) -> usize {
        match self {
            RoundStateKind::NewBet { player, .. } => *player,
            RoundStateKind::Check { player, .. } => *player,
            _ => panic!("call get_player_id for invalid state: {:?}", self),
        }
    }

    pub fn get_bet(&self) -> usize {
        match self {
            RoundStateKind::NewBet { bet, .. } => *bet,
            RoundStateKind::Check { bet, .. } => *bet,
            RoundStateKind::NewCard { bet, .. } => *bet,
            _ => panic!("call get_bet for invalid state: {:?}", self),
        }
    }

    pub fn get_bet_opt(&self) -> Option<usize> {
        match self {
            RoundStateKind::NewBet { bet, .. } => Some(*bet),
            RoundStateKind::Check { bet, .. } => Some(*bet),
            RoundStateKind::NewCard { bet, .. } => Some(*bet),
            _ => None,
        }
    }
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct Player {
    pub name: String,
    pub balance: usize,
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct PlayerRound {
    pub bet: usize,
    pub cards: Vec<Card>,
    pub active: bool,
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct RoundState {
    pub players: Vec<PlayerRound>,
    pub shared_cards: Vec<Card>,
    pub state: RoundStateKind,
    // pub bank: usize,
    pub unused_cards: Vec<Card>,
    pub dealer: usize,
    pub current_player: usize,
}

#[derive(Debug, Eq, PartialEq)]
pub enum SwitchStatus {
    Switched,
    OnlyOneLeft,
    LoopCompleted,
}

impl RoundState {
    pub fn closest_active_player(&self, id: usize) -> usize {
        for i in 0..self.players.len() {
            let index = (i + id) % self.players.len();
            if self.players[index].active {
                return index;
            }
        }
        unreachable!()
    }
}

impl Default for RoundState {
    fn default() -> Self {
        RoundState {
            players: vec![],
            shared_cards: vec![],
            state: RoundStateKind::BigBlind,
            unused_cards: vec![],
            dealer: 0,
            current_player: 0,
        }
    }
}

impl RoundState {
    pub fn get_active_players(&self) -> Vec<(usize, Vec<Card>)> {
        self.players
            .iter()
            .enumerate()
            .filter(|(_, p)| p.active)
            .map(|(i, p)| (i, p.cards.clone()))
            .collect()
    }

    pub fn bank(&self) -> usize {
        self.players.iter().map(|p| p.bet).sum()
    }

    pub fn switch_player_to_next(&mut self) -> SwitchStatus {
        let current = self.current_player;
        let beginner = self.state.get_player_id();
        let active_players = self.players.iter().filter(|p| p.active).count();
        if active_players == 1 {
            return SwitchStatus::OnlyOneLeft;
        }
        for i in 1..self.players.len() {
            let index = (current + i) % self.players.len();
            if beginner == index {
                return SwitchStatus::LoopCompleted;
            }
            if !self.players[index].active {
                continue;
            }
            self.current_player = index;
            return SwitchStatus::Switched;
        }
        unreachable!()
    }
}

#[derive(Default, Debug, Clone, Eq, PartialEq)]
pub struct GameState {
    pub round: RoundState,
    pub round_number: usize,
    pub players: Vec<Player>,
}

#[derive(Debug, Eq, PartialEq, Clone)]
pub enum Step {
    Fold,
    Check,
    Call,
    Raise(usize),
}

impl Step {
    pub fn as_isize(&self) -> isize {
        match self {
            Step::Fold => -1,
            Step::Check => 0,
            Step::Call => 1,
            Step::Raise(val) => *val as isize,
        }
    }
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct PlayerInfo {
    pub name: String,
    pub has_cards: bool,
    pub balance: usize,
    pub bet: usize,
}

#[derive(Default, Debug, Clone, Eq, PartialEq)]
pub struct SharedGameInfo {
    pub players: Vec<PlayerInfo>,
    pub cards: Vec<Card>,
    pub bank: usize,
    pub dealer: usize,
}

impl SharedGameInfo {
    pub fn get_max_bet(&self) -> usize {
        self.players.iter().map(|p| p.bet).max().unwrap_or(0)
    }
}

#[derive(Default, Debug, Clone, Eq, PartialEq)]
pub struct UserGameInfo {
    pub shared_info: SharedGameInfo,
    pub id: usize,
    pub cards: Vec<Card>,
}

pub struct ChosenStep {
    pub step: Step,
    pub info: String,
}

#[derive(Debug, Eq, PartialEq)]
pub enum NextRoundResponse {
    Round(GameState),
    Winner(usize),
    NotReady,
}

#[derive(Debug, Eq, PartialEq)]
pub enum ExpectedAction {
    Nothing,
    AutoStep(RoundStateKind),
    UserStep(usize),
}
