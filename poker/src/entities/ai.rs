#[derive(Debug, Clone)]
pub struct AiConfig {
    pub prob_bluff_va_banque_mode: f64,
    pub prob_common_bluff_mode: f64,
    pub prob_risky_gain_mode: f64,
    pub estimator_tries: usize,
}

impl Default for AiConfig {
    fn default() -> Self {
        AiConfig {
            estimator_tries: 1000,
            prob_bluff_va_banque_mode: -1.0,
            prob_common_bluff_mode: -1.0,
            prob_risky_gain_mode: -1.0,
        }
    }
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd)]
pub enum AiRoundMode {
    BluffVaBanque,
    CommonBluff,
    Std,
    RiskyGain,
}

impl AiRoundMode {
    pub const ALL: &'static [Self] = &[
        Self::BluffVaBanque,
        Self::CommonBluff,
        Self::Std,
        Self::RiskyGain,
    ];

    pub fn to_usize(self) -> usize {
        for i in 0..Self::ALL.len() {
            if self == Self::ALL[i] {
                return i;
            }
        }
        unreachable!()
    }

    pub fn from_usize(val: usize) -> Option<Self> {
        if val < Self::ALL.len() {
            Some(Self::ALL[val])
        } else {
            None
        }
    }
}

#[derive(Debug)]
pub struct AiCheckStrategyParams {
    pub win_estimation: f64,
    pub current_bet: usize,
    pub expected_bet: usize,
    pub balance: usize,
    pub mode: AiRoundMode,
    pub check_loop: bool,
    pub cards_on_desk: usize,
}

#[derive(Debug, Eq, PartialEq)]
pub struct AiState {
    pub mode: AiRoundMode,
    pub init_required: bool,
}

impl Default for AiState {
    fn default() -> Self {
        AiState {
            mode: AiRoundMode::Std,
            init_required: true,
        }
    }
}

impl std::str::FromStr for AiState {
    type Err = ();
    fn from_str(src: &str) -> Result<Self, Self::Err> {
        if src.is_empty() {
            return Ok(Self::default());
        }
        let code = usize::from_str(src).map_err(|_| ())?;
        let mode = match AiRoundMode::from_usize(code) {
            Some(val) => val,
            _ => return Err(()),
        };
        Ok(Self {
            mode,
            init_required: false,
        })
    }
}

impl ToString for AiState {
    fn to_string(&self) -> String {
        if self.init_required {
            return "".to_string();
        }
        self.mode.to_usize().to_string()
    }
}

#[derive(Debug, Default, Clone)]
pub struct AiStrategyParams {
    pub max_win_prob: Option<f64>,
    pub min_win_prob: Option<f64>,
    pub min_cards: Option<usize>,
    pub max_cards: Option<usize>,
    pub min_bet: Option<Result<usize, f64>>,
    pub max_bet: Option<Result<usize, f64>>,
}

#[derive(Debug)]
pub struct StrategiesSchema {
    pub bluff_va_banque: Vec<(AiStrategyParams, String)>,
    pub common_bluff: Vec<(AiStrategyParams, String)>,
    pub risky_gain: Vec<(AiStrategyParams, String)>,
    pub safe: Vec<(AiStrategyParams, String)>,
}

#[derive(Debug)]
pub struct AiSchema {
    pub config: AiConfig,
    pub strategies: StrategiesSchema,
}
