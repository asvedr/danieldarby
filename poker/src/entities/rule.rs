use crate::Card;

#[derive(Debug, Ord, PartialOrd, Eq, PartialEq, Copy, Clone)]
pub enum RuleKind {
    HighCard,
    Pair,
    TwoPairs,
    ThreeOfKind,
    Straight,
    Flush,
    FullHouse,
    FourOfKind,
    StraightFlush,
}

#[derive(Default)]
pub(crate) struct SetStats {
    pub value_count: [usize; Card::LIMIT],
    pub has_4: bool,
    pub has_3: bool,
    pub count_of_2: usize,
    pub suits: usize,
}
