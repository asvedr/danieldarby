pub const BIG_BLIND: usize = 10;
pub const SMALL_BLIND: usize = 5;
pub const PLAYER_CARDS: usize = 2;
pub const MAX_SHARED_CARDS: usize = 3;
pub const RAISES: &[usize] = &[
    SMALL_BLIND,
    SMALL_BLIND * 2,
    SMALL_BLIND * 3,
    SMALL_BLIND * 4,
];
