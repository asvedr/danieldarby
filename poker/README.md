# Poker crate
### Short description
It is an engine for poker game. Manage game state, compile available information for player. Dump/load state. Ai component for automatic opponent.
## components:

- ### Game manager
  Component to init/stop game, change round state and check who is win.
  ```rust
  // Create manager
  manager = poker::game_manager();
  let players = vec!["a".to_string(), "b".to_string()];
  // Start new game with two players and init balance 100. Game object is immutable
  let game = manager.new_game(players, 100);
  // Check if round completed
  manager.get_round_winner(&game);
  // Turn to next round. Next round is in return
  manager.next_round(&game);
  // Get available steps for player with index 0
  manager.get_steps(&game, 0);
  // What is expected now? Player step, auto step or next round
  manager.get_expected_action(&game);
  // Get board state to display
  manager.get_shared_info(&game);
  // Get board state and cards for user with index 0
  manager.get_user_info(&game, 0);
  // Apply step and return new state
  game = manager.apply_auto_step(&game)?;
  // Apply step of user with index 0 and return new state
  game = manager.apply_user_step(&game, 0, step)?;
  ```
- ### serializers
  Use those components to dump/load game state to/from database and serialize/parse user steps
  ```rust
  let ser = poker::create_game_serializer();
  let de = poker::create_game_deserializer();
  ```
- ### AI
  `poker::create_ai_deserializer` creates json AI loader

  `IAi` component is chooser for step from available

  `AiState` is ai mode in current round. Is AI honest now or trying to bluff.

  ```rust
  let de = poker::create_ai_deserializer();
  let ai = de.json_to_ai(text_with_json_ai_config);
  let mut ai_state = poker::AiState::default();
  ...
  let user_info = game_manager.get_user_info(...);
  let steps = game_manager.get_steps(...);
  ... 
  let (step, desicion) = ai.choose_step(user_info, &mut ai_state, steps);
  ...
  let text = ai_state.to_string();
  AiState::from_str(&text);
  ```

# Ai config schema:
```javascript
{
  "config": {
    "tries": 100, // int - number of probes which AI uses to calculate win probability
    "bluff_va_banque": 0.1, // Optional[float[0..1]], default=0 - frequency of bluff_va_banque strategy
    "bluff": 0.2, // Optional[float[0..1]], default=0 frequency of bluff strategy
    "gain": 0.1 // Optional[float[0..1]], default=0 frequency of gain strategy
  },
  "strategies": {
    "bluff_va_banque": [...], // Optional[list[Strategy]], default=[]
    "bluff": [...], // Optional[list[Strategy]], default=[]
    "gain": [...], // Optional[list[Strategy]], default=[]
    "safe": [ // Optional[list[Strategy]], default=[] - default strategy.
      {
        "str": "chca", // ActKey* - action key
        "mwp": 0.1, // Optional[float[0..1]] - min win probability
        "Mwp": 0.9, // Optional[float[0..1]] - max win probability
        "mb": 5, // Optional[Bet*] - min bet
        "Mb": 0.5 // Optional[Bet*] - max bet
        "mc": 0, // Optional[int] - min cards
        "Mc": 2, // Optional[int] - max cards
      },
      {"str": "mrse", "mwp": 0.9, "mc": 1},
      {"str": "rse 20", "mwp": 0.7, "mc": 2}
      {"str": "fold"},
      ...
    ]
  }
}
// *ActKey:
// - chca - check or call
// - fold - fold
// - mrse - raise to max available value
// - rse N - raise till N bet then call
//         N is int or float value. Check Bet* description
// - rand - choose random step
// *Bet:
//   The bet can be int or float
//   int bet is an absolut value
//   float bet is a relative to current balance value
}
```
