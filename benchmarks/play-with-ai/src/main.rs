mod controller;

use std::env;
use std::str::FromStr;

use crate::controller::Controller;
use poker::{AiState, ExpectedAction, GameState, IGame, RoundStateKind};

fn play_round(ctrl: &mut Controller) -> usize {
    use poker::ExpectedAction::*;

    while ctrl.get_winner().is_none() {
        ctrl.show_full_state();
        let action = ctrl.get_expected_action();
        match action {
            UserStep(0) => ctrl.make_user_step(),
            UserStep(n) => ctrl.make_ai_step(n),
            AutoStep(RoundStateKind::NewCard { .. }) => ctrl.new_card(),
            AutoStep(RoundStateKind::CheckAll) | AutoStep(RoundStateKind::OpenAll) => {
                ctrl.do_auto_steps()
            }
            _ => panic!("unexpected action: {:?}", action),
        }
    }
    ctrl.get_winner().unwrap()
}

fn play_game(ctrl: &mut Controller) -> String {
    loop {
        println!("\n\nnew round");
        ctrl.pre_round();
        let round_winner = play_round(ctrl);
        println!("round winner: {}", ctrl.state.players[round_winner].name,);
        ctrl.post_round();
        let winner = ctrl.new_round();
        if let Some(id) = winner {
            return ctrl.state.players[id].name.clone();
        }
    }
}

fn main() {
    let args = env::args().collect::<Vec<_>>();
    if args.len() != 3 {
        println!("expected args: <enemy_count: int> <balance: int>");
        std::process::exit(1);
    }
    let count = usize::from_str(&args[1]).unwrap();
    let balance = usize::from_str(&args[2]).unwrap();
    let mut ctrl = Controller::new(count, balance);
    let winner = play_game(&mut ctrl);
    println!("winner: {}", winner);
}
