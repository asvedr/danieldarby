use std::collections::HashMap;
use std::io::{self, BufRead};

use poker::{
    create_ai, create_game_serializer, AiConfig, AiState, AiStr, AiStrategyParams, Card, ExpectedAction,
    GameState, IAi, IGame, NextRoundResponse, RoundStateKind, Step, Strategies, UserGameInfo,
};

const USER_ID: usize = 0;

struct Ai {
    id: usize,
    ctrl: Box<dyn IAi>,
    state: AiState,
}

impl Ai {
    fn choose_step(&mut self, info: UserGameInfo, steps: &[Step]) -> Step {
        let chosen = self.ctrl.choose_step(&info, &mut self.state, steps);
        println!("Ai[{}]: {}", self.id, chosen.info);
        chosen.step
    }
}

pub struct Controller {
    pub game: Box<dyn IGame>,
    pub state: GameState,
    ai: Vec<Ai>,
}

impl Controller {
    pub fn new(ai_count: usize, balance: usize) -> Self {
        let game = poker::game_manager();
        let mut players = vec!["user".to_string()];
        players.extend((0..ai_count).map(|i| format!("ai_{}", i)));
        let state = game.new_game(players, balance).unwrap();

        let ai = (0..ai_count + 1)
            .map(|id| {
                let strs = Strategies {
                    safe: vec![AiStr::random(AiStrategyParams::default())],
                    ..Default::default()
                };
                let ctrl = create_ai(AiConfig::default(), strs);
                Ai {
                    id,
                    ctrl,
                    state: AiState::default(),
                }
            })
            .collect::<Vec<_>>();
        Self { game, state, ai }
    }

    pub fn make_ai_step(&mut self, id: usize) {
        let info = self.game.get_user_info(&self.state, id);
        let steps = self.game.get_steps(&self.state, id);
        let step = self.ai[id].choose_step(info, &steps);
        self.apply_step(id, step)
    }

    pub fn make_user_step(&mut self) {
        let steps = self.game.get_steps(&self.state, USER_ID);
        assert_ne!(steps, &[] as &[Step]);
        let step = Self::get_user_step(steps);
        self.apply_step(USER_ID, step)
    }

    pub fn get_expected_action(&self) -> ExpectedAction {
        self.game.get_expected_action(&self.state)
    }

    pub fn new_card(&mut self) {
        let expected = self.game.get_expected_action(&self.state);
        assert!(matches!(
            expected,
            ExpectedAction::AutoStep(RoundStateKind::NewCard { .. })
        ));
        let state = self.game.apply_auto_step(&self.state).unwrap();
        self.state = state;
    }

    pub fn pre_round(&mut self) {
        let expected = self.get_expected_action();
        assert!(matches!(
            expected,
            ExpectedAction::AutoStep(RoundStateKind::BigBlind)
        ));
        self.do_auto_steps();
    }

    pub fn post_round(&mut self) {
        assert!(self.get_winner().is_some());
        // self.do_auto_steps();
    }

    pub fn get_winner(&self) -> Option<usize> {
        self.game.get_round_winner(&self.state)
    }

    pub fn new_round(&mut self) -> Option<usize> {
        match self.game.next_round(&self.state) {
            NextRoundResponse::Round(state) => {
                self.state = state;
                self.flush_ai_states();
                None
            }
            NextRoundResponse::Winner(id) => Some(id),
            _ => panic!(),
        }
    }

    pub fn show_full_state(&self) {
        let info = self.game.get_user_info(&self.state, USER_ID);
        let shared_cards = Card::serialize_vec(&info.shared_info.cards, false);
        println!("shared cards: \"{}\"", shared_cards);
        let enemies = info
            .shared_info
            .players
            .iter()
            .enumerate()
            .filter(|(id, p)| *id != USER_ID && p.has_cards)
            .map(|(_, p)| format!("{}: {}", p.name, p.balance))
            .collect::<Vec<_>>()
            .join(", ");
        println!("enemies: {}", enemies);
        println!(
            "current bet: {}, bank: {}",
            info.shared_info.get_max_bet(),
            info.shared_info.bank,
        );
        if !info.shared_info.players[USER_ID].has_cards {
            return;
        }
        println!(
            "your cards: {}, your bet: {}, your balance: {}",
            Card::serialize_vec(&info.cards, false),
            info.shared_info.players[USER_ID].bet,
            info.shared_info.players[USER_ID].balance,
        );
    }

    pub fn do_auto_steps(&mut self) {
        let mut applied = vec![];
        while let ExpectedAction::AutoStep(step) = self.get_expected_action() {
            applied.push(format!("{:?}", step));
            match self.game.apply_auto_step(&self.state) {
                Ok(val) => self.state = val,
                Err(err) => {
                    println!("applied: {:?}", applied);
                    panic!("ERR: {:?}", err);
                }
            }
        }
        println!("applied: {:?}", applied);
    }

    fn apply_step(&mut self, id: usize, step: Step) {
        println!("user: {}, step: {:?}", id, step);
        let new_state = match self.game.apply_user_step(&self.state, id, step) {
            Ok(val) => val,
            Err(err) => panic!(
                "error in apply AI step: {:?}, game state: {:?}",
                err, self.state
            ),
        };
        self.state = new_state;
    }

    fn flush_ai_states(&mut self) {
        for i in 0..self.ai.len() {
            self.ai[i].state = AiState::default()
        }
    }

    fn get_user_step(available: Vec<Step>) -> Step {
        let serializer = create_game_serializer(false);
        let str_map = available
            .iter()
            .map(|step| (serializer.step_to_str(step), step))
            .collect::<HashMap<_, _>>();
        println!("Available steps: {}", serializer.steps_to_str(&available));
        while let Some(line) = Self::get_line() {
            if let Some(step) = str_map.get(line.trim()) {
                return (*step).clone();
            } else {
                println!("invalid step")
            }
        }
        panic!("found EOF");
    }

    fn get_line() -> Option<String> {
        let res_line = io::stdin().lock().lines().next()?;
        Some(res_line.unwrap())
    }
}
