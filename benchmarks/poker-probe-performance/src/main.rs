use std::env;
use std::str::FromStr;
use std::time::SystemTime;

use poker::{Card, Suit, estimator_probe};

const HELP: &str = r#"
args:
  <samples: int> // repeat probe N times; required >= 10000
  <player_cards: str>
  <shared_cards: str>
  <number_of_enemies: int>
"#;

fn main() {
    let args = env::args().collect::<Vec<_>>();
    if args.len() != 5 {
        println!("{}", HELP);
        std::process::exit(1);
    }
    let count = usize::from_str(&args[1]).unwrap();
    let mut estimator = estimator_probe(count);
    let player_cards = Card::deserialize_vec(&args[2]).unwrap();
    let shared = Card::deserialize_vec(&args[3]).unwrap();
    let enemies = usize::from_str(&args[4]).unwrap();
    let begin = SystemTime::now();
    println!("shared: {:?}", shared);
    println!("user: {:?}", player_cards);
    println!("enemies: {}", enemies);
    let value = estimator.estimate(&shared, &player_cards, enemies);
    let end = SystemTime::now();
    let spent = end.duration_since(begin).unwrap();
    println!(
        "{}\"result\": {}, \"time\": {}{}",
        '{',
        value,
        spent.as_secs_f64(),
        '}'
    );
}
