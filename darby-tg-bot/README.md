# Poker telegram robot
### Short description
App includes dealer bot(accepts user commands, shows game status, sends cards, etc)
and 3 ai robots(Daniel, Terence, Dificento).
## How to run
- set environment variables:
  ```sh
  DARBY_DEALER_TOKEN=abc123 # Telegram token of main robot(dealer)
  DARBY_DB=local.db # Path to local sqlite base
  DARBY_INIT_BALANCE=100 #(Default=100) Init balance in new game
  DARBY_INACTIVE_STATE_LIFETIME=120 #(Default=216000) Max life time(secs) for inactive game 
  DARBY_AVAILABLE_AI=daniel,terence,dificento # List of available AI
  DARBY_AI_CONF_DIR=./ai_conf # Path to ai configs
  DARBY_SLEEP=1 #(Default=1) Sleep time(secs) in inactive mode 
  DARBY_TG_TIMEOUT=1 #(Default=1) Min interval between TG messages from one robot
  DARBY_TG_TRIES=10 #(Default=100) Max retries for one TG request 
  DARBY_REBUILD_TG_SENDER=no #(Default=yes) rebuild tg connector for each AI message
  ```
- create ai configs
  for each ai put 3 files in DARBY_AI_CONF_DIR
  for example `daniel`
  ```sh
  daniel_ai.json # ai config(look README.md in ../poker/)
  daniel_reactions.json # Reaction config
  daniel_token # file with TG token
  ```
  Reaction config schema:
  ```javascript
  {
    // reactions on winning round
    "win_round": {
      "prob": 1.0, // probability of reaction
      "phrases": ["a", "b"] // one of list will be chosen by random
    },
    // reactions on winning game
    "win_game": {"prob": ..., "phrases": ...},
    // reactions on loosing game
    "loose_game": {"prob": ..., "phrases": ...},
    // reactions on loosing round
    "loose_round": {"prob": ..., "phrases": ...},
    // reactions on new game
    "new_game": {"prob": ..., "phrases": ...},
    // reactions on other player "raise" step
    "raise": {"prob": ..., "phrases": ...}
  }
  // Special keys in phrase:
    - %name% - placeholder for winner name
    - %sep% - split one string on several messages
  ```
- run app
## How to play
Add all four robots(dealer and 3 ai robots) in one telegram conversations and check `/help` command for dealers
