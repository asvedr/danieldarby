use std::thread;

use crate::application::containers::{ai_bots, dealer_bot};
use crate::entities::errors::ConfigError;
use crate::proto::robot::IRobot;

pub struct App {
    robots: Vec<Box<dyn IRobot>>,
}

struct Wrapper {
    robot: Box<dyn IRobot>,
}

unsafe impl Send for Wrapper {}
unsafe impl Sync for Wrapper {}

impl Wrapper {
    fn run(mut self) {
        if let Err(err) = self.robot.run() {
            eprintln!("ROBOT {} FAILED: {:?}", self.robot.name(), err);
            std::process::exit(1)
        }
    }
}

impl App {
    pub fn new() -> Result<Self, ConfigError> {
        let mut robots = ai_bots()?;
        robots.push(dealer_bot()?);
        Ok(Self { robots })
    }

    pub fn run(self) {
        let mut handlers = Vec::new();
        for robot in self.robots {
            let wrapped = Wrapper { robot };
            handlers.push(thread::spawn(move || wrapped.run()));
        }
        for handler in handlers {
            handler.join().unwrap()
        }
    }
}
