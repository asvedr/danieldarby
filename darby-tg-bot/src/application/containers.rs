use lazy_static::lazy_static;

use crate::entities::errors::ConfigError;
use crate::entities::main_config::MainConfig;
use crate::entities::player::{NewPlayerStep, ReactionRequest, RobotName};
use crate::impls::ai_robot::AiRobotRequest;
use crate::impls::bases::ai_state_repo::AiStateRepo;
use crate::impls::bases::chat_state_repo::ChatStateRepo;
use crate::impls::bases::dealer_base::DealerBase;
use crate::impls::bases::sqlite::connection::SQLConnection;
use crate::impls::bases::user_repo::UserRepo;
use crate::impls::command_parser::CommandParser;
use crate::impls::dealer_robot::DealerRobot;
use crate::impls::dealer_to_player_bus::DealerToPlayerBus;
use crate::impls::queue::Queue;
use crate::impls::sync_tg_sender::SyncTgSender;
use crate::proto::robot::{IDealerToPlayerBus, IRobot};
use crate::utils::env::read_ai;

fn unwrap_init<T>(fun: impl Fn() -> Result<T, ConfigError>) -> T {
    match fun() {
        Ok(val) => val,
        Err(err) => panic!("CAN NOT INIT APP: {:?}", err),
    }
}

fn init_queues(config: &MainConfig) -> Vec<(RobotName, Queue<ReactionRequest>)> {
    config
        .available_ai
        .iter()
        .map(|name| {
            let q_name = format!("dealer_to_{}", name.to_str());
            (*name, Queue::new(q_name, None))
        })
        .collect()
}

lazy_static! {
    pub static ref CONFIG: MainConfig = unwrap_init(MainConfig::build);
    static ref SQL_CNN: SQLConnection = unwrap_init(|| SQLConnection::new(&CONFIG.dealer_db)
        .map_err(|err| ConfigError::CanNotStartBase(format!("{:?}", err))));
    static ref USER_REPO: UserRepo = UserRepo::new(SQL_CNN.clone());
    static ref CHAT_STATE_REPO: ChatStateRepo = ChatStateRepo::new(SQL_CNN.clone());
    static ref DEALER_BASE: DealerBase<UserRepo, ChatStateRepo> = DealerBase {
        user_repo: USER_REPO.clone(),
        chat_state_repo: CHAT_STATE_REPO.clone(),
    };
    static ref STEP_QUEUE: Queue<NewPlayerStep> = Queue::new("ai_to_dealer", None,);
    static ref REQUEST_QUEUES: Vec<(RobotName, Queue<ReactionRequest>)> = init_queues(&CONFIG);
}

pub fn dealer_bot() -> Result<Box<dyn IRobot>, ConfigError> {
    let buses = REQUEST_QUEUES
        .iter()
        .map(|(name, q)| -> Box<dyn IDealerToPlayerBus> {
            let bus = DealerToPlayerBus {
                robot: *name,
                queue: q.clone(),
            };
            Box::new(bus)
        })
        .collect();
    let robot = DealerRobot::new(
        DEALER_BASE.clone(),
        CommandParser::default(),
        STEP_QUEUE.clone(),
        CONFIG.clone(),
        buses,
    );
    Ok(Box::new(robot))
}

fn ai_bot(
    name: RobotName,
    config: &MainConfig,
    queue: Queue<ReactionRequest>,
) -> Result<Box<dyn IRobot>, ConfigError> {
    let (ai, reactions, token) = read_ai(&config.ai_conf_dir, name)
        .map_err(|err| ConfigError::CanNotReadAi(name, format!("{:?}", err)))?;
    let state_repo =
        AiStateRepo::new().map_err(|err| ConfigError::CanNotReadAi(name, format!("{:?}", err)))?;
    let robot = AiRobotRequest {
        name,
        token,
        sleep: config.sleep,
        bus_from_dealer: queue,
        bus_to_dealer: STEP_QUEUE.clone(),
        ai,
        state_repo,
        reactions_config: reactions,
    }
    .build::<SyncTgSender>(config);
    Ok(Box::new(robot))
}

pub fn ai_bots() -> Result<Vec<Box<dyn IRobot>>, ConfigError> {
    REQUEST_QUEUES
        .iter()
        .map(|(name, q)| ai_bot(*name, &CONFIG, q.clone()))
        .collect()
}
