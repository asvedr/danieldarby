use crate::entities::errors::QueueError;

pub trait IQueue<T> {
    fn max_len(&self) -> Option<usize>;
    fn push(&self, value: T) -> Result<(), QueueError>;
    fn pop(&self) -> Result<Option<T>, QueueError>;
    fn peek(&self, count: usize) -> Result<Vec<T>, QueueError>;
    fn filter(&self, predicate: &dyn Fn(&T) -> bool) -> Result<(), QueueError>;
}
