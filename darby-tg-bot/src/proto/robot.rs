use poker::{AiState, GameState, IGame, IGameSerializer, NextRoundResponse, RoundStateKind};
use std::collections::HashMap;

use crate::entities::db::UserInfo;
use crate::entities::errors::{DbError, RobotError};
use crate::entities::main_config::MainConfig;
use crate::entities::player::{PlayerCommand, ReactionRequest, RobotName};
use crate::entities::robot::Notifications;

pub trait IRobot {
    fn name(&self) -> &str;
    fn run(&mut self) -> Result<(), RobotError>;
}

pub trait IDealerBase {
    fn init(&self) -> Result<(), DbError>;
    fn get_state(&self, chat_id: i64) -> Result<GameState, DbError>;
    fn get_state_for_auto_step(&self) -> Result<(i64, GameState), DbError>;
    fn get_states_for_ai_step(&self) -> Result<Vec<(i64, GameState)>, DbError>;
    fn save_state(
        &self,
        chat_id: i64,
        ai_step_expected: bool,
        auto_step_expected: bool,
        state: GameState,
    ) -> Result<(), DbError>;
    fn del_state(&self, chat_id: i64) -> Result<(), DbError>;
    fn del_old_states(&self, threshold: usize) -> Result<(), DbError>;
    fn get_user_by_id(&self, id: i64) -> Result<UserInfo, DbError>;
    fn get_user_info(&self, key: &str, chat_id: i64) -> Result<UserInfo, DbError>;
    fn get_user_info_list(
        &self,
        keys: &[&str],
        chat_id: i64,
    ) -> Result<HashMap<String, UserInfo>, DbError>;
    fn update_user(&self, user: &UserInfo, chat_id: i64) -> Result<(), DbError>;
}

pub trait ICommandParser {
    fn parse_command(&self, text: &str) -> Option<PlayerCommand>;
    fn help(&self) -> String;
}

pub trait IPlayerBase {
    fn get_request(&self) -> Result<ReactionRequest, DbError>;
    fn del_request(&self, id: usize) -> Result<(), DbError>;
    fn get_state(&self, chat_id: i64) -> Result<AiState, DbError>;
    fn save_state(&self, chat_id: i64, state: AiState) -> Result<(), DbError>;
    fn del_state(&self, chat_id: i64) -> Result<(), DbError>;
}

pub trait IDealerRobotInner {
    fn game_manager(&self) -> &dyn IGame;
    fn ser(&self) -> &dyn IGameSerializer;
    fn db(&self) -> &dyn IDealerBase;
    fn get_robot(&self, name: RobotName) -> Result<&dyn IDealerToPlayerBus, RobotError>;
    fn get_player_name(&self, robot_name_or_id: &str) -> Result<String, RobotError>;
    fn is_robot_name(&self, name: &str) -> bool;
    fn get_state_notification(
        &self,
        chat: i64,
        kind: RoundStateKind,
        state: &GameState,
    ) -> Result<Notifications, RobotError>;
    fn get_lost_robots(&self, old_state: &GameState, new_state: &GameState) -> Vec<RobotName>;
}

pub trait IDealerToPlayerBus {
    fn robot(&self) -> RobotName;
    fn push_request(&self, request: ReactionRequest) -> Result<(), RobotError>;
}

pub trait IAutoStepNotifier {
    fn matches(&self, kind: &RoundStateKind) -> bool;
    fn make_notifications(
        &self,
        inner: &dyn IDealerRobotInner,
        chat_id: i64,
        state: &GameState,
    ) -> Result<Notifications, RobotError>;
}

pub struct AutoStepResult {
    pub state: Option<GameState>,
    pub notifications: Notifications,
    pub stop_game: bool,
}

pub trait IAutoStepWorkFlow {
    fn name(&self) -> &str;
    fn matches(&self, nrr: &NextRoundResponse) -> bool;
    fn process_state(
        &self,
        inner: &dyn IDealerRobotInner,
        chat_id: i64,
        state: &GameState,
        nrr: NextRoundResponse,
    ) -> Result<AutoStepResult, RobotError>;
}

pub trait ISyncTgSender {
    fn build(token: String, config: &MainConfig) -> Self;
    fn send_message(&self, receiver: i64, msgs: &[&str]) -> Result<(), RobotError>;
}
