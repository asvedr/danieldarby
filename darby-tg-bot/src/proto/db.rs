use crate::entities::db::UserInfo;
use poker::{AiState, GameState, Step};
use std::collections::HashMap;

use crate::entities::errors::{DbError, DbResult};
use crate::entities::player::{PlayerStep, RobotName};

pub trait INewDbRepo<Connect> {
    fn create(connect: Connect) -> Self;
}

pub trait IDbRepo {
    fn as_super(&self) -> &dyn IDbRepo {
        unimplemented!()
    }
    fn init(&self) -> DbResult<()> {
        self.as_super().init()
    }
    fn recreate_table(&self) -> DbResult<()> {
        self.as_super().recreate_table()
    }
    fn set_indexes(&self) -> DbResult<()> {
        self.as_super().set_indexes()
    }
    fn drop_indexes(&self) -> DbResult<()> {
        self.as_super().drop_indexes()
    }
    fn get_size(&self) -> DbResult<usize> {
        self.as_super().get_size()
    }
}

pub trait IStepRepo: IDbRepo {
    fn get_step(&self) -> Result<PlayerStep, DbError>;
    fn del_step(&self, id: usize) -> Result<(), DbError>;
    fn push_step(&self, player: RobotName, chat_id: i64, step: Step) -> Result<(), DbError>;
}

pub trait IUserRepo: IDbRepo {
    fn get_user_info(&self, key: &str, chat_id: i64) -> Result<UserInfo, DbError>;
    fn get_user_info_list(
        &self,
        keys: &[&str],
        chat_id: i64,
    ) -> Result<HashMap<String, UserInfo>, DbError>;
    fn get_user_info_list_by_id(&self, id_list: &[i64]) -> Result<Vec<UserInfo>, DbError>;
    fn update_user(&self, user: &UserInfo, chat_id: i64) -> Result<(), DbError>;
}

pub trait IChatStateRepo: IDbRepo {
    fn get_chat_state(&self, chat_id: i64) -> Result<GameState, DbError>;
    fn save_chat_state(
        &self,
        chat_id: i64,
        ai_step_expected: bool,
        auto_step_expected: bool,
        state: GameState,
    ) -> Result<(), DbError>;
    fn del_chat_states(&self, chat_id_list: &[i64]) -> Result<(), DbError>;
    fn get_state_to_auto_step(&self) -> Result<(i64, GameState), DbError>;
    fn get_states_to_ai_step(&self) -> Result<Vec<(i64, GameState)>, DbError>;
    fn get_too_old_states(&self, max_time: usize, limit: usize) -> Result<Vec<i64>, DbError>;
}

pub trait IAiStateRepo: IDbRepo {
    fn get_state(&self, chat_id: i64) -> Result<AiState, DbError>;
    fn save_state(&self, chat_id: i64, state: AiState) -> Result<(), DbError>;
    fn del_state(&self, chat_id: i64) -> Result<(), DbError>;
}
