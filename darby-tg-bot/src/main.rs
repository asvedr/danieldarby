use crate::application::app::App;

mod application;
mod entities;
mod impls;
mod proto;
mod utils;

fn main() {
    let app = match App::new() {
        Ok(val) => val,
        Err(err) => panic!("init error: {:?}", err),
    };
    app.run()
}
