use std::env;
use std::env::VarError;
use std::fmt::Debug;
use std::fs::File;
use std::io::Read;
use std::str::FromStr;

use poker::{create_ai_deserializer, IAi};

use crate::entities::ai_config::ReactionsConfig;
use crate::entities::errors::ConfigError;
use crate::entities::player::RobotName;

pub fn parse_var<T, E: Debug, F: Fn(&str) -> Result<T, E>>(
    name: &str,
    parser: F,
) -> Result<T, ConfigError> {
    let val = match env::var(name) {
        Ok(val) => val,
        Err(VarError::NotPresent) => return Err(ConfigError::VariableNotSet(name.to_string())),
        Err(VarError::NotUnicode(_)) => {
            return Err(ConfigError::InvalidVariable(
                name.to_string(),
                "not unicode".to_string(),
            ))
        }
    };
    let stripped = val.trim();
    if stripped.is_empty() {
        return Err(ConfigError::VariableNotSet(name.to_string()));
    }
    parser(stripped).map_err(|e| ConfigError::InvalidVariable(name.to_string(), format!("{:?}", e)))
}

pub fn parse_var_opt<T, E: Debug, F: Fn(&str) -> Result<T, E>, D: Fn() -> T>(
    name: &str,
    parser: F,
    default: D,
) -> Result<T, ConfigError> {
    match parse_var(name, parser) {
        Err(ConfigError::VariableNotSet(_)) => Ok(default()),
        other => other,
    }
}

pub fn as_str(val: &str) -> Result<String, ()> {
    Ok(val.to_string())
}

fn read_file(path: &str) -> Result<String, ConfigError> {
    let mut file = File::open(path).map_err(|_| ConfigError::CanNotReadFile(path.to_string()))?;
    let mut buffer = String::new();
    file.read_to_string(&mut buffer)
        .map_err(|_| ConfigError::CanNotReadFile(path.to_string()))?;
    Ok(buffer)
}

pub fn read_ai(
    base_dir: &str,
    robot: RobotName,
) -> Result<(Box<dyn IAi>, ReactionsConfig, String), ConfigError> {
    let ai_config_path = format!("{}/{}_ai.json", base_dir, robot.to_str());
    let reactions_path = format!("{}/{}_reactions.json", base_dir, robot.to_str());
    let token_path = format!("{}/{}_token", base_dir, robot.to_str());
    let ai_config_src = read_file(&ai_config_path)?;
    let des = create_ai_deserializer();
    let ai = des
        .json_to_ai(&ai_config_src)
        .map_err(|err| ConfigError::CanNotParseFile(ai_config_path, format!("{:?}", err)))?;
    let reactions_src = read_file(&reactions_path)?;
    let reactions = ReactionsConfig::from_str(&reactions_src)?;
    let token = read_file(&token_path)?;
    Ok((ai, reactions, token.trim().to_string()))
}
