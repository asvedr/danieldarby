use std::str::Chars;

pub fn find_index<T>(values: &[T], predicates: &[&dyn Fn(&T) -> bool]) -> Option<usize> {
    for predicate in predicates {
        for (i, val) in values.iter().enumerate() {
            if predicate(val) {
                return Some(i);
            }
        }
    }
    None
}

pub fn find_item<T, F: Fn(&T) -> bool>(vec: &[T], predicate: F) -> &T {
    for item in vec {
        if predicate(item) {
            return item;
        }
    }
    unreachable!()
}

fn capitalize_common(mut chars: Chars) -> String {
    if let Some(first) = chars.next() {
        let mut res = first.to_uppercase().to_string();
        res.push_str(&chars.collect::<String>());
        res
    } else {
        String::new()
    }
}

fn capitalize_no_prefix(prefix: &str, name: &str) -> String {
    let mut chars = name.chars();
    let mut result = String::new();
    for _ in 0..prefix.len() {
        result.push(chars.next().unwrap());
    }
    result.push_str(&capitalize_common(chars));
    result
}

pub fn capitalize_name(name: &str) -> String {
    let mut result = String::new();
    for token in name.split(' ') {
        let capitalized = match () {
            _ if token.starts_with('@') => capitalize_no_prefix("@", token),
            _ if token.starts_with("(@") => capitalize_no_prefix("(@", token),
            _ => capitalize_common(token.chars()),
        };
        if !result.is_empty() {
            result.push(' ');
        }
        result.push_str(&capitalized);
    }
    result
}

#[cfg(test)]
mod tests {
    use crate::utils::algo::capitalize_name;

    #[test]
    fn test_capitalize() {
        assert_eq!(capitalize_name("@abc"), "@Abc");
        assert_eq!(capitalize_name("alyx vance (@aven)"), "Alyx Vance (@Aven)")
    }
}
