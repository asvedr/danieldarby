use std::sync::Arc;

use futures::StreamExt;
use poker::errors::GameError;
#[cfg(test)]
use poker::GameState;
use poker::{create_game_serializer, game_manager, Step};
use telegram_bot_ars::{AllowedUpdate, Api, ChatId, MessageKind, SendMessage, UpdateKind, UserId};
use tokio::task;

use crate::entities::db::UserInfo;
use crate::entities::errors::{DbError, RobotError};
use crate::entities::main_config::MainConfig;
use crate::entities::player::{NewPlayerStep, PlayerCommand, RobotName};
use crate::entities::robot::{Notification, Notifications};
use crate::impls::auto_step_notifiers;
use crate::impls::auto_step_workflow;
use crate::impls::dealer_robot_inner::Inner;
use crate::proto::queue::IQueue;
use crate::proto::robot::{ICommandParser, IDealerBase, IDealerToPlayerBus, IRobot};

const MSG_GAME_NOT_FOUND: &str = "No active games in this chat";
const MSG_TOO_SMALL_PLAYERS: &str = "Need at least 2 players";
const MSG_INVALID_STEP: &str = "Invalid step";
const MSG_NOT_YOUR_STEP: &str = "It's not your step now";
const MSG_UNKNOWN_USER: &str = "Unknown user: %name%";
const MSG_PLAYER_NOT_IN_GAME: &str = "You are not in the game";

pub struct DealerRobot<DB, CP, Q> {
    inner: Arc<Inner<DB, CP>>,
    ai_steps: Q,
}

unsafe impl<A, B, Q: Clone> Send for DealerRobot<A, B, Q> {}
unsafe impl<A, B, Q: Clone> Sync for DealerRobot<A, B, Q> {}

impl<A, B, Q: Clone> Clone for DealerRobot<A, B, Q> {
    fn clone(&self) -> Self {
        Self {
            inner: self.inner.clone(),
            ai_steps: self.ai_steps.clone(),
        }
    }
}

impl<
        DB: IDealerBase + 'static,
        CP: ICommandParser + 'static,
        Q: IQueue<NewPlayerStep> + Clone + Send + 'static,
    > DealerRobot<DB, CP, Q>
{
    pub fn new(
        db: DB,
        parser: CP,
        ai_steps: Q,
        config: MainConfig,
        robots: Vec<Box<dyn IDealerToPlayerBus>>,
    ) -> Self {
        db.init().unwrap();
        let inner = Inner {
            db,
            parser,
            robots,
            auto_step_notifiers: auto_step_notifiers::all(),
            auto_step_workflow: auto_step_workflow::all(),
            config,
            game_manager: game_manager(),
            ser: create_game_serializer(false),
        };
        Self {
            inner: Arc::new(inner),
            ai_steps,
        }
    }

    #[cfg(test)]
    pub fn auto_step(&mut self, game: &GameState) -> Result<GameState, RobotError> {
        Ok(self.inner.game_manager.apply_auto_step(game)?)
    }

    pub fn make_ai_step(
        &mut self,
        robot: RobotName,
        char_id: i64,
        step: Step,
    ) -> Result<Notifications, RobotError> {
        self.inner.make_player_step(robot.to_str(), char_id, step)
    }

    pub fn find_and_make_auto_step(&mut self) -> Notifications {
        self.inner.find_and_make_auto_step()
    }

    pub fn remove_old_states(&mut self) {
        let res = self
            .inner
            .db
            .del_old_states(self.inner.config.inactive_state_lifetime);
        if let Err(err) = res {
            eprintln!("removing old states error: {:?}", err);
        }
    }

    async fn listen_robots_task(&mut self, api: Api) {
        loop {
            let mut steps = match self.ai_steps.peek(1) {
                Ok(val) => val,
                Err(err) => panic!("Ai bus peek error: {:?}", err),
            };
            if steps.is_empty() {
                task::yield_now().await;
                continue;
            }
            let step = steps.pop().unwrap();
            match self.make_ai_step(step.player, step.chat_id, step.step) {
                Ok(notifs) => self.send_notifications(&api, notifs).await,
                Err(err) => panic!("Can not apply AI step: {:?}", err),
            };
            if let Err(err) = self.ai_steps.pop() {
                panic!("Pop ai queue error: {:?}", err)
            };
            task::yield_now().await
        }
    }

    async fn auto_step_task(&mut self, api: Api) {
        loop {
            self.remove_old_states();
            let notifications = self.find_and_make_auto_step();
            let empty = notifications.is_empty();
            self.send_notifications(&api, notifications).await;
            // for notif in notifications {
            //     let msg = SendMessage::new(ChatId::from(notif.tg_id), notif.msg);
            //     if let Err(err) = api.send(msg).await {
            //         eprintln!("autostep send msg error: {:?}", err)
            //     };
            // }
            if empty {
                task::yield_now().await;
            }
        }
    }

    async fn send_notifications(&self, api: &Api, notifications: Notifications) {
        for notif in notifications {
            let msg = SendMessage::new(ChatId::from(notif.tg_id), notif.msg);
            if let Err(err) = api.send(msg).await {
                eprintln!("autostep send msg error: {:?}", err)
            }
        }
    }

    async fn arun(&mut self) -> Result<(), RobotError> {
        let api = Api::new(&self.inner.config.token);
        let api_clone = api.clone();
        let mut self_clone = self.clone();
        let _task_1 = task::spawn(async move { self_clone.listen_robots_task(api_clone).await });
        let api_clone = api.clone();
        let mut self_clone = self.clone();
        let _task_2 = task::spawn(async move { self_clone.auto_step_task(api_clone).await });
        let mut stream = api.stream();
        stream.allowed_updates(&[AllowedUpdate::Message]);
        while let Some(update) = stream.next().await {
            // If the received update contains a new message...
            let update = match update {
                Ok(val) => val,
                Err(err) => {
                    eprintln!("Invalid tg update: {:?}", err);
                    continue;
                }
            };
            let message = match update.kind {
                Some(UpdateKind::Message(val)) => val,
                _ => continue,
            };
            let user = UserInfo {
                first_name: message.from.first_name.to_lowercase(),
                last_name: Self::normalize_opt_name(&message.from.last_name),
                username: Self::normalize_opt_name(&message.from.username),
                tg_id: message.from.id.into(),
                is_robot: false,
            };
            let chat_id = message.chat.id();
            self.inner.db.update_user(&user, chat_id.into())?;
            let text = match message.kind {
                MessageKind::Text { ref data, .. } => data,
                _ => continue,
            };
            let command = match self.inner.parser.parse_command(text) {
                Some(val) => val,
                _ if self.inner.is_game_active(chat_id.into())? => {
                    api.send(SendMessage::new(chat_id, "Invalid command"))
                        .await?;
                    continue;
                }
                _ => continue,
            };
            let resp = match self.execute_command(user, chat_id.into(), command) {
                Ok(val) => val,
                Err(err) => {
                    api.send(SendMessage::new(chat_id, format!("ERR: {:?}", err)))
                        .await?;
                    continue;
                }
            };
            for Notification { tg_id, msg } in resp {
                let msg = if tg_id > 0 {
                    SendMessage::new(UserId::from(tg_id), msg)
                } else {
                    SendMessage::new(ChatId::from(tg_id), msg)
                };
                api.send(msg).await?;
            }
        }
        Ok(())
    }

    pub fn execute_command(
        &self,
        user: UserInfo,
        chat_id: i64,
        command: PlayerCommand,
    ) -> Result<Notifications, RobotError> {
        match self.inner.execute_command(user, chat_id, command) {
            Ok(val) => Ok(val),
            Err(RobotError::DbError(DbError::NotFound(_))) => {
                Self::make_msg(MSG_GAME_NOT_FOUND, chat_id)
            }
            Err(RobotError::GameError(GameError::TooSmallPlayers)) => {
                Self::make_msg(MSG_TOO_SMALL_PLAYERS, chat_id)
            }
            Err(RobotError::GameError(GameError::InvalidStep)) => {
                Self::make_msg(MSG_INVALID_STEP, chat_id)
            }
            Err(RobotError::GameError(GameError::NotYourStep)) => {
                Self::make_msg(MSG_NOT_YOUR_STEP, chat_id)
            }
            Err(RobotError::UnknownUser(name)) => {
                Self::make_msg(MSG_UNKNOWN_USER.replace("%name%", &name), chat_id)
            }
            Err(RobotError::PlayerNotInGame) => Self::make_msg(MSG_PLAYER_NOT_IN_GAME, chat_id),
            Err(err) => Err(err),
        }
    }

    pub fn notify_ai_about_steps_on_init(&self) -> Result<(), RobotError> {
        self.inner.notify_ai_about_steps()
    }

    fn make_msg<T: ToString>(msg: T, tg_id: i64) -> Result<Notifications, RobotError> {
        let notif = Notification {
            tg_id,
            msg: msg.to_string(),
        };
        Ok(vec![notif])
    }

    fn normalize_opt_name(name: &Option<String>) -> String {
        match name {
            None => "".to_string(),
            Some(val) => val.trim().to_lowercase(),
        }
    }
}

impl<
        DB: IDealerBase + 'static,
        CP: ICommandParser + 'static,
        Q: IQueue<NewPlayerStep> + Clone + Send + 'static,
    > IRobot for DealerRobot<DB, CP, Q>
{
    fn name(&self) -> &str {
        "dealer"
    }

    fn run(&mut self) -> Result<(), RobotError> {
        self.notify_ai_about_steps_on_init()?;
        tokio::runtime::Builder::new_current_thread()
            .enable_all()
            // .basic_scheduler()
            .build()
            .unwrap()
            .block_on(self.arun())
    }
}
