use crate::entities::errors::RobotError;
use crate::entities::robot::{Notification, Notifications};
use crate::proto::robot::{IAutoStepNotifier, IDealerRobotInner};
use poker::{GameState, RoundStateKind};
use std::str::FromStr;

#[derive(Default, Clone)]
pub struct Notifier<SIN> {
    shared_info_notifier: SIN,
}

impl<SIN: IAutoStepNotifier> IAutoStepNotifier for Notifier<SIN> {
    fn matches(&self, kind: &RoundStateKind) -> bool {
        matches!(kind, RoundStateKind::SmallBlinds)
    }

    fn make_notifications(
        &self,
        inner: &dyn IDealerRobotInner,
        chat_id: i64,
        state: &GameState,
    ) -> Result<Notifications, RobotError> {
        let ids = state
            .round
            .players
            .iter()
            .enumerate()
            .filter(|(_, p)| p.active)
            .map(|(id, _)| id);
        let mut result = self
            .shared_info_notifier
            .make_notifications(inner, chat_id, state)?;
        for id in ids {
            let name = &state.players[id].name;
            if inner.is_robot_name(name) {
                continue;
            }
            let tg_id = i64::from_str(name).unwrap();
            let cards = inner.game_manager().get_user_info(state, id).cards;
            let msg = format!("your cards: {}", inner.ser().cards_to_str(&cards));
            let notif = Notification { tg_id, msg };
            result.push(notif)
        }
        Ok(result)
    }
}
