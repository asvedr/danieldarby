use crate::proto::robot::IAutoStepNotifier;

mod all_folded;
mod open_all;
mod shared_info;
mod small_blinds;

pub fn all() -> Vec<Box<dyn IAutoStepNotifier>> {
    use shared_info::Notifier as SIN;
    let sb: small_blinds::Notifier<SIN> = Default::default();
    let oa: open_all::Notifier<SIN> = Default::default();
    vec![
        Box::new(sb),
        Box::new(SIN::default()),
        Box::new(oa),
        Box::new(all_folded::Notifier::default()),
    ]
}
