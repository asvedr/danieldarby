use poker::{GameState, RoundStateKind};

use crate::entities::errors::RobotError;
use crate::entities::robot::{Notification, Notifications};
use crate::proto::robot::{IAutoStepNotifier, IDealerRobotInner};

#[derive(Default, Clone)]
pub struct Notifier<SIN> {
    shared_info_notifier: SIN,
}

impl<SIN: IAutoStepNotifier> IAutoStepNotifier for Notifier<SIN> {
    fn matches(&self, kind: &RoundStateKind) -> bool {
        matches!(kind, RoundStateKind::OpenAll)
    }

    fn make_notifications(
        &self,
        inner: &dyn IDealerRobotInner,
        chat_id: i64,
        state: &GameState,
    ) -> Result<Notifications, RobotError> {
        let mut result = self
            .shared_info_notifier
            .make_notifications(inner, chat_id, state)?;
        let shared_cards = state.round.shared_cards.clone();
        let msg = state
            .round
            .players
            .iter()
            .enumerate()
            .filter(|(_, p)| p.active)
            .map(|(id, p)| {
                let name = &state.players[id].name;
                let name = inner.get_player_name(name)?;
                let mut total_cards = p.cards.clone();
                total_cards.extend_from_slice(&shared_cards);
                let s_cards = inner.ser().cards_to_str(&total_cards);
                Ok(format!("{}: {}", name, s_cards))
            })
            .collect::<Result<Vec<String>, RobotError>>()?
            .join("\n");
        let notif = Notification {
            tg_id: chat_id,
            msg,
        };
        result.push(notif);
        Ok(result)
    }
}
