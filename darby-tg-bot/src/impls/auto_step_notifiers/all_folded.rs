use poker::{GameState, RoundStateKind};

use crate::entities::errors::RobotError;
use crate::entities::robot::{Notification, Notifications};
use crate::proto::robot::{IAutoStepNotifier, IDealerRobotInner};

#[derive(Default, Clone)]
pub struct Notifier {}

impl IAutoStepNotifier for Notifier {
    fn matches(&self, kind: &RoundStateKind) -> bool {
        matches!(kind, RoundStateKind::AllFolded)
    }

    fn make_notifications(
        &self,
        inner: &dyn IDealerRobotInner,
        chat_id: i64,
        state: &GameState,
    ) -> Result<Notifications, RobotError> {
        let (id, _) = state
            .round
            .players
            .iter()
            .enumerate()
            .find(|(_, p)| p.active)
            .unwrap();
        let name = inner.get_player_name(&state.players[id].name)?;
        let msg = format!("All folded except {}", name);
        let notif = Notification {
            tg_id: chat_id,
            msg,
        };
        Ok(vec![notif])
    }
}
