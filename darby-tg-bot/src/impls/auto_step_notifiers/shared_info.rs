use poker::{GameState, RoundStateKind};

use crate::entities::errors::RobotError;
use crate::entities::robot::{Notification, Notifications};
use crate::proto::robot::{IAutoStepNotifier, IDealerRobotInner};

#[derive(Default, Clone)]
pub struct Notifier {}

impl IAutoStepNotifier for Notifier {
    fn matches(&self, kind: &RoundStateKind) -> bool {
        matches!(
            kind,
            RoundStateKind::NewCard { .. } | RoundStateKind::NewBet { .. }
        )
    }

    fn make_notifications(
        &self,
        inner: &dyn IDealerRobotInner,
        chat_id: i64,
        state: &GameState,
    ) -> Result<Notifications, RobotError> {
        let info = inner.game_manager().get_shared_info(state);
        let players = info
            .players
            .iter()
            .enumerate()
            .filter(|(_, p)| p.has_cards)
            .map(|(id, p)| {
                let name = &state.players[id].name;
                let msg = format!(
                    "-  {}, bet={}, balance={}",
                    inner.get_player_name(name)?,
                    p.bet,
                    p.balance,
                );
                Ok(msg)
            })
            .collect::<Result<Vec<String>, RobotError>>()?
            .join("\n");
        let msg = format!(
            "players:\n{}\ncards:{}\nbank: {}\ndealer: {}",
            players,
            inner.ser().cards_to_str(&info.cards),
            info.bank,
            info.dealer,
        );
        let notif = Notification {
            tg_id: chat_id,
            msg,
        };
        Ok(vec![notif])
    }
}
