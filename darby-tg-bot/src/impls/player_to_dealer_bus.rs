use crate::entities::errors::RobotError;
use crate::entities::player::{NewPlayerStep, RobotName};
use crate::proto::queue::IQueue;
use crate::proto::robot::IPlayerToDealerBus;
use poker::Step;

pub struct PlayerToDealerBus<Q: IQueue<NewPlayerStep>> {
    pub name: RobotName,
    pub queue: Q,
}

impl<Q: IQueue<NewPlayerStep>> IPlayerToDealerBus for PlayerToDealerBus<Q> {
    fn push_step(&self, chat_id: i64, step: Step) -> Result<(), RobotError> {
        let value = NewPlayerStep {
            player: self.name,
            chat_id,
            step,
        };
        self.queue.push(value)?;
        // self.step_repo.push_step(self.name, chat_id, step)?;
        Ok(())
    }
}
