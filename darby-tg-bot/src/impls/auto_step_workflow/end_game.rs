use std::str::FromStr;

use poker::{GameState, NextRoundResponse};

use crate::entities::errors::RobotError;
use crate::entities::player::{ReactionRequest, RobotName};
use crate::entities::robot::Notification;
use crate::proto::robot::{AutoStepResult, IAutoStepWorkFlow, IDealerRobotInner};

#[derive(Default)]
pub struct WorkFlow {}

impl WorkFlow {
    fn get_lost_robots(
        inner: &dyn IDealerRobotInner,
        old_state: &GameState,
        winner: usize,
    ) -> Vec<RobotName> {
        let mut new_state = GameState {
            players: old_state.players.clone(),
            ..Default::default()
        };
        for i in 0..new_state.players.len() {
            new_state.players[i].balance = 0;
        }
        new_state.players[winner].balance = 1;
        inner.get_lost_robots(old_state, &new_state)
    }
}

impl IAutoStepWorkFlow for WorkFlow {
    fn name(&self) -> &str {
        "end_game"
    }

    fn matches(&self, nrr: &NextRoundResponse) -> bool {
        matches!(nrr, NextRoundResponse::Winner(_))
    }

    fn process_state(
        &self,
        inner: &dyn IDealerRobotInner,
        chat_id: i64,
        old_state: &GameState,
        nrr: NextRoundResponse,
    ) -> Result<AutoStepResult, RobotError> {
        inner.db().del_state(chat_id)?;
        // inner.win_notifier().make_notifications(self, chat_id, old_state)?;
        let id = match nrr {
            NextRoundResponse::Winner(val) => val,
            _ => unreachable!(),
        };
        let winner = &old_state.players[id].name;
        if inner.is_robot_name(winner) {
            let bus = inner.get_robot(RobotName::from_str(winner).unwrap())?;
            bus.push_request(ReactionRequest::WinGame { chat_id })?;
        }
        let name = inner.get_player_name(winner)?;
        let robots = Self::get_lost_robots(inner, old_state, id);
        for robot in robots {
            let request = ReactionRequest::LooseGame {
                chat_id,
                winner: name.clone(),
            };
            inner.get_robot(robot)?.push_request(request)?;
        }
        let notif = Notification {
            tg_id: chat_id,
            msg: format!("Game winner: {}", name),
        };
        Ok(AutoStepResult {
            state: None,
            notifications: vec![notif],
            stop_game: true,
        })
    }
}
