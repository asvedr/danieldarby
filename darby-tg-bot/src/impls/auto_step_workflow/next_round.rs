use std::str::FromStr;

use poker::{GameState, NextRoundResponse};

use crate::entities::errors::RobotError;
use crate::entities::player::{ReactionRequest, RobotName};
use crate::entities::robot::Notification;
use crate::proto::robot::{AutoStepResult, IAutoStepWorkFlow, IDealerRobotInner};

#[derive(Default)]
pub struct WorkFlow {}

impl WorkFlow {
    fn get_lost_round_robots(
        inner: &dyn IDealerRobotInner,
        state: &GameState,
        winner: usize,
        full_lost: &[RobotName],
    ) -> Vec<RobotName> {
        let mut result = Vec::new();
        for (id, player) in state.players.iter().enumerate() {
            if id == winner || !inner.is_robot_name(&player.name) {
                continue;
            }
            let name = RobotName::from_str(&player.name).unwrap();
            if !full_lost.contains(&name) {
                result.push(name)
            }
        }
        result
    }
}

impl IAutoStepWorkFlow for WorkFlow {
    fn name(&self) -> &str {
        "next_round"
    }

    fn matches(&self, nrr: &NextRoundResponse) -> bool {
        matches!(nrr, NextRoundResponse::Round(_))
    }

    fn process_state(
        &self,
        inner: &dyn IDealerRobotInner,
        chat_id: i64,
        old_state: &GameState,
        nrr: NextRoundResponse,
    ) -> Result<AutoStepResult, RobotError> {
        let new_state = match nrr {
            NextRoundResponse::Round(val) => val,
            _ => unreachable!(),
        };
        let winner_id = inner.game_manager().get_round_winner(old_state).unwrap();
        let winner = &old_state.players[winner_id].name;
        if inner.is_robot_name(winner) {
            let bus = inner.get_robot(RobotName::from_str(winner).unwrap())?;
            bus.push_request(ReactionRequest::WinRound { chat_id })?;
        }
        for (id, p) in new_state.round.players.iter().enumerate() {
            if !p.active || id == winner_id {
                continue;
            }
            let name = &new_state.players[id].name;
            if !inner.is_robot_name(name) {
                continue;
            }
            let bus = inner.get_robot(RobotName::from_str(name).unwrap())?;
            bus.push_request(ReactionRequest::NewRound { chat_id })?;
        }
        let winner = inner.get_player_name(winner)?;
        let lost_robots = inner.get_lost_robots(old_state, &new_state);
        let lost_round_robots =
            Self::get_lost_round_robots(inner, old_state, winner_id, &lost_robots);
        let msg = match inner.game_manager().get_round_winner(old_state) {
            None => "New round".to_string(),
            Some(id) => {
                let player = &new_state.players[id].name;
                let player = inner.get_player_name(player)?;
                format!("Round winner: {}", player)
            }
        };
        for robot in lost_robots {
            let bus = inner.get_robot(robot)?;
            bus.push_request(ReactionRequest::LooseGame {
                chat_id,
                winner: winner.clone(),
            })?;
        }
        for robot in lost_round_robots {
            let bus = inner.get_robot(robot)?;
            bus.push_request(ReactionRequest::LooseRound {
                chat_id,
                winner: winner.clone(),
            })?;
        }
        let notifications = vec![Notification {
            tg_id: chat_id,
            msg,
        }];
        Ok(AutoStepResult {
            state: Some(new_state),
            notifications,
            stop_game: false,
        })
    }
}
