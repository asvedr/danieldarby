use poker::{ExpectedAction, GameState, NextRoundResponse};

use crate::entities::errors::RobotError;
use crate::entities::robot::Notification;
use crate::proto::robot::{AutoStepResult, IAutoStepWorkFlow, IDealerRobotInner};

#[derive(Default)]
pub struct WorkFlow {}

impl IAutoStepWorkFlow for WorkFlow {
    fn name(&self) -> &str {
        "common"
    }

    fn matches(&self, nrr: &NextRoundResponse) -> bool {
        matches!(nrr, NextRoundResponse::NotReady)
    }

    fn process_state(
        &self,
        inner: &dyn IDealerRobotInner,
        chat_id: i64,
        state: &GameState,
        _: NextRoundResponse,
    ) -> Result<AutoStepResult, RobotError> {
        let expected_action = match inner.game_manager().get_expected_action(state) {
            ExpectedAction::AutoStep(val) => val,
            val => panic!("Autostep not expected. Expected: {:?}", val),
        };
        let new_state = match inner.game_manager().apply_auto_step(state) {
            Ok(val) => val,
            Err(err) => {
                let _ = inner.db().del_state(chat_id);
                let msg = format!("AUTO STEP ERROR: {:?}", err);
                let notif = Notification {
                    tg_id: chat_id,
                    msg,
                };
                return Ok(AutoStepResult {
                    state: None,
                    notifications: vec![notif],
                    stop_game: false,
                });
            }
        };
        let result = inner.get_state_notification(chat_id, expected_action, &new_state)?;
        Ok(AutoStepResult {
            state: Some(new_state),
            notifications: result,
            stop_game: false,
        })
    }
}
