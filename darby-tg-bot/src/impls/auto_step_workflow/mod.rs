use crate::proto::robot::IAutoStepWorkFlow;

mod common;
mod end_game;
mod next_round;

pub fn all() -> Vec<Box<dyn IAutoStepWorkFlow>> {
    vec![
        Box::new(next_round::WorkFlow::default()),
        Box::new(end_game::WorkFlow::default()),
        Box::new(common::WorkFlow::default()),
    ]
}
