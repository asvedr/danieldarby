use std::collections::HashMap;

use poker::GameState;

use crate::entities::db::UserInfo;
use crate::entities::errors::DbError;
use crate::proto::db::{IChatStateRepo, IUserRepo};
use crate::proto::robot::IDealerBase;

#[derive(Clone)]
pub struct DealerBase<UR, CSR> {
    pub user_repo: UR,
    pub chat_state_repo: CSR,
}

impl<UR: IUserRepo, CSR: IChatStateRepo> IDealerBase for DealerBase<UR, CSR> {
    fn init(&self) -> Result<(), DbError> {
        self.user_repo.init()?;
        self.chat_state_repo.init()
    }

    fn get_state(&self, chat_id: i64) -> Result<GameState, DbError> {
        self.chat_state_repo.get_chat_state(chat_id)
    }

    fn get_state_for_auto_step(&self) -> Result<(i64, GameState), DbError> {
        self.chat_state_repo.get_state_to_auto_step()
    }

    fn get_states_for_ai_step(&self) -> Result<Vec<(i64, GameState)>, DbError> {
        self.chat_state_repo.get_states_to_ai_step()
    }

    fn save_state(
        &self,
        chat_id: i64,
        ai_step_expected: bool,
        auto_step_expected: bool,
        state: GameState,
    ) -> Result<(), DbError> {
        self.chat_state_repo
            .save_chat_state(chat_id, ai_step_expected, auto_step_expected, state)
    }

    fn del_state(&self, chat_id: i64) -> Result<(), DbError> {
        self.chat_state_repo.del_chat_states(&[chat_id])
    }

    fn del_old_states(&self, threshold: usize) -> Result<(), DbError> {
        let id_list = self.chat_state_repo.get_too_old_states(threshold, 100)?;
        self.chat_state_repo.del_chat_states(&id_list)
    }

    fn get_user_by_id(&self, id: i64) -> Result<UserInfo, DbError> {
        let mut res = self.user_repo.get_user_info_list_by_id(&[id])?;
        Ok(res.remove(0))
    }

    fn get_user_info(&self, key: &str, chat_id: i64) -> Result<UserInfo, DbError> {
        self.user_repo.get_user_info(key, chat_id)
    }

    fn get_user_info_list(
        &self,
        keys: &[&str],
        chat_id: i64,
    ) -> Result<HashMap<String, UserInfo>, DbError> {
        self.user_repo.get_user_info_list(keys, chat_id)
    }

    fn update_user(&self, user: &UserInfo, chat_id: i64) -> Result<(), DbError> {
        self.user_repo.update_user(user, chat_id)
    }
}
