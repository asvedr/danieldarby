use crate::entities::errors::DbError;
use crate::impls::bases::chat_state_repo::ChatStateRepo;
use crate::impls::bases::sqlite::connection::SQLConnection;
use crate::proto::db::{IChatStateRepo, IDbRepo};
use poker::GameState;
use std::thread;
use std::time::Duration;

fn make_repo() -> ChatStateRepo {
    let repo = ChatStateRepo::new(SQLConnection::new(":memory:").unwrap());
    repo.init().unwrap();
    repo
}

#[test]
fn test_get_empty() {
    let repo = make_repo();
    assert_eq!(repo.get_chat_state(1), Err(DbError::NotFound(None)));
    assert_eq!(repo.get_states_to_ai_step(), Ok(Vec::new()));
    assert_eq!(repo.get_state_to_auto_step(), Err(DbError::NotFound(None)));
    assert_eq!(repo.get_too_old_states(1, 1), Ok(Vec::new()));
}

#[test]
fn test_save_get_state() {
    let repo = make_repo();
    let state = GameState::default();

    repo.save_chat_state(1, false, false, state.clone())
        .unwrap();
    let got = repo.get_chat_state(1).unwrap();
    assert_eq!(got, state);
    assert_eq!(repo.get_states_to_ai_step(), Ok(Vec::new()));
    assert_eq!(repo.get_state_to_auto_step(), Err(DbError::NotFound(None)));

    repo.save_chat_state(1, true, false, state.clone()).unwrap();
    let got = repo.get_chat_state(1).unwrap();
    assert_eq!(got, state);
    assert_eq!(repo.get_states_to_ai_step(), Ok(vec![(1, state.clone())]));
    assert_eq!(repo.get_state_to_auto_step(), Err(DbError::NotFound(None)));

    repo.save_chat_state(1, false, true, state.clone()).unwrap();
    let got = repo.get_chat_state(1).unwrap();
    assert_eq!(got, state);
    assert_eq!(repo.get_states_to_ai_step(), Ok(Vec::new()));
    assert_eq!(repo.get_state_to_auto_step(), Ok((1, state)));
}

#[test]
fn test_ready_to_del_state() {
    let repo = make_repo();
    repo.save_chat_state(1, false, false, Default::default())
        .unwrap();
    thread::sleep(Duration::from_secs(1));
    assert!(repo.get_too_old_states(10, 100).unwrap().is_empty());
    assert_eq!(repo.get_too_old_states(1, 10).unwrap(), &[1])
}

#[test]
fn test_del_state() {
    let repo = make_repo();
    repo.save_chat_state(1, false, false, Default::default())
        .unwrap();
    assert!(repo.get_chat_state(1).is_ok());
    repo.del_chat_states(&[1]).unwrap();
    assert_eq!(repo.get_chat_state(1), Err(DbError::NotFound(None)))
}
