use crate::entities::db::UserInfo;
use crate::entities::errors::DbError;
use crate::impls::bases::sqlite::connection::SQLConnection;
use crate::impls::bases::user_repo::UserRepo;
use crate::proto::db::{IDbRepo, IUserRepo};
use std::collections::HashMap;

fn make_repo() -> UserRepo {
    let cnn = SQLConnection::new(":memory:").unwrap();
    let repo = UserRepo::new(cnn);
    repo.init().unwrap();
    repo
}

#[test]
fn test_get_empty() {
    let repo = make_repo();
    assert_eq!(repo.get_user_info("abc", 1), Err(DbError::NotFound(None)),)
}

#[test]
fn test_get_user_info() {
    let repo = make_repo();
    let info = UserInfo {
        first_name: "a".to_string(),
        last_name: "b".to_string(),
        username: "c".to_string(),
        tg_id: 12,
        is_robot: false,
    };
    repo.update_user(&info, 3).unwrap();
    for key in ["a", "b", "c", "a b", "b a"] {
        assert_eq!(repo.get_user_info(key, 3), Ok(info.clone()));
        assert_eq!(repo.get_user_info(key, 4), Err(DbError::NotFound(None)));
    }
    for key in ["x", "y", "a c"] {
        assert_eq!(repo.get_user_info(key, 3), Err(DbError::NotFound(None)));
    }
}

#[test]
fn test_get_user_info_two_chats() {
    let repo = make_repo();
    let info = UserInfo {
        first_name: "a".to_string(),
        last_name: "b".to_string(),
        username: "c".to_string(),
        tg_id: 12,
        is_robot: false,
    };
    repo.update_user(&info, 3).unwrap();
    repo.update_user(&info, 4).unwrap();
    for key in ["a", "b", "c", "a b", "b a"] {
        assert_eq!(repo.get_user_info(key, 3), Ok(info.clone()));
        assert_eq!(repo.get_user_info(key, 4), Ok(info.clone()));
    }
    for key in ["x", "y", "a c"] {
        assert_eq!(repo.get_user_info(key, 3), Err(DbError::NotFound(None)));
    }
}

#[test]
fn test_get_user_info_list() {
    let repo = make_repo();
    let u_a = UserInfo {
        first_name: "a".to_string(),
        last_name: "b".to_string(),
        username: "c".to_string(),
        tg_id: 12,
        is_robot: false,
    };
    let u_b = UserInfo {
        first_name: "a".to_string(),
        last_name: "b".to_string(),
        username: "d".to_string(),
        tg_id: 14,
        is_robot: false,
    };
    let u_c = UserInfo {
        first_name: "a".to_string(),
        last_name: "x".to_string(),
        username: "y".to_string(),
        tg_id: 16,
        is_robot: false,
    };
    for (u, c) in [(&u_a, 1), (&u_b, 1), (&u_c, 1), (&u_b, 2), (&u_c, 2)] {
        repo.update_user(u, c).unwrap();
    }

    let found = repo.get_user_info_list(&["x", "a b"], 2).unwrap();
    let expected = [
        ("x".to_string(), u_c.clone()),
        ("a b".to_string(), u_b.clone()),
    ]
    .into_iter()
    .collect::<HashMap<_, _>>();
    assert_eq!(found, expected);

    let err = repo.get_user_info_list(&["x", "a b", "c"], 2).unwrap_err();
    assert_eq!(err, DbError::NotFound(Some("c".to_string())));
}

#[test]
fn test_get_user_info_list_by_id() {
    let repo = make_repo();
    let u_a = UserInfo {
        first_name: "a".to_string(),
        last_name: "b".to_string(),
        username: "c".to_string(),
        tg_id: 12,
        is_robot: false,
    };
    let u_b = UserInfo {
        first_name: "a".to_string(),
        last_name: "b".to_string(),
        username: "d".to_string(),
        tg_id: 14,
        is_robot: false,
    };
    let u_c = UserInfo {
        first_name: "a".to_string(),
        last_name: "x".to_string(),
        username: "y".to_string(),
        tg_id: 16,
        is_robot: false,
    };
    for (u, c) in [(&u_a, 1), (&u_b, 1), (&u_c, 1)] {
        repo.update_user(u, c).unwrap();
    }

    let found = repo.get_user_info_list_by_id(&[12, 14]).unwrap();
    assert_eq!(found, &[u_a, u_b]);

    let err = repo.get_user_info_list_by_id(&[12, 10]).unwrap_err();
    assert_eq!(err, DbError::NotFound(Some("10".to_string())));
}
