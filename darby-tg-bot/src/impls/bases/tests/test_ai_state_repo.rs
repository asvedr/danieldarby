use crate::impls::bases::ai_state_repo::AiStateRepo;
use crate::proto::db::{IAiStateRepo, IDbRepo};
use poker::AiState;

fn make_repo() -> AiStateRepo {
    let repo = AiStateRepo::new().unwrap();
    repo.init().unwrap();
    repo
}

#[test]
fn test_get_default() {
    let repo = make_repo();
    assert_eq!(repo.get_state(1).unwrap(), AiState::default())
}

#[test]
fn test_get_set_default() {
    let repo = make_repo();
    repo.save_state(1, AiState::default()).unwrap();
    assert_eq!(repo.get_state(1).unwrap(), AiState::default())
}

#[test]
fn test_save_get_del() {
    let repo = make_repo();
    let mut state = AiState::default();
    state.init_required = false;
    repo.save_state(1, state).unwrap();
    assert!(!repo.get_state(1).unwrap().init_required);
    repo.del_state(1).unwrap();
    assert!(repo.get_state(1).unwrap().init_required);
}
