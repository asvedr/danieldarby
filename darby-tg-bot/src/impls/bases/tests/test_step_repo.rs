use crate::entities::errors::DbError;
use crate::entities::player::{NewPlayerStep, PlayerStep, RobotName};
use crate::impls::bases::sqlite::connection::SQLConnection;
use crate::impls::bases::step_repo::StepRepo;
use crate::proto::db::{IDbRepo, IStepRepo};
use poker::Step;

fn make_repo() -> StepRepo {
    let cnn = SQLConnection::new(":memory:").unwrap();
    let repo = StepRepo::new(cnn);
    repo.init().unwrap();
    repo
}

#[test]
fn test_empty() {
    let repo = make_repo();
    assert_eq!(repo.get_step(), Err(DbError::NotFound(None)))
}

#[test]
fn test_wf() {
    let repo = make_repo();
    repo.push_step(RobotName::Daniel, -20, Step::Raise(10))
        .unwrap();
    let expected = NewPlayerStep {
        player: RobotName::Daniel,
        chat_id: -20,
        step: Step::Raise(10),
    };
    let got = repo.get_step().unwrap();
    assert_eq!(got.new, expected);
    repo.del_step(got.id).unwrap();
    assert_eq!(repo.get_step(), Err(DbError::NotFound(None)))
}
