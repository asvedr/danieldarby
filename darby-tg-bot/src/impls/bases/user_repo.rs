use crate::entities::db::UserInfo;
use crate::entities::errors::{DbError, DbResult};
use crate::impls::bases::sqlite::connection::SQLConnection;
use crate::impls::bases::sqlite::table_manager::TableManager;
use crate::impls::bases::sqlite::table_schema::{SStr, SVec, TableSchema};
use crate::impls::bases::sqlite::utils::{deescape, escape};
use crate::proto::db::{IDbRepo, INewDbRepo, IUserRepo};
use crate::utils::algo::find_index;
use std::collections::HashMap;
use std::str::FromStr;

const LIMIT: usize = 100;

struct TUser {}
struct TUserChat {}

#[derive(Clone)]
pub struct UserRepo {
    user_manager: TableManager<TUser>,
    user_chat_manager: TableManager<TUserChat>,
    user_fields: String,
}

impl TableSchema for TUser {
    const PK: SStr = "tg_id";
    const NAME: SStr = "user";
    const COLUMNS: SVec<(SStr, SStr)> = &[
        ("tg_id", "INTEGER PRIMARY KEY"),
        ("first_name", "STRING"),
        ("last_name", "STRING"),
        ("username", "STRING"),
        ("full_name", "STRING"),
        ("rev_full_name", "STRING"),
        ("is_robot", "BOOLEAN"),
    ];
    const INDEXES: SVec<SStr> = &["tg_id", "username", "first_name", "last_name"];
}

impl TableSchema for TUserChat {
    const NAME: SStr = "user_chat";
    const COLUMNS: SVec<(SStr, SStr)> = &[
        ("id", "INTEGER PRIMARY KEY"),
        ("user_id", "INTEGER"),
        ("chat_id", "INTEGER"),
    ];
    const INDEXES: SVec<SStr> = &["user_id"];
    const UNIQUE: SVec<SStr> = &["user_id, chat_id"];
    const FOREIGN_KEYS: SVec<(SStr, SStr, SStr)> = &[("user_id", "user", "tg_id")];
}

impl UserRepo {
    pub fn new(cnn: SQLConnection) -> Self {
        Self {
            user_manager: TableManager::create(cnn.clone()),
            user_chat_manager: TableManager::create(cnn),
            user_fields: TUser::get_columns_as_str(),
        }
    }

    fn find_users_query(&self, keys: &[&str], chat_id: i64, limit: usize) -> String {
        let merged_keys = keys
            .iter()
            .map(|s| escape(s))
            .collect::<Vec<String>>()
            .join(",");
        let int_keys = keys
            .iter()
            .filter(|key| i64::from_str(key).is_ok())
            .map(|key| key.to_string())
            .collect::<Vec<_>>()
            .join(",");
        format!(
            r#"
                SELECT {fields} FROM user JOIN user_chat
                ON user.tg_id = user_chat.user_id
                WHERE
                    user_chat.chat_id = {chat_id}
                    AND (
                        user.username IN ({keys})
                        OR user.first_name IN ({keys})
                        OR user.last_name IN ({keys})
                        OR user.full_name IN ({keys})
                        OR user.rev_full_name IN ({keys})
                        OR user.tg_id IN ({int_keys})
                    )
                LIMIT {limit}
            "#,
            fields = self.user_fields,
            chat_id = chat_id,
            keys = merged_keys,
            int_keys = int_keys,
            limit = limit,
        )
    }

    fn deserialize_user(row: &rusqlite::Row) -> rusqlite::Result<UserInfo> {
        Ok(UserInfo {
            tg_id: row.get(0)?,
            first_name: deescape(row.get(1)?),
            last_name: deescape(row.get(2)?),
            username: deescape(row.get(3)?),
            is_robot: row.get(6)?,
        })
    }

    fn find_best_match(key: &str, users: &[UserInfo]) -> Option<usize> {
        find_index(
            users,
            &[
                &|u| u.tg_id.to_string() == key,
                &|u| u.username == key,
                &|u| u.first_name == key,
                &|u| u.last_name == key,
                &|u| u.full_name() == key,
                &|u| u.rev_full_name() == key,
            ],
        )
    }

    fn filter_users<'a>(
        mut users: Vec<UserInfo>,
        keys: &[&'a str],
    ) -> Result<Vec<(&'a str, UserInfo)>, &'a str> {
        let mut result = Vec::new();
        for key in keys {
            let index = match Self::find_best_match(key, &users) {
                Some(val) => val,
                _ => return Err(*key),
            };
            let user = users.swap_remove(index);
            result.push((*key, user));
        }
        Ok(result)
    }
}

impl IDbRepo for UserRepo {
    fn init(&self) -> DbResult<()> {
        self.user_manager.init()?;
        self.user_chat_manager.init()
    }
    fn recreate_table(&self) -> DbResult<()> {
        self.user_chat_manager.recreate_table()?;
        self.user_manager.recreate_table()
    }
    fn set_indexes(&self) -> DbResult<()> {
        self.user_manager.set_indexes()?;
        self.user_chat_manager.set_indexes()
    }
    fn drop_indexes(&self) -> DbResult<()> {
        self.user_manager.drop_indexes()?;
        self.user_chat_manager.drop_indexes()
    }
    fn get_size(&self) -> DbResult<usize> {
        unimplemented!()
    }
}

impl IUserRepo for UserRepo {
    fn get_user_info(&self, key: &str, chat_id: i64) -> Result<UserInfo, DbError> {
        let query = self.find_users_query(&[key], chat_id, LIMIT);
        let users = self.user_manager.get_many(&query, Self::deserialize_user)?;
        let mut result = Self::filter_users(users, &[key]).map_err(|_| DbError::NotFound(None))?;
        let (_, user) = result.swap_remove(0);
        Ok(user)
    }

    fn get_user_info_list(
        &self,
        keys: &[&str],
        chat_id: i64,
    ) -> Result<HashMap<String, UserInfo>, DbError> {
        let query = self.find_users_query(keys, chat_id, LIMIT);
        let users = self.user_manager.get_many(&query, Self::deserialize_user)?;
        let result = Self::filter_users(users, keys)
            .map_err(|key| DbError::NotFound(Some(key.to_string())))?;
        let map = result
            .into_iter()
            .map(|(k, v)| (k.to_string(), v))
            .collect();
        Ok(map)
    }

    fn get_user_info_list_by_id(&self, id_list: &[i64]) -> Result<Vec<UserInfo>, DbError> {
        let id_list_s = id_list
            .iter()
            .map(|s| s.to_string())
            .collect::<Vec<_>>()
            .join(",");
        let query = format!(
            "SELECT {fields} FROM user WHERE tg_id IN ({id_list}) ORDER BY tg_id",
            fields = self.user_fields,
            id_list = id_list_s,
        );
        let users = self.user_manager.get_many(&query, Self::deserialize_user)?;
        if users.len() != id_list.len() {
            for id in id_list {
                if find_index(&users, &[&|u| u.tg_id == *id]).is_none() {
                    return Err(DbError::NotFound(Some(id.to_string())));
                }
            }
            unreachable!()
        }
        Ok(users)
    }

    fn update_user(&self, user: &UserInfo, chat_id: i64) -> Result<(), DbError> {
        let query = format!(
            r#"
                INSERT OR REPLACE INTO {table}({fields})
                VALUES (
                    {tg_id}, {first_name}, {last_name}, {username},
                    {full_name}, {rev_full_name}, {is_robot}
                )
            "#,
            table = TUser::NAME,
            fields = self.user_fields,
            tg_id = user.tg_id,
            first_name = escape(&user.first_name),
            last_name = escape(&user.last_name),
            username = escape(&user.username),
            full_name = escape(&user.full_name()),
            rev_full_name = escape(&user.rev_full_name()),
            is_robot = user.is_robot,
        );
        self.user_manager.execute(&query)?;
        let query = format!(
            r#"
                INSERT OR IGNORE INTO {table}(`user_id`, `chat_id`)
                VALUES ({user_id}, {chat_id})
            "#,
            table = TUserChat::NAME,
            user_id = user.tg_id,
            chat_id = chat_id,
        );
        self.user_chat_manager.execute(&query)
    }
}
