use std::str::FromStr;
use std::sync::Arc;

use poker::{
    create_game_deserializer, create_game_serializer, IGameDeserializer, IGameSerializer, Step,
};

use crate::entities::errors::{DbError, DbResult};
use crate::entities::player::{NewPlayerStep, PlayerStep, RobotName};
use crate::impls::bases::sqlite::connection::SQLConnection;
use crate::impls::bases::sqlite::table_manager::TableManager;
use crate::impls::bases::sqlite::table_schema::{SStr, SVec, TableSchema};
use crate::impls::bases::sqlite::utils::{deescape, escape};
use crate::proto::db::{IDbRepo, INewDbRepo, IStepRepo};

struct Table {}

struct Inner {
    table: TableManager<Table>,
    de: Box<dyn IGameDeserializer>,
    ser: Box<dyn IGameSerializer>,
}

#[derive(Clone)]
pub struct StepRepo {
    inner: Arc<Inner>,
    fields: String,
}

impl TableSchema for Table {
    const PK: SStr = "id";
    const NAME: SStr = "dealer_step";
    const COLUMNS: SVec<(SStr, SStr)> = &[
        ("id", "INTEGER PRIMARY KEY"),
        ("chat_id", "INTEGER"),
        ("robot", "STRING"),
        ("value", "STRING"),
    ];
}

impl IDbRepo for StepRepo {
    fn as_super(&self) -> &dyn IDbRepo {
        &self.inner.table
    }
}

impl StepRepo {
    pub fn new(cnn: SQLConnection) -> Self {
        let inner = Inner {
            table: TableManager::create(cnn),
            de: create_game_deserializer(),
            ser: create_game_serializer(true),
        };
        Self {
            inner: Arc::new(inner),
            fields: Table::get_columns_as_str(),
        }
    }
}

impl IStepRepo for StepRepo {
    fn get_step(&self) -> DbResult<PlayerStep> {
        let query = format!(
            "SELECT {fields} FROM {table} ORDER BY {pk} LIMIT 1",
            fields = self.fields,
            table = Table::NAME,
            pk = Table::PK,
        );
        let (id, robot, chat_id, value) = self.inner.table.get_one(
            &query,
            |row| -> Result<(usize, String, isize, String), _> {
                Ok((
                    row.get("id")?,
                    row.get("robot")?,
                    row.get("chat_id")?,
                    deescape(row.get("value")?),
                ))
            },
        )?;
        let step = self
            .inner
            .de
            .str_to_step(&value)
            .map_err(|_| DbError::InvalidValue("step"))?;
        let result = PlayerStep {
            id,
            new: NewPlayerStep {
                player: RobotName::from_str(&robot)
                    .map_err(|_| DbError::InvalidValue("robot_name"))?,
                chat_id: chat_id as i64,
                step,
            },
        };
        Ok(result)
    }

    fn del_step(&self, id: usize) -> Result<(), DbError> {
        let query = format!(
            "DELETE FROM {table} WHERE {field} = {pk}",
            table = Table::NAME,
            field = Table::PK,
            pk = id,
        );
        self.inner.table.execute(&query)
    }

    fn push_step(&self, player: RobotName, chat_id: i64, step: Step) -> Result<(), DbError> {
        let query = format!(
            "INSERT INTO {table} (`chat_id`,`robot`,`value`) VALUES ({chat_id}, '{robot}', {value})",
            table=Table::NAME,
            chat_id=chat_id,
            robot=player.to_string(),
            value=escape(&self.inner.ser.step_to_str(&step)),
        );
        self.inner.table.execute(&query)
    }
}
