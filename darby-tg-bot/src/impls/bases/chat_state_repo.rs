use poker::{
    create_game_deserializer, create_game_serializer, GameState, IGameDeserializer, IGameSerializer,
};
use std::sync::Arc;
use std::time::SystemTime;

use crate::entities::errors::DbError;
use crate::impls::bases::sqlite::connection::SQLConnection;
use crate::impls::bases::sqlite::table_manager::TableManager;
use crate::impls::bases::sqlite::table_schema::{SStr, SVec, TableSchema};
use crate::impls::bases::sqlite::utils::{deescape, escape};
use crate::proto::db::{IChatStateRepo, IDbRepo, INewDbRepo};

struct TState {}

struct Inner {
    table: TableManager<TState>,
    ser: Box<dyn IGameSerializer>,
    de: Box<dyn IGameDeserializer>,
    fields: String,
}

#[derive(Clone)]
pub struct ChatStateRepo {
    inner: Arc<Inner>,
}

unsafe impl Sync for ChatStateRepo {}
unsafe impl Send for ChatStateRepo {}

impl TableSchema for TState {
    const PK: SStr = "chat_id";
    const NAME: SStr = "chat_state";
    const COLUMNS: SVec<(SStr, SStr)> = &[
        ("chat_id", "INTEGER PRIMARY KEY"),
        ("state", "STRING"),
        ("ai_step_expected", "BOOLEAN"),
        ("auto_step_expected", "BOOLEAN"),
        ("updated_at", "INTEGER"), // timestamp in seconds
    ];
}

impl IDbRepo for ChatStateRepo {
    fn as_super(&self) -> &dyn IDbRepo {
        &self.inner.table
    }
}

impl ChatStateRepo {
    pub fn new(cnn: SQLConnection) -> Self {
        let inner = Inner {
            table: TableManager::create(cnn),
            ser: create_game_serializer(true),
            de: create_game_deserializer(),
            fields: TState::get_columns_as_str(),
        };
        Self {
            inner: Arc::new(inner),
        }
    }

    fn deserialize(&self, text: String) -> Result<GameState, DbError> {
        match self.inner.de.str_to_state(&deescape(text)) {
            Ok(val) => Ok(val),
            Err(err) => Err(DbError::DeserializationError(format!("{:?}", err))),
        }
    }

    fn now() -> u64 {
        SystemTime::now()
            .duration_since(SystemTime::UNIX_EPOCH)
            .unwrap()
            .as_secs()
    }
}

impl IChatStateRepo for ChatStateRepo {
    fn get_chat_state(&self, chat_id: i64) -> Result<GameState, DbError> {
        let query = format!(
            "SELECT state FROM {table} WHERE chat_id = {chat_id}",
            table = TState::NAME,
            chat_id = chat_id
        );
        let text = self.inner.table.get_one(&query, |row| row.get(0))?;
        self.deserialize(text)
    }

    fn save_chat_state(
        &self,
        chat_id: i64,
        ai_step_expected: bool,
        auto_step_expected: bool,
        state: GameState,
    ) -> Result<(), DbError> {
        let query = format!(
            r#"
                INSERT OR REPLACE INTO {table} ({fields})
                VALUES ({chat_id}, {state}, {ai_ex}, {as_ex}, {upd})
            "#,
            table = TState::NAME,
            fields = self.inner.fields,
            chat_id = chat_id,
            state = escape(&self.inner.ser.state_to_str(&state)),
            ai_ex = ai_step_expected,
            as_ex = auto_step_expected,
            upd = Self::now(),
        );
        self.inner.table.execute(&query)
    }

    fn del_chat_states(&self, chat_id_list: &[i64]) -> Result<(), DbError> {
        let list = chat_id_list
            .iter()
            .map(|id| id.to_string())
            .collect::<Vec<_>>()
            .join(",");
        let query = format!(
            "DELETE FROM {table} WHERE {pk} IN ({list})",
            table = TState::NAME,
            pk = TState::PK,
            list = list
        );
        self.inner.table.execute(&query)
    }

    fn get_state_to_auto_step(&self) -> Result<(i64, GameState), DbError> {
        let query = format!(
            "SELECT chat_id, state FROM {table} WHERE auto_step_expected LIMIT 1",
            table = TState::NAME
        );
        let (id, text) = self
            .inner
            .table
            .get_one(&query, |row| Ok((row.get(0)?, row.get(1)?)))?;
        Ok((id, self.deserialize(text)?))
    }

    fn get_states_to_ai_step(&self) -> Result<Vec<(i64, GameState)>, DbError> {
        let query = format!(
            "SELECT chat_id, state FROM {table} WHERE ai_step_expected",
            table = TState::NAME
        );
        let rows: Vec<(i64, String)> = self
            .inner
            .table
            .get_many(&query, |row| Ok((row.get(0)?, row.get(1)?)))?;
        let mut result = Vec::new();
        for (id, text) in rows {
            let state = self.deserialize(text)?;
            result.push((id, state));
        }
        Ok(result)
    }

    fn get_too_old_states(&self, max_time: usize, limit: usize) -> Result<Vec<i64>, DbError> {
        let query = format!(
            "SELECT chat_id FROM {table} WHERE updated_at <= {threshold} LIMIT {limit}",
            table = TState::NAME,
            threshold = Self::now() - (max_time as u64),
            limit = limit
        );
        self.inner.table.get_many(&query, |row| row.get(0))
    }
}
