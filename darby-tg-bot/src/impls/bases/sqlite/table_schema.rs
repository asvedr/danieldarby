pub type SStr = &'static str;
pub type SVec<T> = &'static [T];

pub trait TableSchema {
    // const for FK
    const PK: SStr = "";
    // table name
    const NAME: SStr;
    // [(column name, column type)]
    const COLUMNS: SVec<(SStr, SStr)>;
    // [column name]
    const INDEXES: SVec<SStr> = &[];
    // [column name]
    const UNIQUE: SVec<SStr> = &[];
    // [(self column, ext table, ext column)]
    const FOREIGN_KEYS: SVec<(SStr, SStr, SStr)> = &[];

    fn get_columns_as_str() -> String {
        Self::COLUMNS
            .iter()
            .map(|(c, _)| format!("`{}`", c))
            .collect::<Vec<_>>()
            .join(",")
    }
}
