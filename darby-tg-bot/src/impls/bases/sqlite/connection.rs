use std::path::Path;
use std::sync::{Arc, Mutex, MutexGuard};

use rusqlite;

use crate::entities::errors::{DbError, DbResult};

#[derive(Clone)]
pub struct SQLConnection {
    inner: Arc<Mutex<rusqlite::Connection>>,
}

impl SQLConnection {
    pub fn new<P: AsRef<Path>>(path: P) -> DbResult<Self> {
        let cnct = rusqlite::Connection::open(path)
            .map_err(|err| DbError::CanNotConnect(err.to_string()))?;
        Ok(Self {
            inner: Arc::new(Mutex::new(cnct)),
        })
    }

    #[inline(always)]
    pub fn get(&self) -> Result<MutexGuard<rusqlite::Connection>, DbError> {
        Ok(self.inner.lock()?)
    }
}
