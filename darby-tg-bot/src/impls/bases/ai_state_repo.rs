use std::str::FromStr;

use poker::AiState;

use crate::entities::errors::DbError;
use crate::impls::bases::sqlite::connection::SQLConnection;
use crate::impls::bases::sqlite::table_manager::TableManager;
use crate::impls::bases::sqlite::table_schema::{SStr, SVec, TableSchema};
use crate::impls::bases::sqlite::utils::escape;
use crate::proto::db::{IAiStateRepo, IDbRepo, INewDbRepo};

const SUFFIX: &str = "__APPEND";

struct TState {}

pub struct AiStateRepo {
    table: TableManager<TState>,
}

impl TableSchema for TState {
    const PK: SStr = "chat_id";
    const NAME: SStr = "ai_state";
    const COLUMNS: SVec<(SStr, SStr)> = &[("chat_id", "INTEGER PRIMARY KEY"), ("state", "STRING")];
}

impl AiStateRepo {
    pub fn new() -> Result<Self, DbError> {
        let cnn = SQLConnection::new(":memory:")?;
        let table = TableManager::create(cnn);
        table.init()?;
        Ok(Self { table })
    }
}

impl IDbRepo for AiStateRepo {
    fn as_super(&self) -> &dyn IDbRepo {
        &self.table
    }
}

impl IAiStateRepo for AiStateRepo {
    fn get_state(&self, chat_id: i64) -> Result<AiState, DbError> {
        let query = format!(
            "SELECT state FROM {table} WHERE chat_id = {chat_id}",
            table = TState::NAME,
            chat_id = chat_id,
        );
        let text: String = match self.table.get_one(&query, |row| row.get(0)) {
            Ok(val) => val,
            Err(DbError::NotFound(_)) => return Ok(AiState::default()),
            Err(err) => return Err(err),
        };
        AiState::from_str(&text.replace(SUFFIX, ""))
            .map_err(|_| DbError::DeserializationError(format!("invalid state: {:?}", text)))
    }

    fn save_state(&self, chat_id: i64, state: AiState) -> Result<(), DbError> {
        let mut state = state.to_string();
        state.push_str(SUFFIX);
        let query = format!(
            "INSERT OR REPLACE INTO {table} (chat_id, state) VALUES ({chat_id}, {state})",
            table = TState::NAME,
            chat_id = chat_id,
            state = escape(&state),
        );
        self.table.execute(&query)
    }

    fn del_state(&self, chat_id: i64) -> Result<(), DbError> {
        let query = format!(
            "DELETE FROM {table} WHERE chat_id = {chat_id}",
            table = TState::NAME,
            chat_id = chat_id,
        );
        self.table.execute(&query)
    }
}
