use std::cell::RefCell;
use std::thread;
use std::time::{Duration, SystemTime};
use telegram_bot_ars::{Api, ChatId, SendMessage, UserId};

use crate::entities::errors::RobotError;
use crate::entities::main_config::MainConfig;
use crate::proto::robot::ISyncTgSender;

pub struct SyncTgSender {
    api: Option<Api>,
    token: String,
    timeout: Duration,
    tries: usize,
    last_event: RefCell<SystemTime>,
}

async fn async_send(api: &Api, receiver: i64, msgs: &[&str]) -> Result<(), RobotError> {
    let messages = if receiver < 0 {
        msgs.iter()
            .map(|msg| SendMessage::new(ChatId::from(receiver), msg.to_string()))
            .collect::<Vec<_>>()
    } else {
        msgs.iter()
            .map(|msg| SendMessage::new(UserId::from(receiver), msg.to_string()))
            .collect::<Vec<_>>()
    };
    for message in messages {
        api.send_timeout(message, Duration::from_secs_f64(0.5))
            .await?;
    }
    Ok(())
}

fn sync_send(api: &Api, receiver: i64, msgs: &[&str], tries: usize) -> Result<(), RobotError> {
    let rt = tokio::runtime::Builder::new_current_thread()
        .enable_all()
        .build()
        .unwrap();
    let mut result = Ok(());
    for _ in 0..tries {
        result = rt.block_on(async_send(api, receiver, msgs));
        if let Err(ref err) = result {
            eprintln!("Tg sync send: {:?}", err);
        } else {
            break;
        }
    }
    result
}

impl SyncTgSender {
    fn sleep_if_required(&self) {
        let time = self.last_event.borrow();
        let now = SystemTime::now();
        let duration = now.duration_since(*time).unwrap();
        if duration >= self.timeout {
            return;
        }
        thread::sleep(duration)
    }

    fn save_last_send_time(&self) {
        let mut storage = self.last_event.borrow_mut();
        *storage = SystemTime::now();
    }
}

impl ISyncTgSender for SyncTgSender {
    fn build(token: String, config: &MainConfig) -> Self {
        let api = if config.rebuild_tg_sender {
            None
        } else {
            Some(Api::new(&token))
        };
        Self {
            api,
            token,
            timeout: config.min_tg_timeout,
            tries: config.tg_tries,
            last_event: RefCell::new(SystemTime::UNIX_EPOCH),
        }
    }

    fn send_message(&self, receiver: i64, msgs: &[&str]) -> Result<(), RobotError> {
        self.sleep_if_required();
        match self.api {
            Some(ref api) => sync_send(api, receiver, msgs, self.tries),
            None => {
                let api = Api::new(&self.token);
                sync_send(&api, receiver, msgs, self.tries)
            }
        }?;
        self.save_last_send_time();
        Ok(())
    }
}
