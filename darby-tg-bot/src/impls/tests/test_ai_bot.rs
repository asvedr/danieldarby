use std::cell::RefCell;
use std::rc::Rc;

use poker::{create_ai_deserializer, Step, UserGameInfo};

use crate::entities::ai_config::{Reactions, ReactionsConfig};
use crate::entities::errors::RobotError;
use crate::entities::main_config::MainConfig;
use crate::entities::player::{NewPlayerStep, ReactionRequest, RobotName};
use crate::impls::ai_robot::{AiRobot, AiRobotRequest};
use crate::impls::bases::ai_state_repo::AiStateRepo;
use crate::impls::queue::Queue;
use crate::proto::queue::IQueue;
use crate::proto::robot::ISyncTgSender;

struct MockTgSender {
    result: Rc<RefCell<Vec<(i64, String)>>>,
}

struct TestBotKit {
    robot: AiRobot<Queue<ReactionRequest>, Queue<NewPlayerStep>, AiStateRepo, MockTgSender>,
    requests: Queue<ReactionRequest>,
    steps: Queue<NewPlayerStep>,
    messages: Rc<RefCell<Vec<(i64, String)>>>,
}

impl ISyncTgSender for MockTgSender {
    fn build(_: String, _: &MainConfig) -> Self {
        Self {
            result: Default::default(),
        }
    }

    fn send_message(&self, receiver: i64, msgs: &[&str]) -> Result<(), RobotError> {
        let mut result = self.result.borrow_mut();
        for msg in msgs {
            result.push((receiver, msg.to_string()));
        }
        Ok(())
    }
}

fn setup() -> TestBotKit {
    let requests = Queue::new("requests", None);
    let steps = Queue::new("steps", None);
    let des = create_ai_deserializer();
    let robot = AiRobotRequest {
        name: RobotName::Terence,
        token: "".to_string(),
        sleep: Default::default(),
        bus_from_dealer: requests.clone(),
        bus_to_dealer: steps.clone(),
        ai: des
            .json_to_ai(
                r#"{
            "config": {
                "tries": 100,
                "bluff_va_banque": 0,
                "bluff": 0,
                "gain": 0
            },
            "strategies": {"safe": [{"str": "chca"}]}
        }"#,
            )
            .unwrap(),
        state_repo: AiStateRepo::new().unwrap(),
        reactions_config: ReactionsConfig {
            win_round: Reactions {
                prob: 1.0,
                phrases: vec!["yeah!".to_string()],
            },
            win_game: Reactions {
                prob: 1.0,
                phrases: vec!["YES!".to_string()],
            },
            loose_game: Reactions {
                prob: 1.0,
                phrases: vec!["%name% are pidor!".to_string()],
            },
            loose_round: Reactions {
                prob: 1.0,
                phrases: vec!["%name% AAAA!".to_string()],
            },
            new_game: Reactions {
                prob: 1.0,
                phrases: vec!["i'm in".to_string()],
            },
            raise: Reactions {
                prob: 1.0,
                phrases: vec!["rse".to_string()],
            },
        },
    }
    .build::<MockTgSender>(&MainConfig {
        token: "".to_string(),
        dealer_db: "".to_string(),
        balance: 0,
        inactive_state_lifetime: 0,
        available_ai: vec![],
        ai_conf_dir: "".to_string(),
        sleep: Default::default(),
        min_tg_timeout: Default::default(),
        tg_tries: 0,
        rebuild_tg_sender: false,
    });
    let messages = robot.sender.result.clone();
    TestBotKit {
        robot,
        requests,
        steps,
        messages,
    }
}

#[test]
fn test_nothing() {
    setup().robot.find_and_process_request().unwrap();
}

#[test]
fn test_new_game() {
    let robot_kit = setup();
    robot_kit
        .requests
        .push(ReactionRequest::NewGame { chat_id: 1 })
        .unwrap();
    robot_kit.robot.find_and_process_request().unwrap();
    assert_eq!(robot_kit.steps.pop().unwrap(), None);
    let msg = robot_kit.messages.borrow_mut().pop();
    assert_eq!(msg, Some((1, "i'm in".to_string())));
}

#[test]
fn test_make_step() {
    let robot_kit = setup();
    let request = ReactionRequest::StepRequest {
        chat_id: 1,
        user_info: UserGameInfo::default(),
        steps: vec![Step::Fold],
    };
    robot_kit.requests.push(request).unwrap();
    robot_kit.robot.find_and_process_request().unwrap();
    let expected = NewPlayerStep {
        player: RobotName::Terence,
        chat_id: 1,
        step: Step::Fold,
    };
    assert_eq!(robot_kit.steps.pop().unwrap(), Some(expected));
    let msg = robot_kit.messages.borrow_mut().pop();
    assert_eq!(msg, Some((1, "/fold".to_string())));
}

#[test]
fn test_win_game() {
    let robot_kit = setup();
    let request = ReactionRequest::WinGame { chat_id: 1 };
    robot_kit.requests.push(request).unwrap();
    robot_kit.robot.find_and_process_request().unwrap();
    assert_eq!(robot_kit.steps.pop().unwrap(), None);
    let msg = robot_kit.messages.borrow_mut().pop();
    assert_eq!(msg, Some((1, "YES!".to_string())));
}

#[test]
fn test_win_round() {
    let robot_kit = setup();
    let request = ReactionRequest::WinRound { chat_id: 1 };
    robot_kit.requests.push(request).unwrap();
    robot_kit.robot.find_and_process_request().unwrap();
    assert_eq!(robot_kit.steps.pop().unwrap(), None);
    let msg = robot_kit.messages.borrow_mut().pop();
    assert_eq!(msg, Some((1, "yeah!".to_string())));
}

#[test]
fn test_loose() {
    let robot_kit = setup();
    let request = ReactionRequest::LooseGame {
        chat_id: 1,
        winner: "Alex".to_string(),
    };
    robot_kit.requests.push(request).unwrap();
    robot_kit.robot.find_and_process_request().unwrap();
    assert_eq!(robot_kit.steps.pop().unwrap(), None);
    let msg = robot_kit.messages.borrow_mut().pop();
    assert_eq!(msg, Some((1, "Alex are pidor!".to_string())));
}
