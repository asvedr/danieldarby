use poker::{RoundStateKind, Step};

use crate::entities::db::UserInfo;
use crate::entities::errors::RobotError;
use crate::entities::main_config::MainConfig;
use crate::entities::player::{NewPlayerStep, PlayerCommand, ReactionRequest, RobotName};
use crate::entities::robot::Notification;
use crate::impls::bases::chat_state_repo::ChatStateRepo;
use crate::impls::bases::dealer_base::DealerBase;
use crate::impls::bases::sqlite::connection::SQLConnection;
use crate::impls::bases::user_repo::UserRepo;
use crate::impls::dealer_robot::DealerRobot;
use crate::impls::dealer_to_player_bus::DealerToPlayerBus;
use crate::impls::queue::Queue;
use crate::proto::db::IChatStateRepo;
use crate::proto::queue::IQueue;
use crate::proto::robot::{ICommandParser, IDealerBase, IDealerToPlayerBus};

struct MockCommandParser {
    // commands: RefCell<Vec<Option<PlayerCommand>>>,
}

impl ICommandParser for MockCommandParser {
    fn parse_command(&self, _: &str) -> Option<PlayerCommand> {
        unimplemented!()
        // self.commands.borrow_mut().remove(0)
    }

    fn help(&self) -> String {
        "bep".to_string()
    }
}

type TDealerBase = DealerBase<UserRepo, ChatStateRepo>;

struct TestBotKit {
    main_user: UserInfo,
    base: TDealerBase,
    queues: Vec<(RobotName, Queue<ReactionRequest>)>,
    robot: DealerRobot<TDealerBase, MockCommandParser, Queue<NewPlayerStep>>,
}

fn setup_empty() -> TestBotKit {
    let cnn = SQLConnection::new(":memory:").unwrap();
    let base = DealerBase {
        user_repo: UserRepo::new(cnn.clone()),
        // step_repo: StepRepo::new(cnn.clone()),
        chat_state_repo: ChatStateRepo::new(cnn),
    };
    base.init().unwrap();
    let parser = MockCommandParser {};
    let queues = vec![
        (RobotName::Daniel, Queue::new("a", None)),
        (RobotName::Terence, Queue::new("b", None)),
    ];
    let buses = queues
        .iter()
        .cloned()
        .map(|(robot, queue)| DealerToPlayerBus { robot, queue })
        .map(|pb| -> Box<dyn IDealerToPlayerBus> { Box::new(pb) })
        .collect::<Vec<_>>();
    let config = MainConfig {
        token: "".to_string(),
        dealer_db: "".to_string(),
        balance: 100,
        inactive_state_lifetime: 60 * 60 * 60,
        available_ai: vec![],
        ai_conf_dir: "".to_string(),
        sleep: Default::default(),
        min_tg_timeout: Default::default(),
        tg_tries: 0,
        rebuild_tg_sender: false,
    };
    let robot = DealerRobot::new(
        base.clone(),
        parser,
        Queue::new("steps", None),
        config,
        buses,
    );
    let user = UserInfo::make("a", "b", "c", 1);
    base.update_user(&user, -1).unwrap();
    TestBotKit {
        main_user: user,
        base,
        queues,
        robot,
    }
}

fn setup_new_game() -> TestBotKit {
    let bot_kit = setup_empty();
    bot_kit
        .base
        .update_user(&UserInfo::make("d", "e", "f", 2), -1)
        .unwrap();
    bot_kit
        .robot
        .execute_command(
            bot_kit.main_user.clone(),
            -1,
            PlayerCommand::NewGame(vec!["d".to_string(), RobotName::Daniel.to_string()]),
        )
        .unwrap();
    bot_kit
}

fn setup_ready_to_user_step() -> TestBotKit {
    let mut bot_kit = setup_new_game();
    let mut state = bot_kit.base.get_state(-1).unwrap();
    loop {
        match bot_kit.robot.auto_step(&state) {
            Ok(new_state) => state = new_state,
            Err(_) => break,
        }
    }
    bot_kit.base.save_state(-1, false, false, state).unwrap();
    bot_kit
}

#[test]
fn test_new_game_bad_user() {
    let robot_kit = setup_empty();
    let users = vec!["aa".to_string(), "bb".to_string()];
    let notifs = robot_kit
        .robot
        .execute_command(robot_kit.main_user, -1, PlayerCommand::NewGame(users))
        .unwrap();
    assert_eq!(
        notifs,
        &[Notification {
            tg_id: -1,
            msg: "Unknown user: aa".to_string()
        }]
    )
}

#[test]
fn test_new_game_player_ok() {
    let robot_kit = setup_empty();
    robot_kit
        .base
        .update_user(&UserInfo::make("aa", "", "", 2), -1)
        .unwrap();
    robot_kit
        .base
        .update_user(&UserInfo::make("", "", "bb", 3), -1)
        .unwrap();
    let users = vec!["aa".to_string(), "bb".to_string()];
    let resps = robot_kit
        .robot
        .execute_command(robot_kit.main_user, -1, PlayerCommand::NewGame(users))
        .unwrap();
    assert_eq!(
        resps,
        &[Notification {
            tg_id: -1,
            msg: "started".to_string()
        }]
    );
    assert!(robot_kit.base.chat_state_repo.get_chat_state(-1).is_ok())
}

#[test]
fn test_new_game_robots_ok() {
    let robot_kit = setup_empty();
    let users = vec![
        RobotName::Daniel.to_string(),
        RobotName::Terence.to_string(),
    ];
    let resps = robot_kit
        .robot
        .execute_command(robot_kit.main_user, -1, PlayerCommand::NewGame(users))
        .unwrap();
    assert_eq!(
        resps,
        &[Notification {
            tg_id: -1,
            msg: "started".to_string()
        }]
    );
    assert!(robot_kit.base.chat_state_repo.get_chat_state(-1).is_ok());
    let (_, ref q1) = robot_kit.queues[0];
    let (_, ref q2) = robot_kit.queues[1];
    assert_eq!(
        q1.peek(1).unwrap()[0],
        ReactionRequest::NewGame { chat_id: -1 }
    );
    assert_eq!(
        q2.peek(1).unwrap()[0],
        ReactionRequest::NewGame { chat_id: -1 }
    );
}

#[test]
fn test_new_game_no_users() {
    let robot_kit = setup_empty();
    robot_kit
        .base
        .update_user(&UserInfo::make("aa", "", "", 2), -1)
        .unwrap();
    robot_kit
        .base
        .update_user(&UserInfo::make("", "", "bb", 3), -1)
        .unwrap();
    let users = vec![];
    let notifs = robot_kit
        .robot
        .execute_command(robot_kit.main_user, -1, PlayerCommand::NewGame(users))
        .unwrap();
    assert_eq!(
        notifs,
        &[Notification {
            tg_id: -1,
            msg: "Need at least 2 players".to_string()
        }]
    );
}

#[test]
fn test_stop_game_ok() {
    let robot_kit = setup_new_game();
    let resps = robot_kit
        .robot
        .execute_command(robot_kit.main_user, -1, PlayerCommand::StopGame)
        .unwrap();
    assert_eq!(
        resps,
        &[Notification {
            tg_id: -1,
            msg: "stopped".to_string()
        }]
    );
}

#[test]
fn test_stop_game_nf() {
    let robot_kit = setup_new_game();
    let err = robot_kit
        .robot
        .execute_command(robot_kit.main_user, -2, PlayerCommand::StopGame)
        .unwrap_err();
    assert_eq!(err, RobotError::GameNotFound);
}

#[test]
fn test_user_step_ok() {
    let robot_kit = setup_ready_to_user_step();
    let user = UserInfo::make("d", "e", "f", 2);
    let resps = robot_kit
        .robot
        .execute_command(user, -1, PlayerCommand::MakeStep(Step::Call))
        .unwrap();
    assert_eq!(resps, Vec::new());
}

#[test]
fn test_user_step_err() {
    let robot_kit = setup_ready_to_user_step();
    let notifs = robot_kit
        .robot
        .execute_command(
            robot_kit.main_user,
            -1,
            PlayerCommand::MakeStep(Step::Check),
        )
        .unwrap();
    assert_eq!(
        notifs,
        &[Notification {
            tg_id: -1,
            msg: "It's not your step now".to_string()
        }]
    );
}

#[test]
fn test_ai_step() {
    let mut robot_kit = setup_ready_to_user_step();
    let (_, ref queue) = robot_kit.queues[0];
    assert_eq!(queue.peek(2).unwrap().len(), 1);
    let user = UserInfo::make("d", "e", "f", 2);
    robot_kit
        .robot
        .execute_command(user, -1, PlayerCommand::MakeStep(Step::Call))
        .unwrap();
    // ai notified
    assert_eq!(queue.peek(2).unwrap().len(), 2);
    let notifs = robot_kit
        .robot
        .make_ai_step(RobotName::Daniel, -1, Step::Call)
        .unwrap();
    assert_eq!(
        notifs,
        vec![Notification {
            tg_id: -1,
            msg: "@C your step".to_string()
        }]
    );
}

#[test]
fn test_auto_step() {
    let mut robot_kit = setup_new_game();
    let before = robot_kit.base.get_state(-1).unwrap();
    let notifs = robot_kit.robot.find_and_make_auto_step();
    let after = robot_kit.base.get_state(-1).unwrap();
    assert!(notifs.is_empty());
    assert_eq!(before.round.state, RoundStateKind::BigBlind);
    assert_eq!(after.round.state, RoundStateKind::SmallBlinds);
}

#[test]
fn test_new_round() {
    let mut robot_kit = setup_ready_to_user_step();
    let user = UserInfo::make("d", "e", "f", 2);
    robot_kit
        .robot
        .execute_command(user, -1, PlayerCommand::MakeStep(Step::Fold))
        .unwrap();
    robot_kit
        .robot
        .make_ai_step(RobotName::Daniel, -1, Step::Fold)
        .unwrap();
    let state = robot_kit.base.get_state(-1).unwrap();
    assert_eq!(state.round.state, RoundStateKind::AllFolded);
    let notifs = robot_kit.robot.find_and_make_auto_step();
    assert_eq!(
        notifs,
        &[Notification {
            tg_id: -1,
            msg: "Round winner: @C".to_string()
        }]
    );
}

#[test]
fn test_ai_lost_round_notify() {
    let mut robot_kit = setup_ready_to_user_step();
    let user = UserInfo::make("d", "e", "f", 2);
    robot_kit
        .robot
        .execute_command(user.clone(), -1, PlayerCommand::MakeStep(Step::Raise(90)))
        .unwrap();
    robot_kit
        .robot
        .make_ai_step(RobotName::Daniel, -1, Step::Call)
        .unwrap();
    robot_kit
        .robot
        .execute_command(
            robot_kit.main_user.clone(),
            -1,
            PlayerCommand::MakeStep(Step::Fold),
        )
        .unwrap();
    robot_kit
        .robot
        .execute_command(user, -1, PlayerCommand::MakeStep(Step::Check))
        .unwrap();
    robot_kit
        .robot
        .make_ai_step(RobotName::Daniel, -1, Step::Fold)
        .unwrap();
    let state = robot_kit.base.get_state(-1).unwrap();
    assert_eq!(state.round.state, RoundStateKind::AllFolded);
    robot_kit.robot.find_and_make_auto_step();

    let state = robot_kit.base.get_state(-1).unwrap();
    assert_eq!(state.round.state, RoundStateKind::BigBlind);
    let (_, ref queue) = robot_kit.queues[0];
    let msg = queue.peek(10).unwrap().last().cloned().unwrap();
    assert_eq!(
        msg,
        ReactionRequest::LooseGame {
            chat_id: -1,
            winner: "@F".to_string()
        }
    );
}

#[test]
fn test_end_game() {
    let mut robot_kit = setup_ready_to_user_step();
    let user = UserInfo::make("d", "e", "f", 2);
    robot_kit
        .robot
        .execute_command(user.clone(), -1, PlayerCommand::MakeStep(Step::Raise(90)))
        .unwrap();
    robot_kit
        .robot
        .make_ai_step(RobotName::Daniel, -1, Step::Call)
        .unwrap();
    robot_kit
        .robot
        .execute_command(
            robot_kit.main_user.clone(),
            -1,
            PlayerCommand::MakeStep(Step::Call),
        )
        .unwrap();
    robot_kit
        .robot
        .execute_command(user, -1, PlayerCommand::MakeStep(Step::Fold))
        .unwrap();
    robot_kit
        .robot
        .make_ai_step(RobotName::Daniel, -1, Step::Fold)
        .unwrap();
    let state = robot_kit.base.get_state(-1).unwrap();
    assert_eq!(state.round.state, RoundStateKind::AllFolded);
    let notifs = robot_kit.robot.find_and_make_auto_step();
    assert_eq!(
        notifs,
        &[Notification {
            tg_id: -1,
            msg: "Game winner: @C".to_string()
        }]
    );
    let (_, ref queue) = robot_kit.queues[0];
    let messages = queue.peek(10).unwrap();
    assert_eq!(
        *messages.last().unwrap(),
        ReactionRequest::LooseGame {
            chat_id: -1,
            winner: "@C".to_string()
        }
    );
}

#[test]
fn test_ai_win_round_notify() {
    let mut robot_kit = setup_ready_to_user_step();
    let user = UserInfo::make("d", "e", "f", 2);
    robot_kit
        .robot
        .execute_command(user.clone(), -1, PlayerCommand::MakeStep(Step::Raise(90)))
        .unwrap();
    robot_kit
        .robot
        .make_ai_step(RobotName::Daniel, -1, Step::Call)
        .unwrap();
    robot_kit
        .robot
        .execute_command(
            robot_kit.main_user.clone(),
            -1,
            PlayerCommand::MakeStep(Step::Fold),
        )
        .unwrap();
    robot_kit
        .robot
        .execute_command(user, -1, PlayerCommand::MakeStep(Step::Fold))
        .unwrap();
    let state = robot_kit.base.get_state(-1).unwrap();
    assert_eq!(state.round.state, RoundStateKind::AllFolded);
    robot_kit.robot.find_and_make_auto_step();

    let state = robot_kit.base.get_state(-1).unwrap();
    assert_eq!(state.round.state, RoundStateKind::BigBlind);
    let (_, ref queue) = robot_kit.queues[0];
    let msg = queue.peek(10).unwrap().last().cloned().unwrap();
    assert_eq!(msg, ReactionRequest::WinRound { chat_id: -1 });
}

#[test]
fn test_ai_win_game_notify() {
    let mut robot_kit = setup_ready_to_user_step();
    let user = UserInfo::make("d", "e", "f", 2);
    robot_kit
        .robot
        .execute_command(user.clone(), -1, PlayerCommand::MakeStep(Step::Raise(90)))
        .unwrap();
    robot_kit
        .robot
        .make_ai_step(RobotName::Daniel, -1, Step::Call)
        .unwrap();
    robot_kit
        .robot
        .execute_command(
            robot_kit.main_user.clone(),
            -1,
            PlayerCommand::MakeStep(Step::Call),
        )
        .unwrap();
    robot_kit
        .robot
        .execute_command(user, -1, PlayerCommand::MakeStep(Step::Fold))
        .unwrap();
    robot_kit
        .robot
        .make_ai_step(RobotName::Daniel, -1, Step::Check)
        .unwrap();
    robot_kit
        .robot
        .execute_command(
            robot_kit.main_user.clone(),
            -1,
            PlayerCommand::MakeStep(Step::Fold),
        )
        .unwrap();
    let state = robot_kit.base.get_state(-1).unwrap();
    assert_eq!(state.round.state, RoundStateKind::AllFolded);
    let notifs = robot_kit.robot.find_and_make_auto_step();
    assert_eq!(
        notifs,
        &[Notification {
            tg_id: -1,
            msg: "Game winner: Daniel".to_string()
        }]
    );
    let (_, ref queue) = robot_kit.queues[0];
    let messages = queue.peek(10).unwrap();
    assert_eq!(
        *messages.last().unwrap(),
        ReactionRequest::WinGame { chat_id: -1 }
    );
}

#[test]
fn test_notify_robots_on_start() {
    let robot_kit = setup_ready_to_user_step();
    let user = UserInfo::make("d", "e", "f", 2);
    robot_kit
        .robot
        .execute_command(user.clone(), -1, PlayerCommand::MakeStep(Step::Raise(90)))
        .unwrap();
    {
        let (_, ref queue) = robot_kit.queues[0];
        // start game and step
        assert_eq!(queue.peek(10).unwrap().len(), 2);
        queue.pop().unwrap();
        queue.pop().unwrap();
    }
    robot_kit.robot.notify_ai_about_steps_on_init().unwrap();
    let (_, ref queue) = robot_kit.queues[0];
    let requests = queue.peek(10).unwrap();
    assert_eq!(requests.len(), 1);
    assert!(matches!(
        requests[0],
        ReactionRequest::StepRequest { chat_id: -1, .. },
    ))
}
