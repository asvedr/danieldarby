use std::collections::{HashMap, HashSet};
use std::str::FromStr;

use poker::errors::GameError;
use poker::{
    ExpectedAction, GameState, IGame, IGameSerializer, NextRoundResponse, Player, PlayerInfo,
    RoundStateKind, Step,
};

use crate::entities::db::UserInfo;
use crate::entities::errors::{DbError, RobotError};
use crate::entities::main_config::MainConfig;
use crate::entities::player::{PlayerCommand, ReactionRequest, RobotName};
use crate::entities::robot::{Notification, Notifications};
use crate::proto::robot::{
    IAutoStepNotifier, IAutoStepWorkFlow, ICommandParser, IDealerBase, IDealerRobotInner,
    IDealerToPlayerBus,
};
use crate::utils::algo::{capitalize_name, find_item};

pub struct Inner<DB, CP> {
    pub db: DB,
    pub parser: CP,
    pub robots: Vec<Box<dyn IDealerToPlayerBus>>,
    pub auto_step_notifiers: Vec<Box<dyn IAutoStepNotifier>>,
    pub auto_step_workflow: Vec<Box<dyn IAutoStepWorkFlow>>,
    pub config: MainConfig,
    pub game_manager: Box<dyn IGame>,
    pub ser: Box<dyn IGameSerializer>,
}

unsafe impl<A, B> Send for Inner<A, B> {}
unsafe impl<A, B> Sync for Inner<A, B> {}

impl<DB: IDealerBase, CP: ICommandParser> Inner<DB, CP> {
    pub fn execute_command(
        &self,
        user: UserInfo,
        chat_id: i64,
        command: PlayerCommand,
    ) -> Result<Notifications, RobotError> {
        match command {
            PlayerCommand::NewGame(players) => {
                self.new_game(chat_id, Some(user.tg_id), players)?;
                let notif = Notification {
                    tg_id: chat_id,
                    msg: "started".to_string(),
                };
                Ok(vec![notif])
            }
            PlayerCommand::NewGameRobotsOnly => {
                self.new_game(chat_id, None, RobotName::all_keys())?;
                let notif = Notification {
                    tg_id: chat_id,
                    msg: "started".to_string(),
                };
                Ok(vec![notif])
            }
            PlayerCommand::StopGame => {
                self.stop_game(chat_id)?;
                let notif = Notification {
                    tg_id: chat_id,
                    msg: "stopped".to_string(),
                };
                Ok(vec![notif])
            }
            PlayerCommand::ShowState => self.show_state(chat_id),
            PlayerCommand::MakeStep(step) => {
                self.make_player_step(&user.tg_id.to_string(), chat_id, step)
            }
            PlayerCommand::Help => {
                let notif = Notification {
                    tg_id: chat_id,
                    msg: self.parser.help(),
                };
                Ok(vec![notif])
            }
            PlayerCommand::ShowSteps => self.show_available_steps(user, chat_id),
            PlayerCommand::ShowCards => self.show_cards(user, chat_id),
        }
    }

    fn show_available_steps(
        &self,
        user: UserInfo,
        chat_id: i64,
    ) -> Result<Notifications, RobotError> {
        let state = self.db.get_state(chat_id)?;
        let id = Self::get_player_id(&state.players, &user.tg_id.to_string())?;
        let steps = self.game_manager.get_steps(&state, id);
        if steps.is_empty() {
            return Err(RobotError::GameError(GameError::NotYourStep));
        }
        let msg = self.ser.steps_to_str(&steps);
        Ok(vec![Notification {
            tg_id: chat_id,
            msg,
        }])
    }

    fn show_cards(&self, user: UserInfo, chat_id: i64) -> Result<Notifications, RobotError> {
        let state = self.db.get_state(chat_id)?;
        let id = Self::get_player_id(&state.players, &user.tg_id.to_string())?;
        if !state.round.players[id].active {
            return Err(RobotError::PlayerNotInGame);
        }
        let cards = self.ser.cards_to_str(&state.round.players[id].cards);
        let notif = Notification {
            tg_id: user.tg_id,
            msg: cards,
        };
        Ok(vec![notif])
    }

    fn normalize_players(
        &self,
        players: Vec<String>,
        chat_id: i64,
    ) -> Result<Vec<String>, RobotError> {
        let mut name_map = HashMap::new();
        let mut users: Vec<&str> = Vec::new();
        for player in players.iter() {
            if self.is_robot_name(player) {
                name_map.insert(player.clone(), player.clone());
            } else {
                users.push(player);
            }
        }
        let info_list = match self.db.get_user_info_list(&users, chat_id) {
            Ok(val) => val,
            Err(DbError::NotFound(Some(val))) => return Err(RobotError::UnknownUser(val)),
            Err(err) => return Err(err.into()),
        };
        for (key, user) in info_list {
            name_map.insert(key, user.tg_id.to_string());
        }
        let mut result = Vec::new();
        for player in players {
            if let Some(val) = name_map.remove(&player) {
                result.push(val)
            } else {
                return Err(RobotError::UnknownUser(player));
            }
        }
        Ok(result)
    }

    fn new_game(
        &self,
        chat_id: i64,
        first_player_id_opt: Option<i64>,
        players: Vec<String>,
    ) -> Result<(), RobotError> {
        let mut players = self.normalize_players(players, chat_id)?;
        if let Some(id) = first_player_id_opt {
            players.insert(0, id.to_string());
        }
        for player in players.iter() {
            if self.is_robot_name(player) {
                let robot = RobotName::from_str(player).unwrap();
                let bus = self.get_robot(robot)?;
                bus.push_request(ReactionRequest::NewGame { chat_id })?;
            }
        }
        let state = self.game_manager.new_game(players, self.config.balance)?;
        self.save_state(chat_id, state)?;
        Ok(())
    }

    fn stop_game(&self, chat_id: i64) -> Result<(), RobotError> {
        match self.db.get_state(chat_id) {
            Ok(_) => self.db.del_state(chat_id)?,
            Err(DbError::NotFound(_)) => return Err(RobotError::GameNotFound),
            Err(err) => return Err(err.into()),
        }
        Ok(())
    }

    fn get_player_names(
        &self,
        players: &[PlayerInfo],
        chat_id: i64,
    ) -> Result<HashMap<String, String>, RobotError> {
        let mut result = HashMap::new();
        let mut users: Vec<&str> = Vec::new();
        for player in players {
            if i64::from_str(&player.name).is_ok() {
                users.push(&player.name)
            } else {
                result.insert(player.name.to_string(), player.name.to_string());
            }
        }
        for (key, user) in self.db.get_user_info_list(&users, chat_id)? {
            result.insert(key, user.get_name());
        }
        if result.len() >= players.len() {
            return Ok(result);
        }
        for player in players {
            if result.get(&player.name).is_none() {
                return Err(RobotError::UnknownUser(player.name.clone()));
            }
        }
        unreachable!()
    }

    fn show_state(&self, chat_id: i64) -> Result<Notifications, RobotError> {
        let state = self.db.get_state(chat_id)?;
        let shared = self.game_manager.get_shared_info(&state);
        let players = self.get_player_names(&shared.players, chat_id)?;
        let current = players
            .get(&state.players[state.round.current_player].name)
            .unwrap();
        let shared = format!(
            "players:\n{players}\ncards on desk: {cards}\nbank: {bank}\ncurrent player: {current}\nbet: {bet}",
            players=shared.players.iter()
                .filter(|p|p.has_cards)
                .map(|p|
                    format!("-  {}, bet={}", players.get(&p.name).unwrap(), p.bet)
                )
                .collect::<Vec<_>>()
                .join("\n"),
            cards=self.ser.cards_to_str(&shared.cards),
            bank=shared.bank,
            current=current,
            bet=state.round.state.get_bet_opt()
                .map_or_else(||"None".to_string(), |val|val.to_string())
        );
        let mut result = vec![Notification {
            tg_id: chat_id,
            msg: shared,
        }];
        for (id, player) in state.round.players.iter().enumerate() {
            if !player.active {
                continue;
            }
            if let Ok(tg_id) = i64::from_str(&state.players[id].name) {
                let msg = format!("your cards: {}", self.ser.cards_to_str(&player.cards));
                result.push(Notification { tg_id, msg })
            }
        }
        Ok(result)
    }

    pub fn make_player_step(
        &self,
        user: &str,
        chat_id: i64,
        step: Step,
    ) -> Result<Vec<Notification>, RobotError> {
        let state = self.db.get_state(chat_id)?;
        let in_game_id = Self::get_player_id(&state.players, user)?;
        let new_state = self
            .game_manager
            .apply_user_step(&state, in_game_id, step)?;
        let notif = self.notify_active_player(chat_id, &new_state)?;
        self.save_state(chat_id, new_state)?;
        Ok(match notif {
            None => vec![],
            Some(val) => vec![val],
        })
    }

    pub fn is_game_active(&self, chat_id: i64) -> Result<bool, RobotError> {
        match self.db.get_state(chat_id) {
            Ok(_) => Ok(true),
            Err(DbError::NotFound(_)) => Ok(false),
            Err(err) => Err(err.into()),
        }
    }

    /// result: (expected_ai, expected_auto)
    fn get_expected_params(&self, state: &GameState) -> (bool, bool) {
        match self.game_manager.get_expected_action(state) {
            ExpectedAction::Nothing => panic!("exp::nothing"), // (false, false)
            ExpectedAction::AutoStep(_) => (false, true),
            ExpectedAction::UserStep(id) => {
                let p_name = &state.players[id].name;
                (isize::from_str(p_name).is_err(), false)
            }
        }
    }

    pub fn find_and_make_auto_step(&self) -> Notifications {
        let (chat, state) = match self.db.get_state_for_auto_step() {
            Ok(val) => val,
            Err(DbError::NotFound(_)) => return Vec::new(),
            Err(err) => {
                eprintln!("get_state_for_auto_step error: {:?}", err);
                return Vec::new();
            }
        };
        let nrr = self.game_manager.next_round(&state);
        let wf = find_item(&self.auto_step_workflow, |wf| wf.matches(&nrr));
        match self.make_auto_step_and_notify_robot(&**wf, chat, &state, nrr) {
            Ok(notif) => notif,
            Err(err) => {
                let msg = format!("ERR: {:?}", err);
                vec![Notification { tg_id: chat, msg }]
            }
        }
    }

    pub fn notify_ai_about_steps(&self) -> Result<(), RobotError> {
        for (chat, state) in self.db.get_states_for_ai_step()? {
            let notif = self.notify_active_player(chat, &state)?;
            assert_eq!(notif, None);
        }
        Ok(())
    }

    fn make_auto_step_and_notify_robot(
        &self,
        wf: &dyn IAutoStepWorkFlow,
        chat: i64,
        state: &GameState,
        nrr: NextRoundResponse,
    ) -> Result<Notifications, RobotError> {
        let auto_res = wf.process_state(self, chat, state, nrr)?;
        let opt_player_notif =
            self.notify_active_player(chat, auto_res.state.as_ref().unwrap_or(state))?;
        if let Some(new_state) = auto_res.state {
            self.save_state(chat, new_state)?;
        } else if auto_res.stop_game {
            self.db.del_state(chat)?;
        }
        let mut result = auto_res.notifications;
        if let Some(notif) = opt_player_notif {
            result.push(notif);
        }
        Ok(result)
    }

    fn save_state(&self, chat_id: i64, state: GameState) -> Result<(), RobotError> {
        let (ai, auto) = self.get_expected_params(&state);
        Ok(self.db.save_state(chat_id, ai, auto, state)?)
    }

    fn notify_active_player(
        &self,
        chat_id: i64,
        state: &GameState,
    ) -> Result<Option<Notification>, RobotError> {
        let (name, id) = match self.game_manager.get_expected_action(state) {
            ExpectedAction::UserStep(id) => (&state.players[id].name, id),
            _ => return Ok(None),
        };
        let is_player = !self.is_robot_name(name);
        let name = self.get_player_name(name)?;
        if is_player {
            let notif = Self::make_player_notification(chat_id, name);
            Ok(Some(notif))
        } else {
            self.notif_robot(chat_id, id, name, state)?;
            Ok(None)
        }
    }

    fn notif_robot(
        &self,
        chat_id: i64,
        user_id: usize,
        name: String,
        state: &GameState,
    ) -> Result<(), RobotError> {
        let info = self.game_manager.get_user_info(state, user_id);
        let steps = self.game_manager.get_steps(state, user_id);
        let robot = RobotName::from_str(&name).unwrap();
        let request = ReactionRequest::StepRequest {
            chat_id,
            user_info: info,
            steps,
        };
        self.get_robot(robot)?.push_request(request)
    }

    fn make_player_notification(chat_id: i64, name: String) -> Notification {
        Notification {
            tg_id: chat_id,
            msg: format!("{} your step", name),
        }
    }

    fn get_player_id(players: &[Player], user: &str) -> Result<usize, RobotError> {
        for (id, candidate) in players.iter().enumerate() {
            if candidate.name == user {
                return Ok(id);
            }
        }
        Err(RobotError::PlayerNotInGame)
    }

    fn active_players(players: &[Player]) -> HashSet<&str> {
        let mut result = HashSet::new();
        for player in players {
            if player.balance > 0 {
                result.insert(&player.name as &str);
            }
        }
        result
    }
}

impl<DB: IDealerBase, CP: ICommandParser> IDealerRobotInner for Inner<DB, CP> {
    fn game_manager(&self) -> &dyn IGame {
        &*self.game_manager
    }

    fn ser(&self) -> &dyn IGameSerializer {
        &*self.ser
    }

    fn db(&self) -> &dyn IDealerBase {
        &self.db
    }

    fn get_robot(&self, name: RobotName) -> Result<&dyn IDealerToPlayerBus, RobotError> {
        for robot in self.robots.iter() {
            if robot.robot() == name {
                return Ok(&**robot);
            }
        }
        Err(RobotError::NoBusForRobot(name))
    }

    fn get_player_name(&self, robot_name_or_id: &str) -> Result<String, RobotError> {
        let id = match i64::from_str(robot_name_or_id) {
            Ok(val) => val,
            Err(_) => return Ok(capitalize_name(robot_name_or_id)),
        };
        let info = self.db.get_user_by_id(id)?;
        if !info.username.is_empty() {
            Ok(capitalize_name(&format!("@{}", info.username)))
        } else {
            Ok(capitalize_name(&info.full_name()))
        }
    }

    fn is_robot_name(&self, name: &str) -> bool {
        for robot in self.robots.iter() {
            if robot.robot().to_str() == name {
                return true;
            }
        }
        false
    }

    fn get_state_notification(
        &self,
        chat: i64,
        kind: RoundStateKind,
        state: &GameState,
    ) -> Result<Notifications, RobotError> {
        for notifier in self.auto_step_notifiers.iter() {
            if notifier.matches(&kind) {
                return notifier.make_notifications(self, chat, state);
            }
        }
        Ok(Vec::new())
    }

    fn get_lost_robots(&self, old_state: &GameState, new_state: &GameState) -> Vec<RobotName> {
        let before = Self::active_players(&old_state.players);
        let after = Self::active_players(&new_state.players);
        let mut result = Vec::new();
        if before.len() <= after.len() {
            return result;
        }
        for name in before.difference(&after) {
            if self.is_robot_name(name) {
                result.push(RobotName::from_str(name).unwrap())
            }
        }
        result
    }
}
