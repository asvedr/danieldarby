use std::collections::VecDeque;
use std::sync::{Arc, Mutex, MutexGuard};

use crate::entities::errors::QueueError;
use crate::proto::queue::IQueue;

#[derive(Clone)]
pub struct Queue<T> {
    inner: Arc<Mutex<VecDeque<T>>>,
    name: String,
    max_len: Option<usize>,
}

unsafe impl<T> Send for Queue<T> {}
unsafe impl<T> Sync for Queue<T> {}

impl<T> Queue<T> {
    pub fn new<N: ToString>(name: N, max_len: Option<usize>) -> Self {
        Self {
            inner: Arc::new(Mutex::new(VecDeque::new())),
            max_len,
            name: name.to_string(),
        }
    }

    fn lock(&self) -> Result<MutexGuard<VecDeque<T>>, QueueError> {
        match self.inner.lock() {
            Ok(val) => Ok(val),
            Err(_) => Err(QueueError::MutexPoisoned(self.name.clone())),
        }
    }
}

impl<T: Clone> IQueue<T> for Queue<T> {
    fn max_len(&self) -> Option<usize> {
        self.max_len
    }

    fn push(&self, value: T) -> Result<(), QueueError> {
        let mut lock = self.lock()?;
        lock.push_back(value);
        match self.max_len {
            Some(val) if lock.len() > val => {
                lock.pop_front();
            }
            _ => (),
        }
        Ok(())
    }

    fn pop(&self) -> Result<Option<T>, QueueError> {
        Ok(self.lock()?.pop_front())
    }

    fn peek(&self, count: usize) -> Result<Vec<T>, QueueError> {
        let ptr = self.lock()?;
        let values = ptr.iter().take(count).cloned().collect();
        Ok(values)
    }

    fn filter(&self, predicate: &dyn Fn(&T) -> bool) -> Result<(), QueueError> {
        let mut lock = self.lock()?;
        let queue = std::mem::take(&mut *lock);
        for val in queue.into_iter() {
            if predicate(&val) {
                lock.push_back(val)
            }
        }
        Ok(())
    }
}
