use crate::entities::player::PlayerCommand;
use crate::proto::robot::ICommandParser;
use poker::{create_game_deserializer, IGameDeserializer};

pub struct CommandParser {
    de: Box<dyn IGameDeserializer>,
}

impl Default for CommandParser {
    fn default() -> Self {
        Self {
            de: create_game_deserializer(),
        }
    }
}

impl CommandParser {
    fn parse_players(text: String) -> Vec<String> {
        text.split(',')
            .map(|token| token.trim().to_string())
            .collect()
    }
}

impl ICommandParser for CommandParser {
    fn parse_command(&self, text: &str) -> Option<PlayerCommand> {
        if !text.starts_with('/') {
            return None;
        }
        let normalized = text[1..].trim().to_lowercase();
        let split = normalized.split(' ').collect::<Vec<_>>();
        match split[..] {
            ["new", "game", "robots", "only"] => {
                return Some(PlayerCommand::NewGameRobotsOnly);
            }
            ["new", "game", ..] => {
                let players = Self::parse_players(split[2..].join(" "));
                return Some(PlayerCommand::NewGame(players));
            }
            ["stop", "game"] => return Some(PlayerCommand::StopGame),
            ["show", "board"] => return Some(PlayerCommand::ShowState),
            ["help"] => return Some(PlayerCommand::Help),
            ["show", "steps"] => return Some(PlayerCommand::ShowSteps),
            ["show", "my", "cards"] => return Some(PlayerCommand::ShowCards),
            _ => (),
        }
        let step = self.de.str_to_step(&normalized).ok()?;
        Some(PlayerCommand::MakeStep(step))
    }

    fn help(&self) -> String {
        concat!(
            "/help - Show this message\n",
            "/new game <player1>, [player2], ... - Start game with players\n",
            "/new game robots only - Run robots against each othern\n",
            "/stop game - Stop game in current chat\n",
            "/show board - Show current game state\n",
            "/show steps - Show available steps\n",
            "/show my cards - Send me my cards in private message\n",
            "/fold - make step \"fold\"\n",
            "/call - make step \"call\"\n",
            "/raise N - make step \"raise\" on N amount\n",
            "/check - make step \"check\"\n",
        )
        .to_string()
    }
}
