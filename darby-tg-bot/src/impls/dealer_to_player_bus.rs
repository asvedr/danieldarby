use crate::entities::errors::RobotError;
use crate::entities::player::{ReactionRequest, RobotName};
use crate::proto::queue::IQueue;
use crate::proto::robot::IDealerToPlayerBus;

#[derive(Clone)]
pub struct DealerToPlayerBus<Q: IQueue<ReactionRequest>> {
    pub robot: RobotName,
    pub queue: Q,
}

impl<Q: IQueue<ReactionRequest>> IDealerToPlayerBus for DealerToPlayerBus<Q> {
    fn robot(&self) -> RobotName {
        self.robot
    }

    fn push_request(&self, request: ReactionRequest) -> Result<(), RobotError> {
        self.queue.push(request)?;
        Ok(())
    }
}
