use std::thread;
use std::time::Duration;

use poker::{create_game_serializer, IAi, IGameSerializer, Step, UserGameInfo};

use crate::entities::ai_config::{Reactions, ReactionsConfig, MSG_SEPARATOR, NAME_TEMPLATE};
use crate::entities::errors::RobotError;
use crate::entities::main_config::MainConfig;
use crate::entities::player::{NewPlayerStep, ReactionRequest, RobotName};
use crate::proto::db::IAiStateRepo;
use crate::proto::queue::IQueue;
use crate::proto::robot::{IRobot, ISyncTgSender};

pub struct AiRobotRequest<
    BFD: IQueue<ReactionRequest>,
    BTD: IQueue<NewPlayerStep>,
    SR: IAiStateRepo,
> {
    pub name: RobotName,
    pub token: String,
    pub sleep: Duration,
    pub bus_from_dealer: BFD,
    pub bus_to_dealer: BTD,
    pub ai: Box<dyn IAi>,
    pub state_repo: SR,
    pub reactions_config: ReactionsConfig,
}

pub struct AiRobot<
    BFD: IQueue<ReactionRequest>,
    BTD: IQueue<NewPlayerStep>,
    SR: IAiStateRepo,
    Sn: ISyncTgSender,
> {
    name: RobotName,
    sleep: Duration,
    bus_from_dealer: BFD,
    bus_to_dealer: BTD,
    ai: Box<dyn IAi>,
    state_repo: SR,
    reactions_config: ReactionsConfig,
    pub sender: Sn,
    ser: Box<dyn IGameSerializer>,
}

impl<BFD: IQueue<ReactionRequest>, BTD: IQueue<NewPlayerStep>, SR: IAiStateRepo>
    AiRobotRequest<BFD, BTD, SR>
{
    pub fn build<Sn: ISyncTgSender>(self, config: &MainConfig) -> AiRobot<BFD, BTD, SR, Sn> {
        AiRobot {
            name: self.name,
            sleep: self.sleep,
            bus_from_dealer: self.bus_from_dealer,
            bus_to_dealer: self.bus_to_dealer,
            ai: self.ai,
            state_repo: self.state_repo,
            reactions_config: self.reactions_config,
            sender: Sn::build(self.token, config),
            ser: create_game_serializer(false),
        }
    }
}

impl<
        BFD: IQueue<ReactionRequest>,
        BTD: IQueue<NewPlayerStep>,
        SR: IAiStateRepo,
        Sn: ISyncTgSender,
    > AiRobot<BFD, BTD, SR, Sn>
{
    fn peek_request(&self) -> Result<Option<ReactionRequest>, RobotError> {
        let mut messages = self.bus_from_dealer.peek(1)?;
        if messages.is_empty() {
            return Ok(None);
        }
        Ok(messages.pop())
    }

    pub fn find_and_process_request(&self) -> Result<bool, RobotError> {
        let request = match self.peek_request()? {
            None => return Ok(false),
            Some(val) => val,
        };
        match request {
            ReactionRequest::NewGame { chat_id } => self.react_new_game(chat_id)?,
            ReactionRequest::StepRequest {
                chat_id,
                user_info,
                steps,
            } => self.react_make_step(chat_id, user_info, steps)?,
            ReactionRequest::NewRound { chat_id } => self.state_repo.del_state(chat_id)?,
            ReactionRequest::WinGame { chat_id } => self.react_win_game(chat_id)?,
            ReactionRequest::WinRound { chat_id } => self.react_win_round(chat_id)?,
            ReactionRequest::LooseGame { chat_id, winner } => {
                self.react_loose_game(chat_id, winner)?
            }
            ReactionRequest::LooseRound { chat_id, winner } => {
                self.react_loose_round(chat_id, winner)?
            }
        }
        self.bus_from_dealer.pop()?;
        Ok(true)
    }

    fn react_new_game(&self, chat_id: i64) -> Result<(), RobotError> {
        self.state_repo.del_state(chat_id)?;
        self.take_and_send_reaction(chat_id, &self.reactions_config.new_game, None)
    }

    fn react_make_step(
        &self,
        chat_id: i64,
        user_info: UserGameInfo,
        steps: Vec<Step>,
    ) -> Result<(), RobotError> {
        let mut ai_state = self.state_repo.get_state(chat_id)?;
        let step = self.ai.choose_step(&user_info, &mut ai_state, &steps);
        self.state_repo.save_state(chat_id, ai_state)?;
        println!(
            "AI: {:?}, chat: {}, step: {:?}, reason: {:?}",
            self.name, chat_id, step.step, step.info,
        );
        let str_step = format!("/{}", self.ser.step_to_str(&step.step));
        // self.bus_to_dealer.push_step(chat_id, step.step)?;
        let raise_react_required = matches!(step.step, Step::Raise(_) | Step::Call);
        let bus_step = NewPlayerStep {
            player: self.name,
            chat_id,
            step: step.step,
        };
        let res = self.sender.send_message(chat_id, &[&str_step]);
        if raise_react_required {
            self.take_and_send_reaction(chat_id, &self.reactions_config.raise, None)?;
        }
        if let Err(ref err) = res {
            eprintln!("AI: failed send reaction: {:?}", err);
        }
        res?;
        self.bus_to_dealer.push(bus_step)?;
        Ok(())
    }

    fn react_win_game(&self, chat_id: i64) -> Result<(), RobotError> {
        self.state_repo.del_state(chat_id)?;
        self.take_and_send_reaction(chat_id, &self.reactions_config.win_game, None)
    }

    fn react_win_round(&self, chat_id: i64) -> Result<(), RobotError> {
        self.state_repo.del_state(chat_id)?;
        self.take_and_send_reaction(chat_id, &self.reactions_config.win_round, None)
    }

    fn react_loose_game(&self, chat_id: i64, winner: String) -> Result<(), RobotError> {
        self.state_repo.del_state(chat_id)?;
        self.take_and_send_reaction(chat_id, &self.reactions_config.loose_game, Some(winner))
    }

    fn react_loose_round(&self, chat_id: i64, winner: String) -> Result<(), RobotError> {
        self.state_repo.del_state(chat_id)?;
        self.take_and_send_reaction(chat_id, &self.reactions_config.loose_round, Some(winner))
    }

    fn take_and_send_reaction(
        &self,
        tg_id: i64,
        config: &Reactions,
        name: Option<String>,
    ) -> Result<(), RobotError> {
        if let Some(msg) = Self::take_reaction(config, name) {
            let msgs = msg.split(MSG_SEPARATOR).collect::<Vec<_>>();
            let res = self.sender.send_message(tg_id, &msgs);
            if let Err(ref err) = res {
                eprintln!("AI: send reaction error: {:?}", err);
            }
            return res;
        }
        Ok(())
    }

    fn take_reaction(config: &Reactions, opt_name: Option<String>) -> Option<String> {
        if config.phrases.is_empty() {
            return None;
        }
        let rand = fastrand::f64();
        if rand > config.prob {
            return None;
        }
        let index = fastrand::usize(..config.phrases.len());
        let phrase = &config.phrases[index];
        if let Some(name) = opt_name {
            Some(phrase.replace(NAME_TEMPLATE, &name))
        } else {
            Some(phrase.clone())
        }
    }
}

impl<
        BFD: IQueue<ReactionRequest>,
        BTD: IQueue<NewPlayerStep>,
        SR: IAiStateRepo,
        Sn: ISyncTgSender,
    > IRobot for AiRobot<BFD, BTD, SR, Sn>
{
    fn name(&self) -> &str {
        self.name.to_str()
    }

    fn run(&mut self) -> Result<(), RobotError> {
        loop {
            match self.find_and_process_request() {
                Ok(true) => (),
                Ok(false) => thread::sleep(self.sleep),
                Err(err) => eprintln!("AI({}) error: {:?}", self.name.to_str(), err),
            }
        }
    }
}
