#[derive(Debug, Clone, Eq, PartialEq)]
pub struct UserInfo {
    pub first_name: String,
    pub last_name: String,
    pub username: String,
    pub tg_id: i64,
    pub is_robot: bool,
}

impl UserInfo {
    #[cfg(test)]
    pub fn make<FN: ToString, LN: ToString, U: ToString>(
        first_name: FN,
        last_name: LN,
        username: U,
        tg_id: i64,
    ) -> Self {
        Self {
            first_name: first_name.to_string(),
            last_name: last_name.to_string(),
            username: username.to_string(),
            tg_id,
            is_robot: false,
        }
    }

    pub fn get_name(&self) -> String {
        let joined = format!("{} {}", self.first_name, self.last_name);
        let stripped = joined.trim();
        if !self.username.is_empty() {
            format!("{}(@{})", stripped, self.username)
        } else {
            stripped.to_string()
        }
    }

    pub fn full_name(&self) -> String {
        format!("{} {}", self.first_name, self.last_name)
            .trim()
            .to_string()
    }

    pub fn rev_full_name(&self) -> String {
        format!("{} {}", self.last_name, self.first_name)
            .trim()
            .to_string()
    }
}
