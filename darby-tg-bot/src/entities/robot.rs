#[derive(Debug, Eq, PartialEq)]
pub struct Notification {
    pub tg_id: i64,
    pub msg: String,
}

pub type Notifications = Vec<Notification>;
