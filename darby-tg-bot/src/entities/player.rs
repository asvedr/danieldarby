use poker::Step;
use poker::UserGameInfo;
use std::str::FromStr;

#[derive(Debug, Copy, Clone, PartialEq, Eq, Ord, PartialOrd)]
pub enum RobotName {
    Daniel,
    Terence,
    Dificento,
}

impl RobotName {
    const PAIRS: &'static [(Self, &'static str)] = &[
        (Self::Daniel, "daniel"),
        (Self::Terence, "terence"),
        (Self::Dificento, "dificento"),
    ];

    pub fn all_keys() -> Vec<String> {
        Self::PAIRS
            .iter()
            .map(|(_, name)| name.to_string())
            .collect()
    }

    pub fn to_str(self) -> &'static str {
        for (name, value) in Self::PAIRS {
            if self == *name {
                return value;
            }
        }
        unreachable!()
    }
}

impl ToString for RobotName {
    fn to_string(&self) -> String {
        for (name, value) in Self::PAIRS {
            if self == name {
                return value.to_string();
            }
        }
        unreachable!()
    }
}

impl FromStr for RobotName {
    type Err = ();

    fn from_str(src: &str) -> Result<Self, Self::Err> {
        let lower = src.trim().to_lowercase();
        for (name, value) in Self::PAIRS {
            if *value == lower {
                return Ok(*name);
            }
        }
        Err(())
    }
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub enum ReactionRequest {
    NewGame {
        chat_id: i64,
    },
    StepRequest {
        chat_id: i64,
        user_info: UserGameInfo,
        steps: Vec<Step>,
    },
    NewRound {
        chat_id: i64,
    },
    WinGame {
        chat_id: i64,
    },
    WinRound {
        chat_id: i64,
    },
    LooseGame {
        chat_id: i64,
        winner: String,
    },
    LooseRound {
        chat_id: i64,
        winner: String,
    },
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct NewPlayerStep {
    pub player: RobotName,
    pub chat_id: i64,
    pub step: Step,
}

#[derive(Debug, PartialEq, Eq)]
pub struct PlayerStep {
    pub id: usize,
    pub new: NewPlayerStep,
}

#[derive(Clone)]
pub enum PlayerCommand {
    NewGame(Vec<String>),
    NewGameRobotsOnly,
    StopGame,
    ShowState,
    MakeStep(Step),
    Help,
    ShowSteps,
    ShowCards,
}
