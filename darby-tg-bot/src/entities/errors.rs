use crate::entities::player::RobotName;
use poker::errors::GameError;
use std::sync::PoisonError;
use telegram_bot_ars::Error;

#[derive(Debug)]
pub enum RobotError {
    GameNotFound,
    DbError(DbError),
    TgError(telegram_bot_ars::Error),
    GameError(GameError),
    QueueError(QueueError),
    UnknownUser(String),
    PlayerNotInGame,
    NoBusForRobot(RobotName),
}

impl Eq for RobotError {}

impl PartialEq for RobotError {
    fn eq(&self, other: &Self) -> bool {
        format!("{:?}", self) == format!("{:?}", other)
    }
}

#[derive(Debug, PartialEq, Eq)]
pub enum DbError {
    CanNotConnect(String),
    CanNotInitTable(&'static str, String),
    NotFound(Option<String>),
    MutexError,
    DeserializationError(String),
    Other(String),
}

#[derive(Debug, Eq, PartialEq)]
pub enum QueueError {
    MutexPoisoned(String),
}

#[derive(Debug)]
pub enum ConfigError {
    VariableNotSet(String),
    InvalidVariable(String, String),
    CanNotReadFile(String),
    CanNotParseFile(String, String),
    InvalidJson,
    CanNotStartBase(String),
    CanNotReadAi(RobotName, String),
}

pub type DbResult<T> = Result<T, DbError>;

impl<T> From<PoisonError<T>> for DbError {
    fn from(_: PoisonError<T>) -> Self {
        DbError::MutexError
    }
}

impl From<rusqlite::Error> for DbError {
    fn from(src: rusqlite::Error) -> Self {
        DbError::Other(src.to_string())
    }
}

impl From<QueueError> for RobotError {
    fn from(src: QueueError) -> Self {
        Self::QueueError(src)
    }
}

impl From<DbError> for RobotError {
    fn from(src: DbError) -> Self {
        Self::DbError(src)
    }
}

impl From<telegram_bot_ars::Error> for RobotError {
    fn from(src: Error) -> Self {
        Self::TgError(src)
    }
}

impl From<GameError> for RobotError {
    fn from(src: GameError) -> Self {
        Self::GameError(src)
    }
}
