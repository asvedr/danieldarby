use crate::entities::errors::ConfigError;
use crate::entities::player::RobotName;
use crate::utils::env::{as_str, parse_var, parse_var_opt};
use std::str::FromStr;
use std::time::Duration;

#[derive(Clone)]
pub struct MainConfig {
    pub token: String,
    pub dealer_db: String,
    pub balance: usize,
    pub inactive_state_lifetime: usize,
    pub available_ai: Vec<RobotName>,
    pub ai_conf_dir: String,
    pub sleep: Duration,
    pub min_tg_timeout: Duration,
    pub tg_tries: usize,
    pub rebuild_tg_sender: bool,
}

fn parse_ai_list(src: &str) -> Result<Vec<RobotName>, String> {
    let mut result = Vec::new();
    for key in src.split(',') {
        if let Ok(val) = RobotName::from_str(key) {
            result.push(val);
        } else {
            return Err(format!("invalid robot name: {}", key));
        }
    }
    Ok(result)
}

fn parse_bool(src: &str) -> Result<bool, String> {
    match &src.to_lowercase()[..] {
        "true" | "1" | "yes" => Ok(true),
        "false" | "0" | "no" => Ok(false),
        _ => Err(format!("invalid bool value: {:?}", src)),
    }
}

impl MainConfig {
    pub fn build() -> Result<Self, ConfigError> {
        let config = Self {
            token: parse_var("DARBY_DEALER_TOKEN", as_str)?,
            dealer_db: parse_var("DARBY_DB", as_str)?,
            balance: parse_var_opt("DARBY_INIT_BALANCE", usize::from_str, || 100)?,
            inactive_state_lifetime: parse_var_opt(
                "DARBY_INACTIVE_STATE_LIFETIME",
                usize::from_str,
                || 60 * 60 * 60,
            )?,
            available_ai: parse_var("DARBY_AVAILABLE_AI", parse_ai_list)?,
            ai_conf_dir: parse_var("DARBY_AI_CONF_DIR", as_str)?,
            sleep: Duration::from_secs_f64(parse_var_opt("DARBY_SLEEP", f64::from_str, || 1.0)?),
            min_tg_timeout: Duration::from_secs_f64(parse_var_opt(
                "DARBY_TG_TIMEOUT",
                f64::from_str,
                || 1.0,
            )?),
            tg_tries: parse_var_opt("DARBY_TG_TRIES", usize::from_str, || 100)?,
            rebuild_tg_sender: parse_var_opt("DARBY_REBUILD_TG_SENDER", parse_bool, || true)?,
        };
        Ok(config)
    }
}
