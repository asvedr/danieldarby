use json::object::Object;
use json::JsonValue;
use std::str::FromStr;

use crate::entities::errors::ConfigError;

pub const NAME_TEMPLATE: &str = "%name%";
pub const MSG_SEPARATOR: &str = "%sep%";

#[derive(Debug)]
pub struct Reactions {
    pub prob: f64,
    pub phrases: Vec<String>,
}

#[derive(Debug)]
pub struct ReactionsConfig {
    pub win_round: Reactions,
    pub win_game: Reactions,
    pub loose_round: Reactions,
    pub loose_game: Reactions,
    pub new_game: Reactions,
    pub raise: Reactions,
}

impl Reactions {
    fn parse_obj(obj: &Object) -> Option<Self> {
        let prob = obj.get("prob")?.as_f64()?;
        let js_phrases = match obj.get("phrases")? {
            JsonValue::Array(val) => val,
            _ => return None,
        };
        let mut phrases = Vec::new();
        for js_phrase in js_phrases {
            phrases.push(js_phrase.as_str()?.to_string());
        }
        Some(Self { prob, phrases })
    }

    fn from_obj(obj: &Object, label: &str) -> Result<Self, ConfigError> {
        match Self::parse_obj(obj) {
            None => {
                eprintln!("can not parse {}", label);
                Err(ConfigError::InvalidJson)
            }
            Some(val) => Ok(val),
        }
    }
}

fn get_obj<'a>(obj: &'a Object, field: &str) -> Result<&'a Object, ConfigError> {
    match obj.get(field) {
        None => {
            eprintln!("field {} not found", field);
            Err(ConfigError::InvalidJson)
        }
        Some(JsonValue::Object(val)) => Ok(val),
        Some(_) => {
            eprintln!("field {} not an object", field);
            Err(ConfigError::InvalidJson)
        }
    }
}

impl FromStr for ReactionsConfig {
    type Err = ConfigError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let js = json::parse(s).map_err(|_| ConfigError::InvalidJson)?;
        let toplevel = match js {
            JsonValue::Object(val) => val,
            _ => {
                eprintln!("reaction config not object");
                return Err(ConfigError::InvalidJson);
            }
        };
        Ok(Self {
            win_round: Reactions::from_obj(get_obj(&toplevel, "win_round")?, "win_round")?,
            win_game: Reactions::from_obj(get_obj(&toplevel, "win_game")?, "win_game")?,
            loose_round: Reactions::from_obj(get_obj(&toplevel, "loose_round")?, "loose_round")?,
            loose_game: Reactions::from_obj(get_obj(&toplevel, "loose_game")?, "loose_game")?,
            new_game: Reactions::from_obj(get_obj(&toplevel, "new_game")?, "new_game")?,
            raise: Reactions::from_obj(get_obj(&toplevel, "raise")?, "raise")?,
        })
    }
}
