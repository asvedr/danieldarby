import json
import re

import pytest

from danieldarby.manager import Manager, UserInfo, SharedGameInfo, PlayerInfo
from danieldarby.ai import Ai
from danieldarby.errors import Errors

schema = {
    'config': {
        'tries': 1000,
        'bluff_va_banque': 0,
        'bluff': 0,
        'gain': 0.15,
    },
    'strategies': {
        'gain': [
            {'str': 'chca', 'Mc': 2},
            {'str': 'chca', 'mwp': 0.1, 'Mwp': 0.4},
            {'str': 'rse 0.2', 'mwp': 0.4, 'Mwp': 0.6},
            {'str': 'mrse', 'mwp': 0.6},
        ],
        'safe': [
            {'str': 'fold', 'Mwp': 0.1, 'mwp': 0.0},
            {'str': 'chca', 'mwp': 0.1, 'Mc': 2},
            {'str': 'chca', 'mwp': 0.1, 'Mwp': 0.5},
            {'str': 'rse 15', 'mwp': 0.1, 'Mwp': 0.5},
        ],
    },
}


@pytest.fixture
def ai(lib_path: str) -> Ai:
    return Ai(lib_path, json.dumps(schema))


def test_dump_state(ai: Ai):
    state = ai.dump_state()
    assert state == b'{"mode":2,"init_required":true}'


def test_load_state(ai: Ai):
    src = b'{"mode":1,"init_required":false}'
    ai.load_state(src)
    assert ai.dump_state() == src


def test_choose_step(manager: Manager, ai: Ai):
    manager.create_game(['Ab', 'Cd'], 100)
    manager.apply_auto_step()
    manager.apply_auto_step()
    game = manager.dump_game()
    assert json.loads(game)["round"]["state"] == "bet_10_0"
    steps = manager.get_user_steps(1)
    info = manager.get_user_info(1)
    step, reason = ai.choose_step(info, steps)
    assert step
    assert re.match('estimation: .*, mode: .*, strategy: ".*"', reason)
