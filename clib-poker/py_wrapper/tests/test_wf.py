import json

import pytest

from danieldarby.manager import Manager, UserInfo, SharedGameInfo, PlayerInfo
from danieldarby.errors import Errors


@pytest.fixture
def ready_to_first_step(manager: Manager) -> None:
    manager.create_game(['Ab', 'Cd'], 100)
    manager.apply_auto_step()
    manager.apply_auto_step()
    game = manager.dump_game()
    assert json.loads(game)["round"]["state"] == "bet_10_0"


def test_no_game_error(manager: Manager) -> None:
    with pytest.raises(Errors.GameNotSet):
        manager.dump_game()


def test_new_game(manager: Manager) -> None:
    manager.create_game(['Abc', 'k,z', '\\s\\x'], 100)
    game = manager.dump_game()
    js = json.loads(game)
    assert js['players'] == [
        {'name': 'Abc', 'balance': 100},
        {'name': 'k,z', 'balance': 100},
        {'name': '\\s\\x', 'balance': 100},
    ]


def test_load_game(manager: Manager) -> None:
    src = b'{"round":{"shared_cards":"","unused_cards":"C2,C3,C4,C5,C6,C7,C8,C9,C10,CJ,CQ,CK,CA,D2,D3,D4,D5,D6,D7,D8,D9,D10,DJ,DQ,DK,DA,H2,H3,H4,H5,H6,H7,H8,H9,H10,HJ,HQ,HK,HA,S2,S3,S4,S5,S6,S7,S8,S9,S10,SJ,SQ,SK,SA","dealer":0,"current_player":0,"state":"bb","players":[{"bet":0,"cards":"","active":true},{"bet":0,"cards":"","active":true},{"bet":0,"cards":"","active":true}]},"round_number":1,"players":[{"name":"Abc","balance":100},{"name":"k,z","balance":100},{"name":"\\\\s\\\\x","balance":100}]}'
    manager.load_game(src)
    game = manager.dump_game()
    assert game == src


def test_auto_step(manager: Manager) -> None:
    manager.create_game(['Ab', 'Cd'], 100)
    game = manager.dump_game()
    assert json.loads(game)["round"]["state"] == "bb"
    assert manager.apply_auto_step() == 'BigBlind'
    game = manager.dump_game()
    assert json.loads(game)["round"]["state"] == "sb"
    assert manager.apply_auto_step() == 'SmallBlinds'
    game = manager.dump_game()
    assert json.loads(game)["round"]["state"] == "bet_10_0"
    with pytest.raises(Errors.AutoStepNotExpected):
        manager.apply_auto_step()


@pytest.mark.parametrize(
    'err,user,step',
    [
        (None, 1, 'call'),
        (Errors.NotYourStep, 0, 'raise 10'),
        (Errors.InvalidStep, 1, 'check'),
    ]
)
def test_user_step(manager: Manager, ready_to_first_step, err: type, user: int, step: str) -> None:
    if err is None:
        manager.apply_user_step(user, step)
    else:
        with pytest.raises(err):
            manager.apply_user_step(user, step)


def test_get_user_steps(manager: Manager, ready_to_first_step):
    steps = manager.get_user_steps(1)
    assert steps == ['fold', 'call', 'raise 5', 'raise 10', 'raise 15', 'raise 20', 'raise 90']
    assert manager.get_user_steps(0) == []


def test_get_user_info(manager: Manager):
    src = b'{"round": {"shared_cards": "", "unused_cards": "C2,C3,C5,C6,C7,C8,C9,CJ,CQ,CK,CA,D2,D3,D4,D6,D7,D8,D9,D10,DJ,DQ,DK,DA,H2,H3,H4,H6,H7,H8,H9,H10,HJ,HQ,HK,HA,S2,S3,S4,S5,S6,S7,S8,S9,S10,SJ,SQ,SK,SA", "dealer": 0, "current_player": 1, "state": "bet_10_0", "players": [{"bet": 10, "cards": "D5,H5", "active": true}, {"bet": 5, "cards": "C4,C10", "active": true}]}, "round_number": 1, "players": [{"name": "Ab", "balance": 100}, {"name": "Cd", "balance": 100}]}'
    manager.load_game(src)
    info = manager.get_user_info(0)
    expected = UserInfo(
        id=0,
        shared_info=SharedGameInfo(
            players=[
                PlayerInfo(name='Ab', has_cards=True, balance=100, bet=10),
                PlayerInfo(name='Cd', has_cards=True, balance=100, bet=5)
            ],
            cards=[],
            bank=15,
            dealer=0,
        ),
        cards=['♦5', '♥5']
    )
    assert info == expected
    info = manager.get_user_info(1)
    expected.id = 1
    expected.cards = ['♣4', '♣10']
    assert info == expected


def test_get_shared_info(manager: Manager):
    src = b'{"round": {"shared_cards": "", "unused_cards": "C2,C3,C5,C6,C7,C8,C9,CJ,CQ,CK,CA,D2,D3,D4,D6,D7,D8,D9,D10,DJ,DQ,DK,DA,H2,H3,H4,H6,H7,H8,H9,H10,HJ,HQ,HK,HA,S2,S3,S4,S5,S6,S7,S8,S9,S10,SJ,SQ,SK,SA", "dealer": 0, "current_player": 1, "state": "bet_10_0", "players": [{"bet": 10, "cards": "D5,H5", "active": true}, {"bet": 5, "cards": "C4,C10", "active": true}]}, "round_number": 1, "players": [{"name": "Ab", "balance": 100}, {"name": "Cd", "balance": 100}]}'
    manager.load_game(src)
    info = manager.get_shared_info()
    expected = SharedGameInfo(
        players=[
            PlayerInfo(name='Ab', has_cards=True, balance=100, bet=10),
            PlayerInfo(name='Cd', has_cards=True, balance=100, bet=5)
        ],
        cards=[],
        bank=15,
        dealer=0,
    )
    assert info == expected
    card_map = manager.get_all_cards()
    expected = {
        0: ["D5", "H5"],
        1: ["C4", "C10"],
    }


def test_get_round_winner(manager: Manager, ready_to_first_step):
    assert manager.get_round_winner() == None
    manager.apply_user_step(1, 'fold')
    assert manager.get_round_winner() == 0


def test_get_expected_action_us(manager: Manager, ready_to_first_step):
    action, user = manager.get_expected_action()
    assert user == 1
    assert action == manager.USER_STEP


def test_get_expected_action_as(manager: Manager):
    manager.create_game(['a', 'b'], 100)
    action, user = manager.get_expected_action()
    assert user is None
    assert action == manager.AUTO_STEP


def test_next_round_err(manager: Manager):
    manager.create_game(['a', 'b'], 100)
    with pytest.raises(Errors.NotReady):
        manager.next_round()


def test_next_round_ok(manager: Manager, ready_to_first_step):
    manager.apply_user_step(1, 'fold')
    manager.next_round()
