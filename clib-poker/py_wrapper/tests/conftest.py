import os

import pytest

from danieldarby.manager import Manager


@pytest.fixture
def lib_path() -> str:
    return os.environ['DD_LIB_PATH']


@pytest.fixture
def manager(lib_path: str) -> Manager:
    return Manager(lib_path)
