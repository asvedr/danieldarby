from functools import lru_cache


class DDError(Exception):
    CODE: int


class Errors:
    class GameNotSet(DDError):
        CODE = 1

    class DecodeUTF8(DDError):
        CODE = 2

    class TooSmallPlayers(DDError):
        CODE = 3

    class InvalidStep(DDError):
        CODE = 4

    class NotYourStep(DDError):
        CODE = 5

    class AutoStepNotExpected(DDError):
        CODE = 6

    class InvalidJson(DDError):
        CODE = 7

    class InvalidJsonField(DDError):
        CODE = 8

    class JsonFieldNotFound(DDError):
        CODE = 9

    class JsonFieldNotObject(DDError):
        CODE = 10

    class InvalidStrategy(DDError):
        CODE = 11

    class NotReady(DDError):
        CODE = -1

    @classmethod
    @lru_cache(maxsize=1)
    def get_map(cls) -> dict[int, type]:
        return {
            attr.CODE: attr
            for attr in map(lambda key: getattr(cls, key), dir(cls))
            if isinstance(attr, type) and DDError in attr.__bases__
        }

    @classmethod
    def proc_code(cls, code: int) -> None:
        if code == 0:
            return
        raise cls.get_map()[code]()
