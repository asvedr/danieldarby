import ctypes
from ctypes import CDLL
from functools import lru_cache

CODE_BEGIN = '> '


@lru_cache(maxsize=16)
def load_lib(path: str) -> CDLL:
    lib = CDLL(path)
    lib.py_info.restype = ctypes.c_char_p
    info = lib.py_info().decode()
    for line in info.split('\n'):
        if not line.startswith(CODE_BEGIN):
            continue
        func_data = line[len(CODE_BEGIN):]
        try:
            func_name, rest = func_data.split(':')
        except Exception:
            import pdb; pdb.set_trace()
            pass
        func = getattr(lib, func_name)
        params, result = rest.split('->')
        result = result.strip()
        if result != '_':
            func.restype = getattr(ctypes, result)
        func.argtypes = [
            getattr(ctypes, param.strip())
            for param in params.split(',')
        ]
    return lib
