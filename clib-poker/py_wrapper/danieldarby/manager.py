from ctypes import c_uint, byref
from dataclasses import dataclass, asdict
import json
from typing import Optional

from .base_lib import load_lib
from .errors import Errors


@dataclass
class PlayerInfo:
    name: str
    has_cards: bool
    balance: int
    bet: int

def _parse_cards(src: str) -> list[str]:
    if src:
        return src.split(',')
    else:
        return []


def _dump_cards(cards: list[str]) -> str:
    if not cards:
        return ''
    else:
        return ','.join(cards)


@dataclass
class SharedGameInfo:
    players: list[PlayerInfo]
    cards: list[str]
    bank: int
    dealer: int

    @classmethod
    def from_dict(cls, js: dict) -> 'SharedGameInfo':
        return cls(
            players=[PlayerInfo(**p) for p in js['players']],
            cards=_parse_cards(js['cards']),
            bank=js['bank'],
            dealer=js['dealer'],
        )

    def to_dict(self) -> dict:
        return {
            'players': [asdict(p) for p in self.players],
            'cards': _dump_cards(self.cards),
            'bank': self.bank,
            'dealer': self.dealer,
        }


@dataclass
class UserInfo:
    id: int
    shared_info: SharedGameInfo
    cards: list[str]

    @classmethod
    def from_dict(cls, js: dict) -> 'UserInfo':
        return cls(
            id=js['id'],
            shared_info=SharedGameInfo.from_dict(js['shared_info']),
            cards=_parse_cards(js['cards']),
        )

    def to_dict(self) -> dict:
        return {
            'id': self.id,
            'shared_info': self.shared_info.to_dict(),
            'cards': _dump_cards(self.cards),
        }


class Manager:
    NOTHING: int = 0
    AUTO_STEP: int = 1
    USER_STEP: int = 2

    OK: int = 0
    WINNER: int = 1
    NOT_READY: int = 2

    def __init__(self, path: str, use_ascii: bool = False):
        self._manager = None
        self.lib = load_lib(path)
        self._manager = self.lib.m_create_manager(int(use_ascii))

    def __del__(self):
        if self._manager:
            self.lib.m_drop(self._manager)

    def create_game(self, players: list[str], balance: int) -> None:
        players = ','.join(
            player.replace('\\', '\\s').replace(',', '\\c')
            for player in players
        ).encode()
        res = self.lib.m_create_game(self._manager, players, balance)
        Errors.proc_code(res)

    def load_game(self, game: bytes) -> None:
        Errors.proc_code(self.lib.m_load_game(self._manager, game))

    def dump_game(self) -> bytes:
        Errors.proc_code(self.lib.m_dump_game(self._manager))
        return self._fetch()

    def apply_auto_step(self) -> str:
        Errors.proc_code(self.lib.m_apply_auto_step(self._manager))
        return self._fetch().decode()

    def get_user_info(self, user: int) -> UserInfo:
        code = self.lib.m_get_user_info(self._manager, user)
        Errors.proc_code(code)
        js = json.loads(self._fetch())
        assert js['id'] == user
        return UserInfo.from_dict(js)

    def get_shared_info(self) -> SharedGameInfo:
        Errors.proc_code(self.lib.m_get_shared_info(self._manager))
        js = json.loads(self._fetch())
        return SharedGameInfo.from_dict(js)

    def get_user_steps(self, user: int) -> list[str]:
        Errors.proc_code(self.lib.m_get_steps(self._manager, user))
        return json.loads(self._fetch())

    def apply_user_step(self, user: int, step: str) -> None:
        code = self.lib.m_apply_user_step(
            self._manager,
            user,
            step.encode(),
        )
        Errors.proc_code(code)

    def get_round_winner(self) -> Optional[int]:
        is_some = c_uint()
        value = c_uint()
        code = self.lib.m_get_round_winner(
            self._manager, byref(is_some), byref(value),
        )
        Errors.proc_code(code)
        if bool(is_some.value):
            return value.value
        return None

    def get_expected_action(self) -> tuple[int, Optional[int]]:
        type_code = c_uint()
        user = c_uint()
        code = self.lib.m_get_expected_action(
            self._manager,
            byref(type_code),
            byref(user),
        )
        Errors.proc_code(code)
        if type_code.value == self.USER_STEP:
            return type_code.value, user.value
        return type_code.value, None

    def user_step_required(self) -> bool:
        if self.get_round_winner() is not None:
            return True
        tp, _ = self.get_expected_action()
        return tp == self.AUTO_STEP

    def next_round(self) -> Optional[int]:
        status = c_uint()
        user = c_uint()
        code = self.lib.m_next_round(
            self._manager,
            byref(status),
            byref(user),
        )
        Errors.proc_code(code)
        if status.value == self.WINNER:
            return user.value
        if status.value == self.OK:
            return None
        raise Errors.NotReady()

    def get_all_cards(self) -> dict[int, list[str]]:
        Errors.proc_code(self.lib.m_get_all_cards(self._manager))
        data = json.loads(self._fetch())
        return {
            int(k): _parse_cards(v)
            for k, v in data.items()
        }

    def _fetch(self) -> bytes:
        return self.lib.m_fetch(self._manager)
