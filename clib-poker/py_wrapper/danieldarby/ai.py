from ctypes import c_uint, byref, c_char_p
from dataclasses import dataclass
import json

from .base_lib import load_lib
from .errors import Errors
from .manager import UserInfo


class Ai:
    def __init__(self, path: str, schema: str):
        self.lib = load_lib(path)
        self._ai = None
        ai = c_char_p()
        code = self.lib.ai_create(schema.encode(), byref(ai))
        Errors.proc_code(code)
        self._ai = ai

    def __del__(self):
        if self._ai:
            self.lib.ai_drop(self._ai)

    def load_state(self, state: bytes) -> None:
        code = self.lib.ai_load_state(self._ai, state)
        Errors.proc_code(code)

    def dump_state(self) -> bytes:
        return self.lib.ai_dump_state(self._ai)

    def choose_step(
        self,
        user_info: UserInfo,
        steps: list[str],
    ) -> tuple[str, str]:
        step = c_uint()
        details = c_char_p()
        user_info = user_info.to_dict()
        code = self.lib.ai_choose_step(
            self._ai,
            json.dumps(user_info).encode(),
            json.dumps(steps).encode(),
            byref(step),
            byref(details),
        )
        Errors.proc_code(code)
        return steps[step.value], details.value.decode()
