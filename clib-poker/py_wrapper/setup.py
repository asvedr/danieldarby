#!/usr/bin/env python

from setuptools import setup, find_packages

setup(
    name='danieldarby',
    version='1.0',
    description='Wrapper for danieldarby rust package',
    packages=find_packages(exclude=["*.tests", "*.tests.*", "tests.*", "tests"]),
)
