use std::os::raw::c_uint;

use poker::errors::{GameError, SerializerError};

pub struct Code {}

impl Code {
    pub const OK: c_uint = 0;
    pub const GAME_NOT_SET: c_uint = 1;
    pub const DECODE_UTF8: c_uint = 2;
    pub const TOO_SMALL_PLAYERS: c_uint = 3;
    pub const INVALID_STEP: c_uint = 4;
    pub const NOT_YOUR_STEP: c_uint = 5;
    pub const AUTO_STEP_NOT_EXPECTED: c_uint = 6;
    pub const INVALID_JSON: c_uint = 7;
    pub const INVALID_JSON_FIELD: c_uint = 8;
    pub const JSON_FIELD_NOT_FOUND: c_uint = 9;
    pub const JSON_NOT_AN_OBJECT: c_uint = 10;
    pub const INVALID_STRATEGY: c_uint = 11;
}

pub trait GetCode {
    fn get_code(&self) -> c_uint;
}

impl GetCode for std::str::Utf8Error {
    fn get_code(&self) -> c_uint {
        Code::DECODE_UTF8
    }
}

impl GetCode for GameError {
    fn get_code(&self) -> c_uint {
        match self {
            GameError::TooSmallPlayers => Code::TOO_SMALL_PLAYERS,
            GameError::InvalidStep => Code::INVALID_STEP,
            GameError::NotYourStep => Code::NOT_YOUR_STEP,
            GameError::AutomaticStepNotExpected => Code::AUTO_STEP_NOT_EXPECTED,
        }
    }
}

impl GetCode for SerializerError {
    fn get_code(&self) -> c_uint {
        match self {
            SerializerError::InvalidStep(_) => Code::INVALID_STEP,
            SerializerError::InvalidJson(_) => Code::INVALID_JSON,
            SerializerError::InvalidField(_) => Code::INVALID_JSON_FIELD,
            SerializerError::FieldNotFound(_) => Code::JSON_FIELD_NOT_FOUND,
            SerializerError::TopLevelNotAnObject => Code::JSON_NOT_AN_OBJECT,
            SerializerError::InvalidStrategy(_) => Code::INVALID_STRATEGY,
        }
    }
}

#[macro_export]
macro_rules! ret_err {
    ($ext_obj:expr, $val:expr, $code:expr) => {{
        $ext_obj.buffer = CString::new(format!("{:?}", $val)).unwrap();
        return $code;
    }};
}

#[macro_export]
macro_rules! unwrap_or_ret {
    ($val:expr, $ext_obj:expr) => {
        match $val {
            Ok(val) => val,
            Err(err) => {
                let code = err.get_code();
                crate::ret_err!($ext_obj, err, code)
            }
        }
    };
    ($val:expr, $ext_obj:expr, $code:expr) => {
        match $val {
            Some(val) => val,
            _ => crate::ret_err!($ext_obj, "", $code),
        }
    };
}
