mod ai;
mod expected_action_code;
mod ext_object;
mod game_manager;
mod next_round_code;
mod return_code;

use std::os::raw::c_char;

pub use ai::*;
pub use game_manager::*;

const DATA: &str = concat!(
    "> ai_choose_step: c_void_p, c_char_p, c_char_p, c_void_p, c_void_p -> c_uint\n",
    "> ai_create: c_char_p, c_void_p -> c_uint\n",
    "> ai_drop: c_void_p -> _\n",
    "> ai_dump_state: c_void_p -> c_char_p\n",
    "> ai_load_state: c_void_p, c_char_p -> c_uint\n",
    "> m_apply_auto_step: c_void_p -> c_uint\n",
    "> m_apply_user_step: c_void_p, c_uint, c_char_p -> c_uint\n",
    "> m_create_game: c_void_p, c_char_p, c_uint -> c_uint\n",
    "> m_create_manager: c_uint -> c_void_p\n",
    "> m_drop: c_void_p -> _\n",
    "> m_dump_game: c_void_p -> c_uint\n",
    "> m_fetch: c_void_p -> c_char_p\n",
    "> m_get_all_cards: c_void_p -> c_uint\n",
    "> m_get_expected_action: c_void_p, c_void_p, c_void_p -> c_uint\n",
    "> m_get_round_winner: c_void_p, c_void_p, c_void_p -> c_uint\n",
    "> m_get_shared_info: c_void_p -> c_uint\n",
    "> m_get_steps: c_void_p, c_uint -> c_uint\n",
    "> m_get_user_info: c_void_p, c_uint -> c_uint\n",
    "> m_load_game: c_void_p, c_char_p -> c_uint\n",
    "> m_next_round: c_void_p, c_void_p, c_void_p -> c_uint\n",
    "\0",
);

#[no_mangle]
pub extern "C" fn py_info() -> *const u8 {
    &DATA.as_bytes()[0]
}
