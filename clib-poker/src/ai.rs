use poker::{AiState, IAi, IAiDeserializer, IGameDeserializer};
use std::ffi::{CStr, CString};
use std::os::raw::{c_char, c_uint};

use crate::ext_object::{drop_ptr, ptr2link, ExtObject};
use crate::return_code::{Code, GetCode};
use crate::unwrap_or_ret;

pub struct Ai {
    ai: Box<dyn IAi>,
    state: AiState,
    game_de: Box<dyn IGameDeserializer>,
    ai_ser: Box<dyn IAiDeserializer>,
}

#[no_mangle]
pub unsafe extern "C" fn ai_create(schema: *const c_char, out: *mut *mut ExtObject<Ai>) -> c_uint {
    let src = match CStr::from_ptr(schema).to_str() {
        Ok(val) => val,
        _ => return Code::DECODE_UTF8,
    };
    let ai_ser = poker::create_ai_deserializer();
    let iai = match ai_ser.json_to_ai(src) {
        Ok(val) => val,
        Err(err) => return err.get_code(),
    };
    let ai = Ai {
        ai: iai,
        state: AiState::default(),
        game_de: poker::create_game_deserializer(),
        ai_ser,
    };
    *out = ExtObject::new_ptr(ai);
    Code::OK
}

#[no_mangle]
pub unsafe extern "C" fn ai_drop(ptr: *mut ExtObject<Ai>) {
    drop_ptr(ptr)
}

#[no_mangle]
pub unsafe extern "C" fn ai_load_state(ptr: *mut ExtObject<Ai>, state: *const c_char) -> c_uint {
    let ai = ptr2link(ptr);
    let text = unwrap_or_ret!(CStr::from_ptr(state).to_str(), ai);
    let state_res = ai.data.ai_ser.str_to_ai_state(text);
    let state = unwrap_or_ret!(state_res, ai);
    ai.data.state = state;
    Code::OK
}

#[no_mangle]
pub unsafe extern "C" fn ai_dump_state(ptr: *mut ExtObject<Ai>) -> *const c_char {
    let ai = ptr2link(ptr);
    let text = ai.data.ai_ser.ai_state_to_str(&ai.data.state);
    ai.buffer = CString::new(text).unwrap();
    ai.buffer.as_ptr()
}

fn get_step_index(steps: &[poker::Step], step: &poker::Step) -> usize {
    for i in 0..steps.len() {
        if steps[i] == *step {
            return i;
        }
    }
    unreachable!()
}

#[no_mangle]
pub unsafe extern "C" fn ai_choose_step(
    ptr: *mut ExtObject<Ai>,
    user_info: *const c_char,
    steps: *const c_char,
    out: *mut c_uint,
    details: *mut *const c_char,
) -> c_uint {
    let ai = ptr2link(ptr);
    let text = unwrap_or_ret!(CStr::from_ptr(steps).to_str(), ai);
    let steps_res = ai.data.game_de.str_to_steps(text);
    let steps = unwrap_or_ret!(steps_res, ai);
    let text = unwrap_or_ret!(CStr::from_ptr(user_info).to_str(), ai);
    let info_res = ai.data.game_de.str_to_user_info(text);
    let info = unwrap_or_ret!(info_res, ai);
    let chosen = ai.data.ai.choose_step(&info, &mut ai.data.state, &steps);
    *out = get_step_index(&steps, &chosen.step) as c_uint;
    ai.buffer = CString::new(chosen.info).unwrap();
    *details = ai.buffer.as_ptr();
    Code::OK
}
