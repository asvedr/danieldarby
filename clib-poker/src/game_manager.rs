use crate::expected_action_code::ExpectedActionCode;
use poker::{
    ExpectedAction, GameState, IGame, IGameDeserializer, IGameSerializer, NextRoundResponse,
};
use std::ffi::{CStr, CString};
use std::os::raw::{c_char, c_uint};

use crate::ext_object::{drop_ptr, ptr2link, ExtObject};
use crate::next_round_code::NextRoundCode;
use crate::return_code::{Code, GetCode};
use crate::unwrap_or_ret;

const COMMA: &str = "\\c";
const SLASH: &str = "\\s";

pub struct Man {
    manager: Box<dyn IGame>,
    game: Option<GameState>,
    ser: Box<dyn IGameSerializer>,
    de: Box<dyn IGameDeserializer>,
}

#[no_mangle]
pub unsafe extern "C" fn m_create_manager(use_ascii: c_uint) -> *mut ExtObject<Man> {
    let use_ascii = use_ascii != 0;
    ExtObject::new_ptr(Man {
        manager: poker::game_manager(),
        game: None,
        ser: poker::create_game_serializer(use_ascii),
        de: poker::create_game_deserializer(),
    })
}

#[no_mangle]
pub unsafe extern "C" fn m_drop(ptr: *mut ExtObject<Man>) {
    drop_ptr(ptr)
}

#[no_mangle]
pub unsafe extern "C" fn m_fetch(ptr: *mut ExtObject<Man>) -> *const c_char {
    let man = ptr2link(ptr);
    man.buffer.as_ptr()
}

#[no_mangle]
pub unsafe extern "C" fn m_create_game(
    ptr: *mut ExtObject<Man>,
    players: *const c_char,
    balance: c_uint,
) -> c_uint {
    let man = ptr2link(ptr);
    let players_str: &str = unwrap_or_ret!(CStr::from_ptr(players).to_str(), man);
    let players = players_str
        .split(',')
        .map(|player| player.replace(COMMA, ",").replace(SLASH, "\\"))
        .collect::<Vec<_>>();
    let game = unwrap_or_ret!(man.data.manager.new_game(players, balance as usize), man);
    man.data.game = Some(game);
    Code::OK
}

#[no_mangle]
pub unsafe extern "C" fn m_load_game(ptr: *mut ExtObject<Man>, src: *const c_char) -> c_uint {
    let man = ptr2link(ptr);
    let text = unwrap_or_ret!(CStr::from_ptr(src).to_str(), man);
    let game_res = man.data.de.str_to_state(text);
    let game = unwrap_or_ret!(game_res, man);
    man.data.game = Some(game);
    Code::OK
}

#[no_mangle]
pub unsafe extern "C" fn m_dump_game(ptr: *mut ExtObject<Man>) -> c_uint {
    let man = ptr2link(ptr);
    let game = unwrap_or_ret!(man.data.game.as_ref(), man, Code::GAME_NOT_SET);
    let serialized = man.data.ser.state_to_str(game);
    man.buffer = CString::new(serialized).unwrap();
    Code::OK
}

#[no_mangle]
pub unsafe extern "C" fn m_get_round_winner(
    ptr: *mut ExtObject<Man>,
    is_some: *mut c_uint,
    value: *mut c_uint,
) -> c_uint {
    let man = ptr2link(ptr);
    let game = unwrap_or_ret!(man.data.game.as_ref(), man, Code::GAME_NOT_SET);
    if let Some(winner) = man.data.manager.get_round_winner(game) {
        *is_some = 1;
        *value = winner as c_uint;
    } else {
        *is_some = 0;
    }
    Code::OK
}

#[no_mangle]
pub unsafe extern "C" fn m_next_round(
    ptr: *mut ExtObject<Man>,
    status: *mut c_uint,
    user: *mut c_uint,
) -> c_uint {
    let man = ptr2link(ptr);
    let game = unwrap_or_ret!(man.data.game.as_ref(), man, Code::GAME_NOT_SET);
    *status = match man.data.manager.next_round(game) {
        NextRoundResponse::Round(state) => {
            man.data.game = Some(state);
            NextRoundCode::OK
        }
        NextRoundResponse::Winner(winner) => {
            *user = winner as c_uint;
            NextRoundCode::WINNER
        }
        NextRoundResponse::NotReady => NextRoundCode::NOT_READY,
    };
    Code::OK
}

#[no_mangle]
pub unsafe extern "C" fn m_get_steps(ptr: *mut ExtObject<Man>, player: c_uint) -> c_uint {
    let man = ptr2link(ptr);
    let game = unwrap_or_ret!(man.data.game.as_ref(), man, Code::GAME_NOT_SET);
    let steps = man.data.manager.get_steps(game, player as usize);
    let resp = man.data.ser.steps_to_str(&steps);
    man.buffer = CString::new(resp).unwrap();
    Code::OK
}

#[no_mangle]
pub unsafe extern "C" fn m_get_expected_action(
    ptr: *mut ExtObject<Man>,
    type_code: *mut c_uint,
    user: *mut c_uint,
) -> c_uint {
    let man = ptr2link(ptr);
    let game = unwrap_or_ret!(man.data.game.as_ref(), man, Code::GAME_NOT_SET);
    let action = man.data.manager.get_expected_action(game);
    match action {
        ExpectedAction::Nothing => {
            *type_code = ExpectedActionCode::NOTHING;
        }
        ExpectedAction::AutoStep(step) => {
            *type_code = ExpectedActionCode::AUTO_STEP;
            man.buffer = CString::new(format!("{:?}", step)).unwrap();
        }
        ExpectedAction::UserStep(id) => {
            *type_code = ExpectedActionCode::USER_STEP;
            *user = id as c_uint;
        }
    }
    Code::OK
}

#[no_mangle]
pub unsafe extern "C" fn m_get_shared_info(ptr: *mut ExtObject<Man>) -> c_uint {
    let man = ptr2link(ptr);
    let game = unwrap_or_ret!(man.data.game.as_ref(), man, Code::GAME_NOT_SET);
    let info = man.data.manager.get_shared_info(game);
    let text = man.data.ser.shared_info_to_str(&info);
    man.buffer = CString::new(text).unwrap();
    Code::OK
}

#[no_mangle]
pub unsafe extern "C" fn m_get_user_info(ptr: *mut ExtObject<Man>, player: c_uint) -> c_uint {
    let man = ptr2link(ptr);
    let game = unwrap_or_ret!(man.data.game.as_ref(), man, Code::GAME_NOT_SET);
    let info = man.data.manager.get_user_info(game, player as usize);
    let text = man.data.ser.user_info_to_str(info);
    man.buffer = CString::new(text).unwrap();
    Code::OK
}

#[no_mangle]
pub unsafe extern "C" fn m_apply_auto_step(ptr: *mut ExtObject<Man>) -> c_uint {
    let man = ptr2link(ptr);
    let game = unwrap_or_ret!(man.data.game.as_ref(), man, Code::GAME_NOT_SET);
    let act = man.data.manager.get_expected_action(game);
    let new_state_res = man.data.manager.apply_auto_step(game);
    let new_state = unwrap_or_ret!(new_state_res, man);
    man.data.game = Some(new_state);
    let msg = match act {
        ExpectedAction::AutoStep(val) => format!("{:?}", val),
        _ => panic!(),
    };
    man.buffer = CString::new(msg.split('{').next().unwrap()).unwrap();
    Code::OK
}

#[no_mangle]
pub unsafe extern "C" fn m_apply_user_step(
    ptr: *mut ExtObject<Man>,
    player: c_uint,
    step: *const c_char,
) -> c_uint {
    let man = ptr2link(ptr);
    let game = unwrap_or_ret!(man.data.game.as_ref(), man, Code::GAME_NOT_SET);
    let step = unwrap_or_ret!(CStr::from_ptr(step).to_str(), man);
    let step_res = man.data.de.str_to_step(step);
    let step = unwrap_or_ret!(step_res, man);
    let new_state_res = man
        .data
        .manager
        .apply_user_step(game, player as usize, step);
    let new_state = unwrap_or_ret!(new_state_res, man);
    man.data.game = Some(new_state);
    Code::OK
}

#[no_mangle]
pub unsafe extern "C" fn m_get_all_cards(
    ptr: *mut ExtObject<Man>
) -> c_uint {
    let man = ptr2link(ptr);
    let game = unwrap_or_ret!(man.data.game.as_ref(), man, Code::GAME_NOT_SET);
    let text = man.data.ser.active_user_cards_to_str(game);
    man.buffer = CString::new(text).unwrap();
    Code::OK
}
