use std::ffi::CString;
use std::mem::transmute;

pub struct ExtObject<T> {
    pub(crate) data: T,
    pub(crate) buffer: CString,
}

pub unsafe fn box2ptr<T>(src: Box<T>) -> *mut T {
    transmute(src)
}

pub unsafe fn ptr2box<T>(src: *mut T) -> Box<T> {
    transmute(src)
}

pub unsafe fn ptr2link<T>(src: *mut T) -> &'static mut T {
    transmute(src)
}

pub unsafe fn drop_ptr<T>(src: *mut T) {
    let _ = ptr2box(src);
}

impl<T> ExtObject<T> {
    pub unsafe fn new_ptr(data: T) -> *mut Self {
        let val = Self {
            data,
            buffer: CString::new("").unwrap(),
        };
        box2ptr(Box::new(val))
    }
}
