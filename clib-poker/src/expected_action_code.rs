use std::os::raw::c_uint;

pub struct ExpectedActionCode {}

impl ExpectedActionCode {
    pub const NOTHING: c_uint = 0;
    pub const AUTO_STEP: c_uint = 1;
    pub const USER_STEP: c_uint = 2;
}
