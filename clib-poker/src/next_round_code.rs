use std::os::raw::c_uint;

pub struct NextRoundCode {}

impl NextRoundCode {
    pub const OK: c_uint = 0;
    pub const WINNER: c_uint = 1;
    pub const NOT_READY: c_uint = 2;
}
