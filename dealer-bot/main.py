import asyncio
import os
from typing import Optional

from aiogram import Bot, Dispatcher, types
from aiogram.utils.executor import Executor

from darby_bot.entities.constants import RobotName
from darby_bot.entities.settings import settings
from darby_bot.impls.ext_tcp_bus import ExtTcpBus
from darby_bot.impls.int_list_bus import IntListBus
from darby_bot.proto.bot import ICardBotFactory


factories: dict[str, ICardBotFactory] = {}


# async def async_main():
    # bot = Bot(token=API_TOKEN)
    # dp = Dispatcher(bot)

    # @dp.message_handler(commands=['ping'])
    # async def react(message: types.Message):
    #     await message.reply("put into queue")
    #     queue_output.push((message.chat.id, message.message_id, False, 'pong'))
    #     # loop.create_task(resp())
    #
    # @dp.message_handler(commands=['pic'])
    # async def react(message: types.Message):
    #     # print(message.text)
    #     await message.reply("processing...")
    #     text = message.text[len('/pic'):]
    #     queue_input.push((message.chat.id, message.message_id, text))
    #     # loop.create_task(resp())

    # loop.create_task(resp())
    # loop.create_task(worker(config, queue_input, queue_output))


def main():
    int_bus = IntListBus(settings.robots)
    ext_bus = ExtTcpBus(settings.external_robots)
    robot_map = {name: int_bus for name in settings.robots}
    for robot, _ in settings.external_robots:
        robot_map[robot] = ext_bus
    loop = asyncio.get_event_loop()
    tasks = []
    for robot in settings.robots:
        factory = factories[robot]
        bot = factory.produce(robot_map)
        tasks.append(loop.create_task(bot.run_with_restarts_on_exc()))
    loop.run_until_complete(asyncio.gather(*tasks))


main()
