from typing import Optional

from darby_bot.entities.constants import RobotName
from darby_bot.entities.messages import RobotMessage
from darby_bot.proto.bot import IBus


class IntListBus(IBus):
    """
        This class works in asyncio mode. So real locks are not required
    """
    def __init__(self, names: list[RobotName]):
        self.queues = {
            name: []
            for name in names
        }

    async def send(self, name: RobotName, msg: RobotMessage) -> None:
        self.queues[name].append(msg)

    def peek(self, name: RobotName) -> Optional[RobotMessage]:
        queue = self.queues[name]
        if queue:
            return queue[0]

    def pop(self, name: RobotName) -> None:
        queue = self.queues[name]
        if queue:
            del queue[0]
