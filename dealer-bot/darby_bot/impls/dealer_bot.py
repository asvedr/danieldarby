import asyncio
from dataclasses import dataclass
from typing import Optional, Coroutine, Union

from aiogram import Bot, Dispatcher
from danieldarby.errors import Errors
from danieldarby.manager import Manager

from darby_bot.entities.constants import RobotName
from darby_bot.entities.messages import RobotMakeStep
from darby_bot.entities.settings import settings
from darby_bot.entities.states import ChatState, Expected
from darby_bot.proto.bot import ICardBot, IStateBase, IDealerCommand, IAi
from darby_bot.utils.tg_utils import start_polling


@dataclass
class AutoStepResults:
    state: ChatState
    applied: list[str]
    winner: Optional[int]


@dataclass
class NextRoundResult:
    state: ChatState
    winner: Optional[int]
    loosers: list[int]


class TgBot(ICardBot):

    START_GAME = 'new_game'
    INIT_BALANCE = 100

    def __init__(self, base: IStateBase, commands: list[IDealerCommand], ai_list: list[IAi]):
        self.bot: Optional[Bot] = None
        self.token = settings.dealer_token
        self.sleep = settings.sleep
        self.tasks: list[asyncio.Task] = []
        self.base = base
        self.rs_manager = Manager(settings.lib_path)
        self.commands = commands
        self.ai = {ai.name: ai for ai in ai_list}
        for cmd in commands:
            cmd.rs_manager = self.rs_manager

    async def run(self) -> None:
        bot = Bot(token=self.token)
        self.bot = bot
        dp = Dispatcher(bot)

        for cmd in self.commands:
            cmd.bot = bot
            dp.register_message_handler(cmd.apply_wrapped, commands=cmd.commands)

        await asyncio.gather(
            asyncio.create_task(start_polling(
                dispatcher=dp,
                skip_updates=False,
                allowed_updates=['text'],
            )),
            asyncio.create_task(self.do_auto_steps()),
        )

    async def stop(self) -> None:
        self.bot = None
        for task in self.tasks:
            task.cancel()

    def robot_step(self, robot: RobotName, chat_id: str, action: RobotMakeStep) -> None:
        try:
            state = self.base.get_chat_state(chat_id)
        except self.base.StateNotFound:
            # state already destroyed
            return
        index = state.players.index(robot)
        if index < 0:
            return
        self.rs_manager.load_game(state.game_state)
        try:
            self.rs_manager.apply_user_step(index, action.step)
        except (Errors.NotYourStep, Errors.InvalidStep):
            return
        state.update(self.rs_manager)
        self.base.set_chat_state(state)

    async def do_auto_steps(self) -> None:
        while True:
            await asyncio.sleep(settings.sleep)
            state = self.base.get_state_for_autosteps()
            if not state:
                continue
            if state.expected == Expected.autostep:
                await self.do_auto_step(state)
            elif state.expected == Expected.ai:
                await self.do_ai_step(state)
            elif state.expected == Expected.win_notificaton:
                await self.do_new_round(state) # or notify win
            else:
                raise Exception(f"invalud state found in autosteps: {state}")

    async def do_auto_step(self, state: ChatState) -> None:
        self.rs_manager.load_game(state.game_state)
        applied = self.rs_manager.apply_auto_step()
        state.update(self.rs_manager)
        tp, user = self.rs_manager.get_expected_action()
        if applied == 'BigBlind':
            await self.show_init_info(state)
        if applied in ['SmallBlinds', 'NewCard', 'OpenAll']:
            await self.show_info(state)
        if applied == 'SmallBlinds':
            await self.notify_user_about_cards(state)
        if tp == self.rs_manager.USER_STEP and isinstance(state.players[user], str):
            await self.notify_user(state, state.players[user])
        if applied == 'CheckAll':
            await self.show_all_cards(state)
        self.base.set_chat_state(state)

    async def notify_user_about_cards(self, state: ChatState) -> None:
        self.rs_manager.load_game(state.game_state)
        for id, player in enumerate(state.players):
            if isinstance(player, RobotName):
                continue
            user = self.base.get_user(player)
            cards = self.rs_manager.get_user_info(id).cards
            await self.bot.send_message(user.tg_id, f'Your cards: {cards}')

    async def show_init_info(self, state: ChatState) -> None:
        self.rs_manager.load_game(state.game_state)
        info = self.rs_manager.get_shared_info()
        players = [
            f'- {self.get_user_name(state.players[id])}, balance={player.balance}'
            for id, player in enumerate(info.players)
            if player.balance > 0
        ]
        loosers = ','.join(
            self.get_user_name(state.players[id])
            for id, player in enumerate(info.players)
            if player.balance == 0
        )
        msg = (
            f'dealer: {self.get_user_name(state.players[info.dealer])}\n'
            f'active players:\n{players}\n'
            f'out of balance: {loosers}'
        )
        await self.bot.send_message(state.chat_id, msg)

    async def do_ai_step(self, state: ChatState) -> None:
        self.rs_manager.load_game(state.game_state)
        tp, id = self.rs_manager.get_expected_action()
        assert tp == self.rs_manager.USER_STEP
        player = state.players[id]
        assert isinstance(player, RobotName)
        ai: IAi = self.ai[player]
        step = ai.choose_step(
            self.rs_manager.get_user_info(id),
            self.rs_manager.get_user_steps(id)
        )
        self.rs_manager.apply_user_step(id, step)
        state.update(self.rs_manager)
        self.base.set_chat_state(state)
        await ai.send_msg(step)

    async def do_new_round(self, state: ChatState) -> None:
        self.rs_manager.load_game(state.game_state)
        winner = self.rs_manager.next_round()
        if winner is not None:
            await self.process_win(state, winner)
            return
        state.update(self.rs_manager)
        self.base.set_chat_state(state)
        await self.bot.send_message(state.chat_id, 'New round')

    async def show_all_cards(self, state: ChatState) -> None:
        self.rs_manager.load_game(state.game_state)
        msg = '\n'.join(
            f'{self.get_user_name(state.players[id])}: {", ".join(cards)}'
            for id, cards in self.rs_manager.get_all_cards().items()
        )
        await self.bot.send_message(state.chat_id, msg)

    def get_user_name(self, key: Union[str, RobotName]) -> str:
        if isinstance(key, RobotName):
            return key.value
        user = self.base.get_user(key)
        msg = user.name
        if user.login:
            msg += f'({user.login})'
        return msg

    async def show_info(self, state: ChatState) -> None:
        self.rs_manager.load_game(state.game_state)
        info = self.rs_manager.get_shared_info()
        cards = ', '.join(info.cards) if info.cards else 'nothing'
        message = f'cards on table: {cards}\nbank: {info.bank}'
        await self.bot.send_message(state.chat_id, message)
        players = '\n'.join(
            f'- {self.get_user_name(state.players[player])}, balance={player.balance}, bet={player.bet}'
            for id, player in info.players
            if player.has_cards
        )
        await self.bot.send_message(state.chat_id, f'active players: {players}')

    async def notify_user(self, state: ChatState, key: str) -> None:
        msg = 'Your step ' + self.get_user_name(key)
        await self.bot.send_message(state.chat_id, msg)

    def process_win(self, state: ChatState, winner: int) -> Coroutine:
        player = state.players[winner]
        name = self.get_user_name(player)
        self.base.del_chat_state(state.chat_id)
        return self.bot.send_message(state.chat_id, f'{name} won the game')

    async def notify_about_win(self, result: NextRoundResult) -> None:
        player = str(result.state.players[result.winner])
        await self.bot.send_message(
            result.state.chat_id, f'Player {player} won the game'
        )

    async def notify_about_loose(self, result: NextRoundResult) -> None:
        players = ', '.join(
            str(result.state.players[player])
            for player in result.loosers
        )
        await self.bot.send_message(result.state.chat_id, f'Loosers: {players}')

    async def notify_about_next_round(self, chat: str):
        await self.bot.send_message(chat, 'Next round')
