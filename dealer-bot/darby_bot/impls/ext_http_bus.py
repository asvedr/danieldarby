import asyncio
from typing import Optional

import aiohttp
from aiohttp import web

from darby_bot.entities.constants import RobotName
from darby_bot.entities.messages import RobotMessage
from darby_bot.proto.bot import IBus


MSG = 'msg/'


class ExtHttpBusListen(IBus):
    def __init__(
        self,
        robots: list[RobotName],
        port: int,
    ):
        self.queues = {
            robot: []
            for robot in robots
        }
        self.port = port
        self.server = None
        self.app = None

    def run(self) -> Optional[asyncio.Task]:
        routes = web.RouteTableDef()

        @routes.post(f'/{MSG}')
        async def listen(request: web.Request):
            data = await request.content.read()
            receiver = RobotName(request.query.get('receiver'))
            msg = RobotMessage.from_json(data)
            self.queues[receiver].append(msg)
            return web.Response(status=201)

        app = web.Application()
        app.add_routes(routes)
        return asyncio.create_task(
            web._run_app(app, host='0.0.0.0', port=self.port)
        )

    def peek(self, name: RobotName) -> Optional[RobotMessage]:
        queue = self.queues[name]
        if queue:
            return queue[0]

    def pop(self, name: RobotName) -> None:
        queue = self.queues[name]
        if queue:
            del queue[0]


class ExtHttpBusSend(IBus):
    def __init__(self, url: str):
        self.session = aiohttp.ClientSession(url)

    async def send(self, receiver: RobotName, msg: RobotMessage) -> None:
        await self.session.post(f'{MSG}?receiver={receiver.value}', data=msg.to_json())
