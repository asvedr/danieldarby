import pytest

from darby_bot.entities.constants import RobotName
from darby_bot.entities.messages import RobotMessage, RobotJoinGame
from darby_bot.impls.int_list_bus import IntListBus


@pytest.mark.asyncio
async def test_send_peek_pop():
    bus = IntListBus([RobotName.dealer, RobotName.player_daniel])
    assert bus.peek(RobotName.dealer) is None
    assert bus.peek(RobotName.player_daniel) is None

    msg = RobotMessage(
        sender=RobotName.dealer,
        chat='123',
        data=RobotJoinGame(all_players=[]),
    )
    await bus.send(RobotName.player_daniel, msg)

    assert bus.peek(RobotName.dealer) is None
    assert bus.peek(RobotName.player_daniel) is msg
    assert bus.peek(RobotName.player_daniel) is msg

    bus.pop(RobotName.player_daniel)
    assert bus.peek(RobotName.player_daniel) is None
