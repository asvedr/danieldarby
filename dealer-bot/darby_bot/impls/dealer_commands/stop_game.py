from aiogram import types

from darby_bot.entities.constants import RobotName
from darby_bot.entities.messages import RobotMessage, RobotStopGame
from darby_bot.proto.bot import IDealerCommand


class StartGameCmd(IDealerCommand):
    commands = ['stop game']

    err_mapping = {}

    async def apply(self, msg: types.Message) -> None:
        chat_id = str(msg.chat.id)
        state = self.base.get_chat_state(chat_id)
        robot_msg = RobotMessage(
            sender=RobotName.dealer,
            chat=chat_id,
            data=RobotStopGame(
                you_win=False,
                by_command=True,
                winner=None,
            )
        )
        self.base.del_chat_state(chat_id)
        for user in state.players:
            if not isinstance(user, RobotName):
                continue
            await self.other_bots[user].send(user, robot_msg)
        await msg.reply('ok')
