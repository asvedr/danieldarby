from typing import Iterable, Union

from aiogram import types

from darby_bot.entities.constants import RobotName, INIT_BALANCE
from darby_bot.entities.messages import RobotMessage, RobotJoinGame
from darby_bot.entities.settings import settings
from darby_bot.entities.states import ChatState, Expected
from darby_bot.entities.user import User
from darby_bot.proto.bot import IDealerCommand


CMD = 'new game'


class StartGameCmd(IDealerCommand):
    commands = [CMD]

    err_mapping = {}

    async def apply(self, msg: types.Message) -> None:
        request_names = [name.lower() for name in msg.text[len(CMD):].split(',')]
        robot_names, user_names = self.split_names(request_names)
        user_names.append(str(msg.from_user.id))
        users = self.base.get_users(user_names)
        user_names = [user.get_any_name() for user in users]
        bot_msg = RobotMessage(
            sender=RobotName.dealer,
            chat=str(msg.chat.id),
            data=RobotJoinGame(all_players=robot_names + user_names),
        )
        request_names.insert(0, str(msg.from_user.id))
        all_players = self.sorted_names(request_names, robot_names + users)
        self.rs_manager.create_game(all_players, INIT_BALANCE)
        self.rs_manager.dump_game()
        game_state = ChatState(
            chat_id=str(msg.chat.id),
            game_state=b'',
            players=all_players,
            expected=Expected.nothing,
        )
        game_state.update(self.rs_manager)
        self.base.set_chat_state(game_state)
        for robot in robot_names:
            await self.other_bots[robot].send(robot, bot_msg)
        await msg.reply('ok')

    @staticmethod
    def split_names(names: Iterable[str]) -> tuple[list[RobotName], list[str]]:
        robots = set()
        users = set()
        robot_names = settings.robot_names()
        for name in names:
            robot = robot_names.get(name)
            if robot:
                robots.add(robot)
            else:
                users.add(name)
        return list(robots), list(users)

    @classmethod
    def sorted_names(cls, request: list[str], users: list[Union[RobotName, User]]) -> list[str]:
        data_to_sort = []
        for user in users:
            if isinstance(user, User):
                data_to_sort.append((cls.get_index_of_user(request, user), user.tg_id))
            else:
                index = request.index(user.value)
                data_to_sort.append((index, user.value))
        data_to_sort.sort(key=lambda p: p[0])
        return [val for _, val in data_to_sort]

    @staticmethod
    def get_index_of_user(request: list[str], user: User) -> int:
        for attr in user.prioritized_names():
            index = request.index(attr)
            if index >= 0:
                return index
        raise Exception('name not found')
