from aiogram import types

from darby_bot.proto.bot import IDealerCommand


class StartGameCmd(IDealerCommand):
    commands = ['ping']

    err_mapping = {}

    async def apply(self, msg: types.Message) -> None:
        await msg.reply('ok')
