from aiogram import types
from danieldarby.errors import Errors

from darby_bot.entities.constants import StepPrefixes
from darby_bot.proto.bot import IDealerCommand, OtherMsgError


class StartGameCmd(IDealerCommand):
    commands = [step.value for step in StepPrefixes]

    err_mapping = {
        Errors.InvalidStep: lambda _: "Invalid step",
        Errors.NotYourStep: lambda _: "Not your step",
    }

    async def apply(self, msg: types.Message) -> None:
        state = self.base.get_chat_state(str(msg.chat.id))
        player_id = state.players.index(str(msg.from_user.id))
        if player_id < 0:
            raise OtherMsgError("You are not in the game")
        self.rs_manager.load_game(state.game_state)
        self.rs_manager.apply_user_step(player_id, msg.text[1:])
        state.update(self.rs_manager)
        self.base.set_chat_state(state)
