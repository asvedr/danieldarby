from typing import Optional, Callable

from aiogram import Dispatcher, types
from aiogram.utils.executor import Executor

from darby_bot.proto.bot import IMsgError


async def start_polling(
    dispatcher: Dispatcher,
    skip_updates: bool = False,
    timeout: float = 20,
    relax: float = 0.1,
    fast: bool = True,
    allowed_updates: Optional[list[str]] = None,
) -> None:
    executor = Executor(dispatcher, skip_updates=skip_updates)
    executor._prepare_polling()
    try:
        await executor.dispatcher.start_polling(
            reset_webhook=True,
            timeout=timeout,
            relax=relax,
            fast=fast,
            allowed_updates=allowed_updates
        )
    except (KeyboardInterrupt, SystemExit):
        return
    finally:
        await executor._shutdown_polling()


def message_handler_decorator(func: Callable) -> Callable:
    async def wrapped(msg: types.Message) -> None:
        try:
            await func(msg)
        except IMsgError as err:
            await msg.reply(f'ERROR: {err.to_msg()}')
    return wrapped

# async def re_start_polling(
#     *args,
#     allowed_exceptions: tuple[type, ...] = (Exception,),
# ) -> None:
#     while True:
#         try:
#             await start_polling(*args)
#         except allowed_exceptions:
#             pass
#         else:
#             break
