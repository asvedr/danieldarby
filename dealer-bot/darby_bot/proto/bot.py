import asyncio
from abc import ABC, abstractmethod
from dataclasses import dataclass
from typing import Optional, Callable

from aiogram import types, Bot
from danieldarby.manager import Manager, UserInfo

from darby_bot.entities.constants import RobotName
from darby_bot.entities.messages import RobotMessage
from darby_bot.entities.states import ChatState
from darby_bot.entities.user import User


class IMsgError(Exception, ABC):
    @abstractmethod
    def to_msg(self) -> str:
        ...


class OtherMsgError(IMsgError):
    def __init__(self, msg: str):
        self.msg = msg

    def to_msg(self) -> str:
        return self.msg


class ICardBot(ABC):
    @abstractmethod
    async def run(self) -> None:
        ...

    @abstractmethod
    async def stop(self) -> None:
        ...

    async def run_with_restarts_on_exc(
        self,
        allowed_exceptions=(Exception,)
    ) -> None:
        while True:
            try:
                await self.run()
            except allowed_exceptions:
                await self.stop()
            else:
                break


class IAi(ABC):
    name: RobotName

    @abstractmethod
    def choose_step(self, info: UserInfo, steps: list[str]) -> str:
        ...

    @abstractmethod
    async def send_msg(self, step: str) -> None:
        ...


class IStateBase(ABC):

    @dataclass
    class UserNotFound(IMsgError):
        users: list[str]

        def to_msg(self) -> str:
            return f'Unknown users: {self.users}'

    class StateNotFound(IMsgError):

        def to_msg(self) -> str:
            return 'Game not found'

    @abstractmethod
    def get_chat_state(self, chat: str) -> ChatState:
        ...

    @abstractmethod
    def set_chat_state(self, state: ChatState) -> None:
        ...

    @abstractmethod
    def del_chat_state(self, chat: str) -> None:
        ...

    @abstractmethod
    def get_state_for_autosteps(self) -> Optional[ChatState]:
        ...

    @abstractmethod
    def update_user(self, user: User) -> None:
        ...

    @abstractmethod
    def get_users(self, attrs: list[str]) -> list[User]:
        ...

    @abstractmethod
    def get_user(self, tg_id: str) -> User:
        ...

    @abstractmethod
    def get_ai_state(self, chat_id: str, robot: RobotName) -> Optional[bytes]:
        ...

    @abstractmethod
    def set_ai_state(self, chat_id: str, robot: RobotName, state: bytes) -> None:
        ...

    @abstractmethod
    def del_ai_state(self, chat_id: str, robot: RobotName) -> None:
        ...


class IDealerCommand(ABC):
    commands: list[str]
    bot: Optional[Bot]
    rs_manager: Manager
    base: IStateBase

    err_mapping: dict[type, Callable[[Exception], str]]

    @abstractmethod
    async def apply(self, msg: types.Message) -> None:
        ...

    async def apply_wrapped(self, msg: types.Message) -> None:
        self.register_user(msg.from_user)
        try:
            await self.apply(msg)
        except IMsgError as err:
            await msg.reply(err.to_msg())
        except Exception as err:
            func = self.err_mapping.get(type(err))
            if func is None:
                raise
            await msg.reply(func(err))

    def register_user(self, user: types.User) -> None:
        self.base.update_user(
            User(
                tg_id=str(user.id),
                name=str(user.first_name).lower(),
                surname=str(user.last_name).lower(),
                login=str(user.username).lower(),
            )
        )
