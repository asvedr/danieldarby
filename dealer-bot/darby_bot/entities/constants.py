from enum import Enum


INIT_BALANCE = 100


class RobotName(Enum):
    dealer = 'dealer'
    player_daniel = 'daniel'
    player_terence = 'terence'
    player_difficento = 'difficeto'


class StepPrefixes(Enum):
    fold = "fold"
    check = "check"
    call = "call"
    raise_ = "raise"
