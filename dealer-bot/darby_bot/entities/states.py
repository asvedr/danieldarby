from enum import IntEnum
from typing import Union, Optional
from dataclasses import dataclass

from danieldarby.manager import Manager

from darby_bot.entities.constants import RobotName


class Expected(IntEnum):
    nothing = 0
    autostep = 1
    ai = 2
    win_notificaton = 3


@dataclass
class ChatState:
    chat_id: str
    game_state: bytes
    players: list[Union[str, RobotName]]
    expected: Expected

    def update(self, manager: Manager, new_raw: bytes = None) -> None:
        if new_raw:
            self.game_state = new_raw
            manager.load_game(new_raw)
        else:
            self.game_state = manager.dump_game()
        winner = manager.get_round_winner()
        if winner is not None:
            self.expected = Expected.win_notificaton
            return
        tp, user_id = manager.get_expected_action()
        if tp == manager.AUTO_STEP:
            self.expected = Expected.autostep
            return
        if tp == manager.USER_STEP and isinstance(self.players[user_id], RobotName):
            self.expected = Expected.ai
            return
        self.expected = Expected.nothing
