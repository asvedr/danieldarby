from dataclasses import dataclass


@dataclass
class User:
    tg_id: str
    name: str
    surname: str
    login: str

    def get_any_name(self) -> str:
        return self.name or self.surname or self.login or self.tg_id

    def prioritized_names(self) -> list[str]:
        return [
            self.tg_id,
            self.login,
            self.name,
            self.surname,
        ]
