from functools import lru_cache

from pydantic import BaseSettings, BaseModel

from darby_bot.entities.constants import RobotName


class RobotConfig(BaseModel):
    name: RobotName
    text_name: str
    token: str


class Settings(BaseSettings):
    robots: list[RobotName]
    external_robots: dict[RobotName, str]
    dealer_token: str
    dealer_name: str = 'dealer'
    daniel_token: str = ''
    daniel_name: str = 'daniel'
    terence_token: str = ''
    terence_name: str = 'terence'
    difficento_token: str = ''
    difficento_name: str = 'difficento'
    base: str
    sleep: float = 0.1
    lib_path: str

    @lru_cache(maxsize=1)
    def robots(self) -> list[RobotConfig]:
        return [
            RobotConfig(
                name=key,
                text_name=getattr(self, key.value + '_name'),
                token=getattr(self, key.value + '_token'),
            )
            for key in list(RobotName)
            if getattr(self, key.value + '_token')
        ]

    class Config:
        env_prefix = 'darby_'


settings = Settings()
