from typing import Union
from dataclasses import dataclass

from danieldarby.manager import UserInfo

from darby_bot.entities.constants import RobotName


@dataclass
class RobotJoinGame:
    all_players: list[Union[str, RobotName]]


@dataclass
class RobotStopGame:
    you_win: bool
    by_command: bool
    winner: Union[str, RobotName]


@dataclass
class RobotMakeStep:
    step: str


@dataclass
class RobotUpdGameState:
    state: UserInfo


@dataclass
class RobotRequestStep:
    info: UserInfo
    steps: list[str]


@dataclass
class RobotMessage:
    sender: RobotName
    chat: str
    data: Union[
        RobotJoinGame,
        RobotStopGame,
        RobotMakeStep,
        RobotUpdGameState,
        RobotRequestStep,
    ]

    @classmethod
    def from_json(cls, src: bytes) -> 'RobotMessage':
        ...

    def to_json(self) -> bytes:
        ...
